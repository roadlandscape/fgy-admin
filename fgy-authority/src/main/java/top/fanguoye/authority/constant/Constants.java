package top.fanguoye.authority.constant;

/**
 * @Author：Vance
 * @Description：常量信息
 * @Date：2021/12/18 10:22
 */
public interface Constants {

  /**
   * 密码hash迭代次数
   */
  int PASSWORD_HASH_ITERATIONS = 512;

  /**
   * 用户账号key
   */
  String USER_ACCOUNT_KEY = "userAccount";

  /**
   * 登录用户唯一标识key
   */
  String LOGIN_UNIQUE_KEY = "loginUniqueKey";

  /**
   * 登录路径
   */
  String LOGIN_PATH = "/auth/login";

  /**
   * 退出路径
   */
  String LOGOUT_PATH = "/auth/logout";

  /**
   * 注册路径
   */
  String REGISTER_PATH = "/auth/register";

  /**
   * 验证码路径
   */
  String CAPTCHA_PATH = "/auth/captcha/**";

  /**
   * WebSocket连接路径统一前缀
   */
  String WEBSOCKET_PATH = "/websocket/**";

  /**
   * 上传文件的访问路径
   */
  String FILE_ACCESS_PATH = "/file/**";
}
