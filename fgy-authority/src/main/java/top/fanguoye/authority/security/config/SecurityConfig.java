package top.fanguoye.authority.security.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import top.fanguoye.authority.constant.Constants;
import top.fanguoye.authority.security.filter.JwtAuthenticationTokenFilter;
import top.fanguoye.authority.security.handler.AccessDeniedHandlerImpl;
import top.fanguoye.authority.security.handler.AuthenticationEntryPointImpl;
import top.fanguoye.authority.security.handler.LogoutSuccessHandlerImpl;

/**
 * @Author：Vance
 * @Description：spring security配置
 * @Date：2022/1/2 13:56
 */
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  private final AuthenticationEntryPointImpl unauthorizedHandler;
  private final AccessDeniedHandlerImpl accessDeniedHandler;
  private final LogoutSuccessHandlerImpl logoutSuccessHandler;
  private final JwtAuthenticationTokenFilter authenticationTokenFilter;
  private final UserDetailsService userDetailsService;

  @Value("${any-request-permit:false}")
  private boolean anyRequestPermit;

  /**
   * 强散列哈希加密实现
   */
  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }

  /**
   * 解决 无法直接注入 AuthenticationManager
   * @return
   * @throws Exception
   */
  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  /**
   * 身份认证接口
   * @param auth
   * @throws Exception
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
  }

  /**
   * anyRequest          |   匹配所有请求路径
   * access              |   SpringEl表达式结果为true时可以访问
   * anonymous           |   匿名可以访问
   * denyAll             |   用户不能访问
   * fullyAuthenticated  |   用户完全认证可以访问（非remember-me下自动登录）
   * hasAnyAuthority     |   如果有参数，参数表示权限，则其中任何一个权限可以访问
   * hasAnyRole          |   如果有参数，参数表示角色，则其中任何一个角色可以访问
   * hasAuthority        |   如果有参数，参数表示权限，则其权限可以访问
   * hasIpAddress        |   如果有参数，参数表示IP地址，如果用户IP和参数匹配，则可以访问
   * hasRole             |   如果有参数，参数表示角色，则其角色可以访问
   * permitAll           |   用户可以任意访问
   * rememberMe          |   允许通过remember-me登录的用户访问
   * authenticated       |   用户登录后可访问
   */
  @Override
  protected void configure(HttpSecurity httpSecurity) throws Exception {
    // 所有请求放行
    if (anyRequestPermit) {
      httpSecurity.cors().and()
          .headers().frameOptions().disable().and()
          .csrf().disable()
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
          .authorizeRequests().anyRequest().permitAll();
      // 退出登录
      httpSecurity.logout().logoutUrl(Constants.LOGOUT_PATH).logoutSuccessHandler(logoutSuccessHandler);
      // 添加JWT filter
      httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
      return;
    }

    httpSecurity
        // 允许跨域访问
        .cors().and()
        // 防止iframe 造成跨域
        .headers().frameOptions().disable().and()
        // CSRF禁用，因为不使用session
        .csrf().disable()
        // 认证失败处理类
        .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
        // 权限不足处理类
        .accessDeniedHandler(accessDeniedHandler).and()
        // 基于token，所以不需要session
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        // 过滤请求
        .authorizeRequests()
        // 对于登录login 注册register 验证码captcha 允许匿名访问
        .antMatchers(
            Constants.LOGIN_PATH,
            Constants.REGISTER_PATH,
            Constants.CAPTCHA_PATH
        ).anonymous()
        .antMatchers(
            HttpMethod.GET,
            "/",
            "/*.html",
            "/**/*.html",
            "/**/*.css",
            "/**/*.js",
            Constants.FILE_ACCESS_PATH
        ).permitAll()
        .antMatchers("/favicon.ico").permitAll()
        .antMatchers("/doc.html").permitAll()
        .antMatchers("/swagger-ui.html").permitAll()
        .antMatchers("/swagger-resources/**").permitAll()
        .antMatchers("/webjars/**").permitAll()
        .antMatchers("/*/api-docs").permitAll()
        .antMatchers(
            "/error",
            Constants.WEBSOCKET_PATH
        ).permitAll()
        // 除上面外的所有请求全部需要鉴权认证
        .anyRequest().authenticated();
    // 退出登录
    httpSecurity.logout().logoutUrl(Constants.LOGOUT_PATH).logoutSuccessHandler(logoutSuccessHandler);
    // 添加JWT filter
    httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
  }
}
