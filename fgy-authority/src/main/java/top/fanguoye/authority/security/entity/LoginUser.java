package top.fanguoye.authority.security.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import top.fanguoye.core.enums.Sex;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.enums.UserType;

import java.util.Collection;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：登录用户信息
 * @Date：2021/12/29 23:52
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "登录用户信息")
public class LoginUser implements UserDetails {

  private Long id;

  @ApiModelProperty("登录用户唯一标识")
  private String loginUniqueKey;

  @ApiModelProperty("部门ID")
  private Long deptId;

  @ApiModelProperty("部门名称")
  private String deptName;

  @ApiModelProperty("用户账号")
  private String userAccount;

  @ApiModelProperty("用户昵称")
  private String nickName;

  @ApiModelProperty("密码")
  private String password;

  @ApiModelProperty(value = "1:管理员 2:普通用户", allowableValues = "1,2")
  private UserType userType;

  @ApiModelProperty("邮箱")
  private String email;

  @ApiModelProperty("联系电话")
  private String phone;

  @ApiModelProperty(value = "1:男 2:女 3:未知", allowableValues = "1,2,3")
  private Sex sex;

  @ApiModelProperty("头像地址")
  private String avatar;

  @ApiModelProperty(value = "账户状态 1:正常 2:停用", allowableValues = "1,2")
  private Status status;

  @ApiModelProperty("备注")
  private String remark;

  @ApiModelProperty(value = "登录时间")
  private Long loginTime;

  @ApiModelProperty(value = "过期时间")
  private Long expireTime;

  @ApiModelProperty(value = "权限列表")
  private Set<String> permissions;

  @ApiModelProperty(value = "角色列表")
  private Set<String> roles;

  @JsonIgnore
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return null;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @JsonIgnore
  @Override
  public String getUsername() {
    return userAccount;
  }

  /**
   * 账户是否未过期,过期无法验证
   * @return
   */
  @JsonIgnore
  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  /**
   * 用户是否解锁,锁定的用户无法进行身份验证
   * @return
   */
  @JsonIgnore
  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  /**
   * 是否未过期的用户凭据(密码),过期的凭据禁止认证
   * @return
   */
  @JsonIgnore
  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  /**
   * 是否可用 ,禁用的用户不能身份验证
   * @return
   */
  @JsonIgnore
  @Override
  public boolean isEnabled() {
    return Status.NORMAL.equals(status);
  }
}
