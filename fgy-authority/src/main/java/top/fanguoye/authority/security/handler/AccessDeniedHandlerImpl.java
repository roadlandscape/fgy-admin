package top.fanguoye.authority.security.handler;

import cn.hutool.json.JSONUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.utils.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author：Vance
 * @Description：权限不足处理类 只能捕获通过 .antMatchers("/xxx").hasXXX("xxx") 配置权限引起的异常,注解设置的无法捕获
 * @Date：2022/1/3 16:49
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {

  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {
    String result = JSONUtil.toJsonStr(R.resp(HttpStatus.FORBIDDEN));
    ServletUtils.renderString(response, result);
  }
}
