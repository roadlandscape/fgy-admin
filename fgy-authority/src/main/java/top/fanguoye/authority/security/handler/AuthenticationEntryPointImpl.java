package top.fanguoye.authority.security.handler;

import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.utils.ServletUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author：Vance
 * @Description：未携带token，或者token认证失败，会调用此类，返回凭证认证失败
 * @Date：2022/1/2 14:12
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
    String errMsg = (String) request.getAttribute("errMsg");
    if (StrUtil.isNotEmpty(errMsg)) {
      ServletUtils.renderString(response, JSONUtil.toJsonStr(R.resp(HttpStatus.UNAUTHORIZED.getCode(), errMsg)));
    } else {
      ServletUtils.renderString(response, JSONUtil.toJsonStr(R.resp(HttpStatus.UNAUTHORIZED)));
    }
  }
}
