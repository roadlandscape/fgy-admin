package top.fanguoye.authority.security.handler;

import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import top.fanguoye.authority.security.service.TokenService;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.utils.ServletUtils;
import top.fanguoye.authority.security.entity.LoginUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author：Vance
 * @Description：自定义退出处理类 返回成功
 * @Date：2022/1/2 16:05
 */
@Component
@RequiredArgsConstructor
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler {

  private final TokenService tokenService;

  @Override
  public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
    LoginUser loginUser = tokenService.getLoginUser(request);
    if (loginUser != null) {
      // 删除用户登录记录
      tokenService.delLoginUser(loginUser.getLoginUniqueKey(), loginUser.getUserAccount());
    }
    String result = JSONUtil.toJsonStr(R.ok("退出成功！"));
    ServletUtils.renderString(response, result);
  }
}
