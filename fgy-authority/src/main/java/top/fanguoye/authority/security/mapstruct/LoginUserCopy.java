package top.fanguoye.authority.security.mapstruct;

import org.mapstruct.*;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.core.interfaces.BaseCopy;
import top.fanguoye.system.entity.po.SysUser;

/**
 * @Author：Vance
 * @Description：登录用户copy
 * @Date：2021/12/29 23:51
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LoginUserCopy extends BaseCopy<SysUser, LoginUser> {
}
