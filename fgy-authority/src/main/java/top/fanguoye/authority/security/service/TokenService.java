package top.fanguoye.authority.security.service;

import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import top.fanguoye.authority.constant.Constants;
import top.fanguoye.authority.utils.JWTUtil;
import top.fanguoye.core.constant.RedisConstants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.redis.RedisCache;
import top.fanguoye.authority.security.entity.LoginUser;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author：Vance
 * @Description：token验证处理
 * @Date：2022/1/2 16:27
 */
@Component
@RequiredArgsConstructor
public class TokenService {

  private final RedisCache redisCache;

  /** 令牌请求头 */
  @Value("${token.header}")
  private String header;
  /** 令牌签发人 */
  @Value("${token.issuer}")
  private String issuer;
  /** 令牌秘钥 */
  @Value("${token.secret}")
  private String secret;
  /** 令牌有效期，单位分（默认30分钟） */
  @Value("${token.expire-time:30}")
  private int expireTime;
  /** 令牌有效期剩余多少自动刷新，单位分（默认20分钟） */
  @Value("${token.refresh-time:20}")
  private int refreshTime;

  /**
   * 获取登录用户身份信息
   * @param request
   * @return
   */
  public LoginUser getLoginUser(HttpServletRequest request) {
    // 获取请求携带的令牌
    String token = getToken(request);
    LoginUser loginUser = getLoginUser(token);
    if (loginUser == null) {
      request.setAttribute("errMsg", "登录状态已失效，请重新登录!");
    }
    return loginUser;
  }

  /**
   * 获取登录用户身份信息
   * @param token
   * @return
   */
  public LoginUser getLoginUser(String token) {
    if (StrUtil.isEmpty(token)) {
      return null;
    }

    Map<String, String> map = JWTUtil.decode(token, this.issuer, this.secret);
    if (map == null) {
      return null;
    }

    return redisCache.getCacheObject(
        RedisConstants.LOGIN_INFO_KEY + map.get(Constants.USER_ACCOUNT_KEY) + StringPool.COLON + map.get(Constants.LOGIN_UNIQUE_KEY));
  }

  /**
   * 删除登录用户身份信息
   * @param loginUniqueKey
   * @param userAccount
   */
  public void delLoginUser(String loginUniqueKey, String userAccount) {
    redisCache.delObject(RedisConstants.LOGIN_INFO_KEY + userAccount + StringPool.COLON + loginUniqueKey);
  }

  /**
   * 创建令牌
   * @param loginUser
   * @return
   */
  public String createToken(LoginUser loginUser) {
    loginUser.setLoginTime(System.currentTimeMillis());
    loginUser.setExpireTime(loginUser.getLoginTime() + (this.expireTime * 60 * 1000L));
    redisCache.setCacheObject(RedisConstants.LOGIN_INFO_KEY + loginUser.getUserAccount() + StringPool.COLON + loginUser.getLoginUniqueKey()
        , loginUser, this.expireTime, TimeUnit.MINUTES);
    return JWTUtil.encode(loginUser.getLoginUniqueKey(), loginUser.getUserAccount(), this.issuer, this.secret);
  }

  /**
   * 刷新令牌（缓存）
   * @param loginUser
   */
  public void refreshToken(LoginUser loginUser) {
    long expireTime = loginUser.getExpireTime();
    long currentTime = System.currentTimeMillis();
    if (expireTime - currentTime <= (this.refreshTime * 60 * 1000L)) {
      loginUser.setLoginTime(System.currentTimeMillis());
      loginUser.setExpireTime(loginUser.getLoginTime() + (this.expireTime * 60 * 1000L));
      redisCache.setCacheObject(RedisConstants.LOGIN_INFO_KEY + loginUser.getUserAccount() + StringPool.COLON + loginUser.getLoginUniqueKey()
          , loginUser, this.expireTime, TimeUnit.MINUTES);
    }
  }

  /**
   * 获取请求token
   * @param request
   * @return
   */
  public String getToken(HttpServletRequest request) {
    return request.getHeader(this.header);
  }
}
