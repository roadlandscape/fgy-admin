package top.fanguoye.authority.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.authority.security.mapstruct.LoginUserCopy;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.enums.UserType;
import top.fanguoye.system.entity.po.SysUser;
import top.fanguoye.system.service.ISysMenuService;
import top.fanguoye.system.service.ISysUserService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：用户验证处理
 * @Date：2022/1/2 22:43
 */
@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

  private final ISysUserService sysUserService;
  private final ISysMenuService sysMenuService;
  private final LoginUserCopy loginUserCopy;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<SysUser> sysUserOptional = sysUserService.selectByUserAccount(username);
    sysUserOptional.orElseThrow(() -> new UsernameNotFoundException("用户不存在!"));
    SysUser sysUser = sysUserOptional.get();

    LoginUser loginUser = loginUserCopy.toT(sysUser);
    if (UserType.ADMIN.equals(loginUser.getUserType())) {
      Set<String> permsSet = new HashSet<>();
      permsSet.add(Constants.ADMIN_PERMS_SIGN);
      loginUser.setPermissions(permsSet);
      return loginUser;
    }
    loginUser.setPermissions(sysMenuService.selectMenuPermsByUserId(loginUser.getId()));
    return loginUser;
  }
}
