package top.fanguoye.authority.security.utils;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import top.fanguoye.authority.security.entity.LoginUser;

/**
 * @Author：Vance
 * @Description：security工具类
 * @Date：2022/1/3 14:21
 */
public class SecurityUtils {

  /**
   * 生成BCryptPasswordEncoder密码
   * @param password 密码
   * @return 加密字符串
   */
  public static String encryptPassword(String password) {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    return passwordEncoder.encode(password);
  }

  /**
   * 获取当前登录用户信息
   * 未登录返回null
   * @return
   */
  public static LoginUser getLoginUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return null;
    }
    // 如果是未登录状态会返回 anonymousUser
    Object principal = authentication.getPrincipal();
    if (principal instanceof LoginUser) {
      return (LoginUser) principal;
    } else {
      return null;
    }
  }

  /**
   * 获取当前登录用户的ID
   * 未登录返回null
   * @return
   */
  public static Long getLoginUserId() {
    LoginUser loginUser = getLoginUser();
    if (loginUser != null) {
      return loginUser.getId();
    }
    return null;
  }

  /**
   * 获取当前登录用户的唯一标识
   * 未登录返回null
   * @return
   */
  public static String getLoginUniqueKey() {
    LoginUser loginUser = getLoginUser();
    if (loginUser != null) {
      return loginUser.getLoginUniqueKey();
    }
    return null;
  }

  /**
   * 获取当前登录用户的账号
   * 未登录返回null
   * @return
   */
  public static String getLoginUserAccount() {
    LoginUser loginUser = getLoginUser();
    if (loginUser != null) {
      return loginUser.getUserAccount();
    }
    return null;
  }

  /**
   * 获取当前登录用户的昵称
   * 未登录返回null
   * @return
   */
  public static String getLoginNickName() {
    LoginUser loginUser = getLoginUser();
    if (loginUser != null) {
      return loginUser.getNickName();
    }
    return null;
  }

  /**
   * 判断密码是否相同
   * @param rawPassword 真实密码
   * @param encodedPassword 加密后字符
   * @return
   */
  public static boolean matchesPassword(String rawPassword, String encodedPassword) {
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    return passwordEncoder.matches(rawPassword, encodedPassword);
  }
}
