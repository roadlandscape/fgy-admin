package top.fanguoye.authority.shiro;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import top.fanguoye.authority.constant.Constants;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.authority.security.service.TokenService;
import top.fanguoye.authority.utils.JWTUtil;
import top.fanguoye.core.constant.RedisConstants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.redis.RedisCache;

import java.util.Map;

/**
 * @Author：Vance
 * @Description：自定义的Realm
 * @Date：2021/12/17 10:11
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class CustomRealm extends AuthorizingRealm {

  private final TokenService tokenService;
  private final RedisCache redisCache;

  /** 令牌签发人 */
  @Value("${token.issuer}")
  private String issuer;
  /** 令牌秘钥 */
  @Value("${token.secret}")
  private String secret;

  @Override
  public boolean supports(AuthenticationToken token) {
    // 这个 Realm 仅支持 JwtToken
    return token instanceof JwtToken;
  }

  /**
   * 获取身份验证信息
   * @param authenticationToken
   * @return
   * @throws AuthenticationException
   */
  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
    String token = (String) authenticationToken.getCredentials();
    Map<String, String> map = JWTUtil.decode(token, this.issuer, this.secret);
    if (map == null) {
      throw new AuthenticationException("token校验失败!");
    }
    LoginUser loginUser = redisCache.getCacheObject(
        RedisConstants.LOGIN_INFO_KEY + map.get(Constants.USER_ACCOUNT_KEY) + StringPool.COLON + map.get(Constants.LOGIN_UNIQUE_KEY));
    if (loginUser == null) {
      throw new AuthenticationException("登录状态已失效，请重新登录!");
    }
    tokenService.refreshToken(loginUser);
    return new SimpleAuthenticationInfo(loginUser, token, getName());
  }

  /**
   * 获取授权信息
   * @param principals
   * @return
   */
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    LoginUser loginUser = (LoginUser) principals.getPrimaryPrincipal();
    SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
    simpleAuthorizationInfo.setStringPermissions(loginUser.getPermissions());
    simpleAuthorizationInfo.setRoles(loginUser.getRoles());
    return simpleAuthorizationInfo;
  }
}
