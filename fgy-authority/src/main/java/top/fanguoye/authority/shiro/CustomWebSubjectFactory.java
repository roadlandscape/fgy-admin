package top.fanguoye.authority.shiro;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSubjectFactory;
import org.springframework.stereotype.Component;

/**
 * @Author：Vance
 * @Description：定义一个 WebSubjectFactory, 用来禁用 session
 * @Date：2021/12/18 11:17
 */
@Component
public class CustomWebSubjectFactory extends DefaultWebSubjectFactory {

  @Override
  public Subject createSubject(SubjectContext context) {
    // 禁用session
    context.setSessionCreationEnabled(false);
    return super.createSubject(context);
  }
}
