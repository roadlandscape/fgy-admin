package top.fanguoye.authority.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @Author：Vance
 * @Description：封装token来替换Shiro原生Token
 * @Date：2021/12/17 10:20
 */
public class JwtToken implements AuthenticationToken {

    private static final long serialVersionUID = 1L;

    private final String token;

    public JwtToken(String token) {
        this.token = token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}