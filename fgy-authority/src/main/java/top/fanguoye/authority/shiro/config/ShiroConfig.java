package top.fanguoye.authority.shiro.config;

import lombok.RequiredArgsConstructor;
import org.apache.shiro.mgt.DefaultSessionStorageEvaluator;
import org.apache.shiro.mgt.DefaultSubjectDAO;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.fanguoye.authority.shiro.CustomRealm;
import top.fanguoye.authority.shiro.CustomWebSubjectFactory;
import top.fanguoye.authority.constant.Constants;
import top.fanguoye.authority.shiro.filter.AnonFilter;
import top.fanguoye.authority.shiro.filter.JWTFilter;
import top.fanguoye.authority.shiro.filter.LogoutFormAuthenticationFilter;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：shiro配置
 * @Date：2021/12/17 09:26
 */
@Configuration
@RequiredArgsConstructor
public class ShiroConfig {

  private final CustomWebSubjectFactory customWebSubjectFactory;

  @Value("${any-request-permit:false}")
  private boolean anyRequestPermit;

  /**
   * 注册shiro的Filter，拦截请求
   * @param securityManager
   * @return
   */
  @Bean
  public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager
      , ShiroFilterChainDefinition shiroFilterChainDefinition
      , FilterRegistrationBean<JWTFilter> jwtFilterRegBean
      , FilterRegistrationBean<AnonFilter> anonFilterRegBean) {
    ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
    shiroFilterFactoryBean.setSecurityManager(securityManager);
    Map<String, Filter> filterMap = new LinkedHashMap<>();
    // 添加jwt过滤器
    filterMap.put("jwt", jwtFilterRegBean.getFilter());
    // 添加anon过滤器
    filterMap.put("anon", anonFilterRegBean.getFilter());
    // 添加自定义退出过滤器
    filterMap.put("logout", new LogoutFormAuthenticationFilter());
    shiroFilterFactoryBean.setFilters(filterMap);
    // 表示指定登录页面(前后分离不适用)
    // shiroFilterFactoryBean.setLoginUrl();
    // 登录成功后要跳转的链接(前后分离不适用)
    // shiroFilterFactoryBean.setSuccessUrl();
    // 设置无权限时跳转的链接(前后分离不适用)
    // shiroFilterFactoryBean.setUnauthorizedUrl();
    shiroFilterFactoryBean.setFilterChainDefinitionMap(shiroFilterChainDefinition.getFilterChainMap());
    return shiroFilterFactoryBean;
  }

  /**
   * 配置过滤器链
   */
  @Bean
  public ShiroFilterChainDefinition shiroFilterChainDefinition() {
    DefaultShiroFilterChainDefinition chainDefinition = new DefaultShiroFilterChainDefinition();
    // 所有请求放行
    if (this.anyRequestPermit) {
      chainDefinition.addPathDefinition("/**", "anon");
      return chainDefinition;
    }

    chainDefinition.addPathDefinition("/favicon.ico","anon");
    chainDefinition.addPathDefinition("/doc.html","anon");
    chainDefinition.addPathDefinition("/swagger-ui.html","anon");
    chainDefinition.addPathDefinition("/swagger-resources/**","anon");
    chainDefinition.addPathDefinition("/webjars/**","anon");
    chainDefinition.addPathDefinition("/*/api-docs","anon");
    chainDefinition.addPathDefinition("/","anon");
    chainDefinition.addPathDefinition("/*.html","anon");
    chainDefinition.addPathDefinition("/**/*.html","anon");
    chainDefinition.addPathDefinition("/**/*.css","anon");
    chainDefinition.addPathDefinition("/**/*.js","anon");

    chainDefinition.addPathDefinition("/error","anon");
    chainDefinition.addPathDefinition(Constants.WEBSOCKET_PATH,"anon");
    chainDefinition.addPathDefinition(Constants.LOGIN_PATH,"anon");
    chainDefinition.addPathDefinition(Constants.REGISTER_PATH,"anon");
    chainDefinition.addPathDefinition(Constants.CAPTCHA_PATH,"anon");
    chainDefinition.addPathDefinition(Constants.FILE_ACCESS_PATH,"anon");
    // 退出访问url
    chainDefinition.addPathDefinition(Constants.LOGOUT_PATH, "logout");
    // 这行代码必须放在所有权限设置的最后，不然会导致所有 url 都被拦截
    // chainDefinition.addPathDefinition("/**", "authc");
    // 使用自定义过滤器拦截所有请求，不再使用默认的authc
    chainDefinition.addPathDefinition("/**", "jwt");
    return chainDefinition;
  }

  /**
   * 配置JwtFilter过滤器,并设置为未注册状态
   */
  @Bean
  public FilterRegistrationBean<JWTFilter> jwtFilterRegBean() {
    FilterRegistrationBean<JWTFilter> filterRegistrationBean = new FilterRegistrationBean<>();
    // 添加 JwtFilter 并设置为未注册状态
    filterRegistrationBean.setFilter(new JWTFilter());
    filterRegistrationBean.setEnabled(false);
    return filterRegistrationBean;
  }

  /**
   * 配置AnonFilter过滤器,并设置为未注册状态
   */
  @Bean
  public FilterRegistrationBean<AnonFilter> anonFilterRegBean() {
    FilterRegistrationBean<AnonFilter> filterRegistrationBean = new FilterRegistrationBean<>();
    // 添加 AnonFilter 并设置为未注册状态
    filterRegistrationBean.setFilter(new AnonFilter());
    filterRegistrationBean.setEnabled(false);
    return filterRegistrationBean;
  }

  /**
   * 注入 securityManager
   * @return
   */
  @Bean
  public DefaultWebSecurityManager securityManager(CustomRealm customRealm) {
    DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
    securityManager.setRealm(customRealm);
    securityManager.setSubjectFactory(this.customWebSubjectFactory);

    // 关闭shiro自带的session
    DefaultSubjectDAO subjectDAO = new DefaultSubjectDAO();
    DefaultSessionStorageEvaluator defaultSessionStorageEvaluator = new DefaultSessionStorageEvaluator();
    defaultSessionStorageEvaluator.setSessionStorageEnabled(false);
    subjectDAO.setSessionStorageEvaluator(defaultSessionStorageEvaluator);
    securityManager.setSubjectDAO(subjectDAO);
    // 禁用 rememberMe
    securityManager.setRememberMeManager(null);
    return securityManager;
  }

  /**
   * 解决 spring boot + shiro + @RequiresPermissions 的 controller 中 swagger 无法读取
   * @return
   */
  @Bean
  public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
    DefaultAdvisorAutoProxyCreator proxyCreator = new DefaultAdvisorAutoProxyCreator();
    // proxyTargetClass为true，使用cglib代理，使类对象在被二次代理时，仍旧能够找到原始类对象，并且被成功放入映射注册表。
    proxyCreator.setProxyTargetClass(true);
    return proxyCreator;
  }
}
