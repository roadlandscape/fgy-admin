package top.fanguoye.authority.shiro.filter;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import top.fanguoye.authority.shiro.JwtToken;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author：Vance
 * @Description：放行资源过滤器
 * @Date：2022/1/11 11:42
 */
@Slf4j
public class AnonFilter extends BasicHttpAuthenticationFilter {

  /**
   * 如果带有 token，则尝试执行登录，不管登录成功与否，都放行，因为拦截的资源是放行的资源
   * @param request
   * @param response
   * @param mappedValue
   * @return
   */
  @Override
  protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
    // 判断请求的请求头是否带上 "token"
    if (isLoginAttempt(request, response)) {
      // 如果存在，则进入 executeLogin 方法执行登入，检查 token 是否正确
      return executeLogin(request, response);
    }
    return true;
  }

  /**
   * 检测headers里是否包含token字段
   */
  @Override
  protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
    HttpServletRequest req = (HttpServletRequest) request;
    String token = req.getHeader(SpringUtil.getBean(Environment.class).getProperty("token.header"));
    return StrUtil.isNotEmpty(token);
  }

  /**
   * 重写AuthenticatingFilter的executeLogin方法丶执行登陆操作
   */
  @Override
  protected boolean executeLogin(ServletRequest request, ServletResponse response) {
    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
    String token = httpServletRequest.getHeader(SpringUtil.getBean(Environment.class).getProperty("token.header"));
    JwtToken jwtToken = new JwtToken(token);
    // 提交给realm进行登入
    try {
      getSubject(request, response).login(jwtToken);
    } catch (AuthenticationException e) {
      // 拦截的资源是放行的资源，登录失败依然放行
      log.warn("登录失败:{}", e.getMessage());
      return true;
    }
    return true;
  }

  /**
   * 对跨域提供支持
   */
  @Override
  protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
    httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");
    httpServletResponse.setHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
    httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PATCH,PUT,DELETE");
    httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
    httpServletResponse.setHeader("Access-Control-Expose-Headers", "token,Authorization");
    // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
    if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
      httpServletResponse.setStatus(HttpStatus.OK.value());
      return false;
    }
    return super.preHandle(request, response);
  }
}
