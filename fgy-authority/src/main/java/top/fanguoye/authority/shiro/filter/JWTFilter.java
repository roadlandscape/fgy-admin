package top.fanguoye.authority.shiro.filter;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import top.fanguoye.authority.shiro.JwtToken;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.utils.ServletUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author：Vance
 * @Description：自定义过滤器，对token进行处理
 * @Date：2021/12/17 15:51
 */
@Slf4j
public class JWTFilter extends BasicHttpAuthenticationFilter {

  /**
   * 如果带有 token，则对 token 进行检查，否则返回凭证认证失败
   * 不存在 token，可能是执行登陆或者游客状态访问
   *    一般处理方式是无需检查 token，直接返回 true；这样对于需要token才能访问的资源都需要加上注解@RequiresAuthentication，否则会被绕过；
   *    这种方式处理繁琐（容易忘记加注解），这里拦截所有url，不携带token一律认为非法操作，对需要放行的url通过配置过滤链放行
   * @param request
   * @param response
   * @param mappedValue
   * @return
   */
  @Override
  protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
    // 判断请求的请求头是否带上 "token"
    if (isLoginAttempt(request, response)) {
      // 如果存在，则进入 executeLogin 方法执行登入，检查 token 是否正确
      return executeLogin(request, response);
    }
    log.error("\n==============请求地址'{}', 未携带token.==============", ((HttpServletRequest) request).getRequestURI());
    return false;
  }

  /**
   * 处理认证失败，isAccessAllowed 返回 false 会进入这里
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  @Override
  protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
    String errMsg = (String) request.getAttribute("errMsg");
    if (StrUtil.isNotEmpty(errMsg)) {
      ServletUtils.renderString((HttpServletResponse) response
          , JSONUtil.toJsonStr(R.resp(top.fanguoye.core.enums.HttpStatus.UNAUTHORIZED.getCode(), errMsg)));
    } else {
      ServletUtils.renderString((HttpServletResponse) response
          , JSONUtil.toJsonStr(R.resp(top.fanguoye.core.enums.HttpStatus.UNAUTHORIZED)));
    }
    return false;
  }

  /**
   * 判断用户是否是登入,检测headers里是否包含token字段
   */
  @Override
  protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
    HttpServletRequest req = (HttpServletRequest) request;
    String token = req.getHeader(SpringUtil.getBean(Environment.class).getProperty("token.header"));
    return StrUtil.isNotEmpty(token);
  }

  /**
   * 重写AuthenticatingFilter的executeLogin方法丶执行登陆操作
   */
  @Override
  protected boolean executeLogin(ServletRequest request, ServletResponse response) {
    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
    String token = httpServletRequest.getHeader(SpringUtil.getBean(Environment.class).getProperty("token.header"));
    JwtToken jwtToken = new JwtToken(token);
    // 提交给realm进行登入
    try {
      getSubject(request, response).login(jwtToken);
    } catch (AuthenticationException e) {
      log.error("登录失败!", e);
      request.setAttribute("errMsg", e.getMessage());
      return false;
    }
    return true;
  }

  /**
   * 对跨域提供支持
   */
  @Override
  protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
    HttpServletResponse httpServletResponse = (HttpServletResponse) response;
    httpServletResponse.setHeader("Access-Control-Allow-Credentials", "true");
    httpServletResponse.setHeader("Access-Control-Allow-Origin", httpServletRequest.getHeader("Origin"));
    httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PATCH,PUT,DELETE");
    httpServletResponse.setHeader("Access-Control-Allow-Headers", httpServletRequest.getHeader("Access-Control-Request-Headers"));
    httpServletResponse.setHeader("Access-Control-Expose-Headers", "token,Authorization");
    // 跨域时会首先发送一个option请求，这里我们给option请求直接返回正常状态
    if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
      httpServletResponse.setStatus(HttpStatus.OK.value());
      return false;
    }
    return super.preHandle(request, response);
  }
}
