package top.fanguoye.authority.shiro.filter;

import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONUtil;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.authority.security.service.TokenService;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.utils.ServletUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Author：Vance
 * @Description：退出过滤器
 * @Date：2022/1/5 18:28
 */
public class LogoutFormAuthenticationFilter extends LogoutFilter {

  @Override
  protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
    Subject subject = getSubject(request, response);
    subject.logout();
    TokenService tokenService = SpringUtil.getBean(TokenService.class);
    LoginUser loginUser = tokenService.getLoginUser((HttpServletRequest) request);
    if (loginUser != null) {
      // 删除用户登录记录
      tokenService.delLoginUser(loginUser.getLoginUniqueKey(), loginUser.getUserAccount());
    }
    ServletUtils.renderString((HttpServletResponse) response, JSONUtil.toJsonStr(R.ok("退出成功！")));
    return false;
  }
}
