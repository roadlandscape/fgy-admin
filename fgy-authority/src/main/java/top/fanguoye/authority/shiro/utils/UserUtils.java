package top.fanguoye.authority.shiro.utils;

import top.fanguoye.authority.security.entity.LoginUser;

/**
 * @Author：Vance
 * @Description：用户信息工具类
 * @Date：2021/12/21 10:14
 */
public class UserUtils {

  /**
   * 获取当前登录用户信息
   * 未登录返回null
   * @return
   */
  public static LoginUser getLoginUser() {
    Object obj = getPrincipal();
    if (obj == null) {
      return null;
    }
    return (LoginUser) obj;
  }

  /**
   * 获取当前登录用户的ID
   * 未登录返回null
   * @return
   */
  public static Long getLoginUserId() {
    LoginUser user = getLoginUser();
    if (user == null) {
      return null;
    }
    return user.getId();
  }

  /**
   * 获取当前登录用户的账号
   * 未登录返回null
   * @return
   */
  public static String getLoginUserAccount() {
    LoginUser user = getLoginUser();
    if (user == null) {
      return null;
    }
    return user.getUserAccount();
  }

  /**
   * 获取当前登录用户的昵称
   * 未登录返回null
   * @return
   */
  public static String getLoginNickName() {
    LoginUser user = getLoginUser();
    if (user == null) {
      return null;
    }
    return user.getNickName();
  }

  private static Object getPrincipal() {
    return org.apache.shiro.SecurityUtils.getSubject().getPrincipal();
  }
}
