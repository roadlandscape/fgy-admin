package top.fanguoye.authority.utils;

import cn.hutool.core.map.MapUtil;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import top.fanguoye.authority.constant.Constants;

import java.util.*;

/**
 * @Author：Vance
 * @Description：jwt工具类
 * @Date：2021/12/17 15:58
 */
@Slf4j
public class JWTUtil {

  /**
   * jwt字符串包括三个部分
   * 1. header
   *    typ：当前字符串的类型，一般都是“JWT”
   *    alg：哪种算法加密，“HS256”或者其他的加密算法
   * 2. payload 一般有四个最常见的标准字段 可以有自定义数据
   *    iat：签发时间，也就是这个jwt什么时候生成的
   *    jti：JWT的唯一标识，主要用来作为一次性token,从而回避重放攻击
   *    iss：签发人
   *    exp：过期时间，这个过期时间必须要大于签发时间
   *    aud：接收jwt的一方
   *    sub：jwt所面向的用户
   *    nbf：定义在什么时间之前，该jwt都是不可用的.
   * 3. Signature
   *    1.将第一段base64编码 + 第二段base64编码 字符串拼接起来（中间用.）
   *    2.将拼接完成的字符串进行加密， Header中的算法 + 盐 + 密钥
   * @param ttlMillis 有效时长 如果不传，默认1天
   * @param loginUniqueKey 存储一些其他非隐私信息
   * @param issuer 签发人
   * @param secret 密钥
   * @return
   */
  public static String encode(Long ttlMillis, String loginUniqueKey, String issuer, String secret) {
    if (ttlMillis == null) {
      ttlMillis = 24 * 60 * 60 * 1000L;
    }

    Algorithm algorithm = Algorithm.HMAC256(secret);
    String token = JWT.create()
        // 签发时间
        .withIssuedAt(new Date())
        // 签发人
        .withIssuer(issuer)
        // 过期时间
        .withExpiresAt(new Date(System.currentTimeMillis() + ttlMillis))
        .withClaim(Constants.LOGIN_UNIQUE_KEY, loginUniqueKey)
        .sign(algorithm);
    return token;
  }

  /**
   * 生成token，不设置过期时间，Token的有效性转到Redis维护
   * @param loginUniqueKey
   * @param userAccount
   * @param issuer
   * @param secret
   * @return
   */
  public static String encode(String loginUniqueKey, String userAccount, String issuer, String secret) {
    Algorithm algorithm = Algorithm.HMAC256(secret);
    String token = JWT.create()
        // 签发时间
        .withIssuedAt(new Date())
        // 签发人
        .withIssuer(issuer)
        .withClaim(Constants.LOGIN_UNIQUE_KEY, loginUniqueKey)
        .withClaim(Constants.USER_ACCOUNT_KEY, userAccount)
        .sign(algorithm);
    return token;
  }

  /**
   * 解码token，得到自定义信息
   * @param token
   * @param issuer
   * @param secret
   * @return 如果为null，表示解码失败
   */
  public static Map<String, String> decode(String token, String issuer, String secret) {
    try {
      Algorithm algorithm = Algorithm.HMAC256(secret);
      JWTVerifier verifier = JWT.require(algorithm).withIssuer(issuer).build();
      // 校验TOKEN
      DecodedJWT jwt = verifier.verify(token);
      String userAccount = jwt.getClaim(Constants.USER_ACCOUNT_KEY).asString();
      String loginUniqueKey = jwt.getClaim(Constants.LOGIN_UNIQUE_KEY).asString();
      Map<String, String> map = MapUtil.newHashMap(2);
      map.put(Constants.USER_ACCOUNT_KEY, userAccount);
      map.put(Constants.LOGIN_UNIQUE_KEY, loginUniqueKey);
      return map;
    } catch (Exception e) {
      log.error("token校验失败：", e);
      return null;
    }
  }

  public static void main(String[] args) {
    String token = JWTUtil.encode("123456", "vance", "fgy-admin", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9");
    System.out.println(token);

    System.out.println(JWTUtil.decode(token, "fgy-admin", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"));
  }
}
