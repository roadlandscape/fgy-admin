package top.fanguoye.core;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.map.MapUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：程序启动后执行
 * @Date：2021/12/28 11:37
 */
@Slf4j
@Component
public class MyApplicationRunner implements ApplicationRunner {

  /** 继承了 BaseEnum接口 的枚举map,用于json根据枚举成员变量反序列化 */
  public static final Map<Class<BaseEnum>, Map<Integer, BaseEnum>> ENUM_MAP = new HashMap<>();

  /** 枚举包 */
  @Value("${mybatis-plus.type-enums-package:}")
  private String typeEnumsPackage;

  @Override
  public void run(ApplicationArguments args) {
    try {
      Set<Class<BaseEnum>> classes = ClassUtils.getExtendsBaseEnumClass(this.typeEnumsPackage);
      if (CollectionUtil.isEmpty(classes)) {
        return;
      }
      for (Class<BaseEnum> aClass : classes) {
        BaseEnum[] enumConstants = aClass.getEnumConstants();
        Map<Integer, BaseEnum> map = MapUtil.newHashMap(enumConstants.length);
        for (BaseEnum enumConstant : enumConstants) {
          map.put(enumConstant.getCode(), enumConstant);
        }
        ENUM_MAP.put(aClass, map);
      }
    } catch (IOException e) {
      log.warn("扫描枚举包出现异常:", e);
    }
  }
}
