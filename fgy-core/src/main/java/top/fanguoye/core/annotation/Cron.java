package top.fanguoye.core.annotation;

import top.fanguoye.core.validator.CronValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author：Vance
 * @Description：校验cron表达式
 * @Date：2022/1/15 22:37
 */
@Documented
@Constraint(validatedBy = CronValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
public @interface Cron {

  String message() default "表达式不正确";

  Class<?>[] groups() default { };

  Class<? extends Payload>[] payload() default { };
}
