package top.fanguoye.core.annotation;

import top.fanguoye.core.enums.AlgorithmType;

import java.lang.annotation.*;

/**
 * @Author：Vance
 * @Description：对接口参数解密
 * @Date：2022/1/19 15:47
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.PARAMETER})
public @interface Decrypt {

  AlgorithmType value() default AlgorithmType.AES;
}
