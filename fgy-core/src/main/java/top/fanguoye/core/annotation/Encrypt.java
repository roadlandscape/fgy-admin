package top.fanguoye.core.annotation;

import top.fanguoye.core.enums.AlgorithmType;

import java.lang.annotation.*;

/**
 * @Author：Vance
 * @Description：对接口返回的结果加密
 * @Date：2022/1/19 15:44
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Encrypt {

  AlgorithmType value() default AlgorithmType.AES;
}
