package top.fanguoye.core.annotation;

import top.fanguoye.core.validator.IPValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author：Vance
 * @Description：验证是否为ip,包含ip4和ip6
 * @Date：2022/3/14 10:52
 */
@Documented
@Constraint(validatedBy = IPValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
public @interface IP {

  String message() default "ip格式不正确";

  Class<?>[] groups() default { };

  Class<? extends Payload>[] payload() default { };
}
