package top.fanguoye.core.annotation;

import top.fanguoye.core.validator.MobileValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author：Vance
 * @Description：校验手机号码
 * @Date：2022/1/15 22:37
 */
@Documented
@Constraint(validatedBy = MobileValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
public @interface Mobile {

  String message() default "不是一个合法的手机号码";

  Class<?>[] groups() default { };

  Class<? extends Payload>[] payload() default { };
}
