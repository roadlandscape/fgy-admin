package top.fanguoye.core.annotation;

import top.fanguoye.core.validator.CronValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Author：Vance
 * @Description：验证是否仅包含字母和数字
 * @Date：2022/3/14 10:08
 */
@Documented
@Constraint(validatedBy = CronValidator.class)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Retention(RUNTIME)
public @interface OnlyNumAndChar {

  String message() default "只能包含字母和数字";

  Class<?>[] groups() default { };

  Class<? extends Payload>[] payload() default { };
}
