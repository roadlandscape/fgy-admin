package top.fanguoye.core.annotation;

import java.lang.annotation.*;

/**
 * @Author：Vance
 * @Description：防止重复提交
 * @Date：2022/3/7 11:49
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RepeatSubmit {

  /**
   * 间隔时间(ms)，小于此时间视为重复提交
   */
  int interval() default 5000;

  /**
   * 提示消息
   */
  String message() default "请勿重复提交!";
}
