package top.fanguoye.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Author：Vance
 * @Description：应用配置
 * @Date：2022/6/19 21:24
 */
@Data
@Component
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {

  /** 项目名称 */
  private String name;

  /** url */
  private String url;

  /** 版本 */
  private String version;

  /** 描述 */
  private String description;

  /** 联系人信息 */
  private Contact contact;

  @Data
  public static class Contact {
    private String name;
    private String url;
    private String email;
  }
}
