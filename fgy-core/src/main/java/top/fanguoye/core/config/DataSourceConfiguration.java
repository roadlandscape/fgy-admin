package top.fanguoye.core.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;

/**
 * @Author：Vance
 * @Description：数据源配置 搬运自DataSourceConfiguration
 * @Date：2022/2/22 16:04
 */
@Configuration
public class DataSourceConfiguration {

  @Value("${spring.datasource.primary.driver-class-name}")
  private String primaryDriverClassName;
  @Value("${spring.datasource.primary.url}")
  private String primaryUrl;
  @Value("${spring.datasource.primary.username}")
  private String primaryUsername;
  @Value("${spring.datasource.primary.password}")
  private String primaryPassword;

  @Value("${spring.datasource.secondary.driver-class-name}")
  private String secondaryDriverClassName;
  @Value("${spring.datasource.secondary.url}")
  private String secondaryUrl;
  @Value("${spring.datasource.secondary.username}")
  private String secondaryUsername;
  @Value("${spring.datasource.secondary.password}")
  private String secondaryPassword;

  @Primary
  @Bean
  @ConfigurationProperties(prefix = "spring.datasource.primary.hikari")
  public HikariDataSource primaryDataSource() {
    DataSourceProperties properties = new DataSourceProperties();
    properties.setDriverClassName(primaryDriverClassName);
    properties.setUrl(primaryUrl);
    properties.setUsername(primaryUsername);
    properties.setPassword(primaryPassword);
    HikariDataSource dataSource = createDataSource(properties, HikariDataSource.class);
    if (StringUtils.hasText(properties.getName())) {
      dataSource.setPoolName(properties.getName());
    }
    return dataSource;
  }

  @Primary
  @Bean
  public DataSourceTransactionManager primaryTransactionManager(Environment environment,
                                                                @Qualifier("primaryDataSource") DataSource dataSource,
                                                                ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
    DataSourceTransactionManager transactionManager = createTransactionManager(environment, dataSource);
    transactionManagerCustomizers.ifAvailable((customizers) -> customizers.customize(transactionManager));
    return transactionManager;
  }

  @Bean
  @ConfigurationProperties(prefix = "spring.datasource.secondary.hikari")
  public HikariDataSource secondaryDataSource() {
    DataSourceProperties properties = new DataSourceProperties();
    properties.setDriverClassName(secondaryDriverClassName);
    properties.setUrl(secondaryUrl);
    properties.setUsername(secondaryUsername);
    properties.setPassword(secondaryPassword);
    HikariDataSource dataSource = createDataSource(properties, HikariDataSource.class);
    if (StringUtils.hasText(properties.getName())) {
      dataSource.setPoolName(properties.getName());
    }
    return dataSource;
  }

  @Bean
  public DataSourceTransactionManager secondaryTransactionManager(Environment environment,
                                                                  @Qualifier("secondaryDataSource") DataSource dataSource,
                                                                  ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
    DataSourceTransactionManager transactionManager = createTransactionManager(environment, dataSource);
    transactionManagerCustomizers.ifAvailable((customizers) -> customizers.customize(transactionManager));
    return transactionManager;
  }

  @SuppressWarnings("unchecked")
  protected static <T> T createDataSource(DataSourceProperties properties, Class<? extends DataSource> type) {
    return (T) properties.initializeDataSourceBuilder().type(type).build();
  }

  private DataSourceTransactionManager createTransactionManager(Environment environment, DataSource dataSource) {
    return environment.getProperty("spring.dao.exceptiontranslation.enabled", Boolean.class, Boolean.TRUE)
        ? new JdbcTransactionManager(dataSource) : new DataSourceTransactionManager(dataSource);
  }
}
