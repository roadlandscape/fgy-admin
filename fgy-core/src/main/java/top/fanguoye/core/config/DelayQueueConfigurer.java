package top.fanguoye.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import top.fanguoye.core.entity.DelayTask;
import top.fanguoye.core.entity.TaskEntity;
import top.fanguoye.core.utils.MethodUtils;

import java.util.List;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Executors;

/**
 * @Author：Vance
 * @Description：延时队列配置器
 * @Date：2022/2/28 15:39
 */
@Slf4j
@Component
public class DelayQueueConfigurer implements CommandLineRunner {

  private final DelayQueue<DelayTask> delayQueue = new DelayQueue<>();

  /**
   * 加入延时任务
   * @param task
   */
  public void put(TaskEntity task) {
    log.info("加入延时任务：{}", task);
    DelayTask delayTask = new DelayTask(task);
    // 先删除再加入，预防队列中已存在相同的任务
    delayQueue.remove(delayTask);
    delayQueue.put(delayTask);
  }

  /**
   * 取消延时任务
   * @param task
   */
  public void remove(TaskEntity task) {
    log.info("取消延时任务：{}", task);
    delayQueue.remove(new DelayTask(task));
  }

  /**
   * 取消延时任务
   * @param tasks
   */
  public void remove(List<TaskEntity> tasks) {
    for (TaskEntity task : tasks) {
      remove(task);
    }
  }

  /**
   * 取消延时任务
   * @param group
   * @param uniqueKey
   */
  public void remove(String group, String uniqueKey) {
    TaskEntity taskEntity = new TaskEntity();
    taskEntity.setGroup(group).setUniqueKey(uniqueKey);
    remove(taskEntity);
  }

  @Override
  public void run(String... args) throws Exception {
    log.info("初始化延时任务.");
    Executors.newSingleThreadExecutor().execute(new Thread(this::executeThread));
  }

  /**
   * 延时任务执行线程
   */
  private void executeThread() {
    while (true) {
      try {
        DelayTask task = delayQueue.take();
        log.info("执行延时任务：{}", task);
        MethodUtils.invokeMethod(task.getTaskEntity().getInvokeMethod());
      } catch (InterruptedException e) {
        log.error("获取延时任务异常 - ", e);
      } catch (Exception e) {
        log.error("任务执行异常 - ", e);
      }
    }
  }
}
