package top.fanguoye.core.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.*;
import com.fasterxml.jackson.datatype.jsr310.ser.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.fanguoye.core.constant.CalendarConstants;
import top.fanguoye.core.interfaces.BaseEnum;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Author：Vance
 * @Description：Jackson配置
 * @Date：2021/12/14 22:06
 */
@Configuration
public class JacksonObjectMapper {

  /**
   * Json序列化和反序列化转换器
   * @return
   */
  @Bean
  public ObjectMapper objectMapper() {
    final ObjectMapper objectMapper = new ObjectMapper();
    // 禁止时间以时间戳的形式序列化
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    // 在对ZonedDateTime进行反序列化期间,Jackson会将解析的时区调整为上下文提供的时区，修改此行为,以使解析的ZonedDateTime保持在Z
    objectMapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
    // 将空字符串反序列化为 null 此设置只能用于 POJO 或一些结构化的值（比如Map、Collection、枚举），但不能用于标量值（scalar values ，比如 boolean 和 number）
    objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
    // 当json中存在bean中没有的字段时，忽略掉，而不是报错
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    JavaTimeModule javaTimeModule = new JavaTimeModule();
    javaTimeModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(CalendarConstants.DATE_TIME_FORMAT)));
    javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern(CalendarConstants.DATE_FORMAT)));
    javaTimeModule.addSerializer(LocalTime.class, new LocalTimeSerializer(DateTimeFormatter.ofPattern(CalendarConstants.TIME_FORMAT)));
    javaTimeModule.addSerializer(MonthDay.class, new MonthDaySerializer(DateTimeFormatter.ofPattern(CalendarConstants.MONTH_DAY_FORMAT)));
    javaTimeModule.addSerializer(YearMonth.class, new YearMonthSerializer(DateTimeFormatter.ofPattern(CalendarConstants.YEAR_MONTH_FORMAT)));
    javaTimeModule.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(CalendarConstants.DATE_TIME_FORMAT)));
    javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern(CalendarConstants.DATE_FORMAT)));
    javaTimeModule.addDeserializer(LocalTime.class, new LocalTimeDeserializer(DateTimeFormatter.ofPattern(CalendarConstants.TIME_FORMAT)));
    javaTimeModule.addDeserializer(MonthDay.class, new MonthDayDeserializer(DateTimeFormatter.ofPattern(CalendarConstants.MONTH_DAY_FORMAT)));
    javaTimeModule.addDeserializer(YearMonth.class, new YearMonthDeserializer(DateTimeFormatter.ofPattern(CalendarConstants.YEAR_MONTH_FORMAT)));

    javaTimeModule.addSerializer(Date.class, new JsonSerializer<Date>() {
      @Override
      public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        SimpleDateFormat formatter = new SimpleDateFormat(CalendarConstants.DATE_TIME_FORMAT);
        String formattedDate = formatter.format(date);
        jsonGenerator.writeString(formattedDate);
      }
    });
    javaTimeModule.addDeserializer(Date.class, new JsonDeserializer<Date>() {
      @Override
      public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        SimpleDateFormat format = new SimpleDateFormat(CalendarConstants.DATE_TIME_FORMAT);
        String date = jsonParser.getText();
        try {
          return format.parse(date);
        } catch (ParseException e) {
          throw new RuntimeException(e);
        }
      }
    });

    SimpleModule simpleModule = new SimpleModule();
    // 配置 Long类型 使用 字符串 序列化，解决雪花算法ID传到前端之后精度丢失的问题（影响：系统中所有Long类型json序列化后都变成了字符串）
    simpleModule.addSerializer(Long.class, ToStringSerializer.instance);
    // 配置实现 BaseEnum 接口的枚举序列化
    simpleModule.addSerializer(BaseEnum.class, new JsonSerializer<BaseEnum>() {
      @Override
      public void serialize(BaseEnum baseEnum, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(baseEnum.getCode());
      }
    });
    objectMapper.registerModules(javaTimeModule, simpleModule);
    return objectMapper;
  }
}
