package top.fanguoye.core.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.task.TaskSchedulerBuilder;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;
import top.fanguoye.core.entity.TaskEntity;
import top.fanguoye.core.utils.MethodUtils;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ScheduledFuture;
import java.util.stream.Collectors;

/**
 * @Author：Vance
 * @Description：动态定时任务配置器
 * @Date：2022/2/21 16:05
 */
@Slf4j
@Component
public class MySchedulingConfigurer implements SchedulingConfigurer {

  private final TaskSchedulerBuilder taskSchedulerBuilder;

  private final ConcurrentMap<String, ScheduledFuture<?>> scheduledFutures = new ConcurrentHashMap<>();
  private ScheduledTaskRegistrar registrar;

  public MySchedulingConfigurer(TaskSchedulerBuilder taskSchedulerBuilder) {
    this.taskSchedulerBuilder = taskSchedulerBuilder;
  }

  @Override
  public void configureTasks(ScheduledTaskRegistrar registrar) {
    // 设置线程核心数等（taskSchedulerBuilder已经设置了配置文件spring.task的配置），默认单线程,如果不设置的话，不能同时并发执行任务
    ThreadPoolTaskScheduler taskScheduler = taskSchedulerBuilder.build();
    taskScheduler.initialize();
    registrar.setScheduler(taskScheduler);
    this.registrar = registrar;
  }

  /**
   * 新增/修改任务
   * @param taskEntity
   */
  public void saveTask(TaskEntity taskEntity) {
    delTask(taskEntity);
    CronTask cronTask = getCronTask(taskEntity);
    // 执行业务
    ScheduledFuture<?> future = registrar.getScheduler().schedule(cronTask.getRunnable(), cronTask.getTrigger());
    scheduledFutures.put(taskEntity.getGroup() + taskEntity.getUniqueKey(), future);
  }

  /**
   * 获取CronTask(设置具体处理业务逻辑的地方)
   * @param taskEntity
   * @return
   */
  private CronTask getCronTask(TaskEntity taskEntity)  {
    return new CronTask(() -> {
      try {
        MethodUtils.invokeMethod(taskEntity.getInvokeMethod());
      } catch (Exception e) {
        log.error("任务执行异常 - ", e);
      }
    }, taskEntity.getCronExpression());
  }

  /**
   * 通过 group 删除任务
   * @param group
   */
  public void delTaskByGroup(String group) {
    Set<String> keys = scheduledFutures.keySet();
    Set<String> mathKeys = keys.stream().filter(key -> key.startsWith(group)).collect(Collectors.toSet());
    for (String key : mathKeys) {
      // 让当前线程，执行完再结束。
      scheduledFutures.get(key).cancel(false);
      scheduledFutures.remove(key);
    }
  }

  /**
   * 通过 key 删除任务，key = group + uniqueKey
   * @param taskEntity
   */
  public void delTask(TaskEntity taskEntity) {
    ScheduledFuture<?> future = scheduledFutures.get(taskEntity.getGroup() + taskEntity.getUniqueKey());
    if (future != null) {
      // 让当前线程，执行完再结束。
      future.cancel(false);
      scheduledFutures.remove(taskEntity.getGroup() + taskEntity.getUniqueKey());
    }
  }

  /**
   * 通过 key 删除任务，key = group + uniqueKey
   * @param taskEntities
   */
  public void delTask(List<TaskEntity> taskEntities) {
    List<String> keys = taskEntities.stream()
        .map(taskEntity -> taskEntity.getGroup() + taskEntity.getUniqueKey())
        .collect(Collectors.toList());
    Set<String> taskKeys = scheduledFutures.keySet();
    for (String key : keys) {
      if (taskKeys.contains(key)) {
        // 让当前线程，执行完再结束。
        scheduledFutures.get(key).cancel(false);
        scheduledFutures.remove(key);
      }
    }
  }
}
