package top.fanguoye.core.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.extension.MybatisMapWrapperFactory;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.fanguoye.core.constant.Constants;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：Vance
 * @Description：mybatis-plus配置
 * @Date：2021/12/14 17:39
 */
@Configuration
public class MybatisPlusConfig {

  /**
   * MybatisPlus插件
   * @return
   */
  @Bean
  public MybatisPlusInterceptor mybatisPlusInterceptor() {
    MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
    // 分页插件
    PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
    // 溢出页数跳回第一页
    paginationInnerInterceptor.setOverflow(true);
    // 单页分页条数限制
    paginationInnerInterceptor.setMaxLimit(Constants.PAGINATION_MAX_LIMIT);

    // 防全表更新与删除插件
    BlockAttackInnerInterceptor blockAttackInnerInterceptor = new BlockAttackInnerInterceptor();

    List<InnerInterceptor> interceptors = new ArrayList<>();
    interceptors.add(paginationInnerInterceptor);
    interceptors.add(blockAttackInnerInterceptor);
    interceptor.setInterceptors(interceptors);
    return interceptor;
  }

  /**
   * 开启返回map结果集的下划线转驼峰
   * @return
   */
  @Bean
  public ConfigurationCustomizer mybatisPlusCustomizer(){
    return configuration -> configuration.setObjectWrapperFactory(new MybatisMapWrapperFactory());
  }

  @Bean
  @ConfigurationProperties(prefix = com.baomidou.mybatisplus.core.toolkit.Constants.MYBATIS_PLUS)
  public MybatisPlusProperties mybatisPlusPrimaryProperties() {
    return new MybatisPlusProperties();
  }

  @Bean
  @ConfigurationProperties(prefix = com.baomidou.mybatisplus.core.toolkit.Constants.MYBATIS_PLUS)
  public MybatisPlusProperties mybatisPlusSecondaryProperties() {
    return new MybatisPlusProperties();
  }
}
