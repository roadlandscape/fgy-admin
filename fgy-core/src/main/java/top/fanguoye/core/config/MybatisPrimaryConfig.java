package top.fanguoye.core.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.autoconfigure.MybatisPlusProperties;
import com.baomidou.mybatisplus.autoconfigure.SpringBootVFS;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.mapping.DatabaseIdProvider;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.type.TypeHandler;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * @Author：Vance
 * @Description：Primary数据源MyBatis配置
 * @Date：2022/2/22 16:10
 */
@Configuration
@MapperScan(basePackages = "top.fanguoye.*.mapper", sqlSessionFactoryRef = "sqlSessionFactoryPrimary")
public class MybatisPrimaryConfig {

  private final ResourceLoader resourceLoader;
  private final Interceptor[] interceptors;
  private final TypeHandler[] typeHandlers;
  private final LanguageDriver[] languageDrivers;
  private final List<ConfigurationCustomizer> configurationCustomizers;
  private final DatabaseIdProvider databaseIdProvider;
  private final MybatisPlusProperties properties;
  private final ApplicationContext applicationContext;

  public MybatisPrimaryConfig(ResourceLoader resourceLoader,
                              ObjectProvider<Interceptor[]> interceptorsProvider,
                              ObjectProvider<TypeHandler[]> typeHandlersProvider,
                              ObjectProvider<LanguageDriver[]> languageDriversProvider,
                              ObjectProvider<List<ConfigurationCustomizer>> configurationCustomizersProvider,
                              ObjectProvider<DatabaseIdProvider> databaseIdProvider,
                              ApplicationContext applicationContext,
                              @Qualifier("mybatisPlusPrimaryProperties") MybatisPlusProperties properties) {
    this.resourceLoader = resourceLoader;
    this.interceptors = interceptorsProvider.getIfAvailable();
    this.typeHandlers = typeHandlersProvider.getIfAvailable();
    this.languageDrivers = languageDriversProvider.getIfAvailable();
    this.configurationCustomizers = configurationCustomizersProvider.getIfAvailable();
    this.databaseIdProvider = databaseIdProvider.getIfAvailable();
    this.properties = properties;
    this.applicationContext = applicationContext;
  }

  @Bean
  public SqlSessionFactory sqlSessionFactoryPrimary(@Qualifier("primaryDataSource") DataSource primaryDataSource) throws Exception {
    // MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
    // factoryBean.setDataSource(this.primaryDataSource);
    // return factoryBean.getObject();

    /*
     * 下面代码搬运自 MybatisPlusAutoConfiguration.sqlSessionFactory，未做任何删改
     * 升级MybatisPlus版本，如果以下代码有变更，需要重新粘贴过来
     * 这样做的目的是确保 MybatisPlusProperties 所有配置能正常生效
     */
    // TODO 使用 MybatisSqlSessionFactoryBean 而不是 SqlSessionFactoryBean
    MybatisSqlSessionFactoryBean factory = new MybatisSqlSessionFactoryBean();
    factory.setDataSource(primaryDataSource);
    factory.setVfs(SpringBootVFS.class);
    if (StringUtils.hasText(this.properties.getConfigLocation())) {
      factory.setConfigLocation(this.resourceLoader.getResource(this.properties.getConfigLocation()));
    }
    applyConfiguration(factory);
    if (this.properties.getConfigurationProperties() != null) {
      factory.setConfigurationProperties(this.properties.getConfigurationProperties());
    }
    if (!ObjectUtils.isEmpty(this.interceptors)) {
      factory.setPlugins(this.interceptors);
    }
    if (this.databaseIdProvider != null) {
      factory.setDatabaseIdProvider(this.databaseIdProvider);
    }
    if (StringUtils.hasLength(this.properties.getTypeAliasesPackage())) {
      factory.setTypeAliasesPackage(this.properties.getTypeAliasesPackage());
    }
    if (this.properties.getTypeAliasesSuperType() != null) {
      factory.setTypeAliasesSuperType(this.properties.getTypeAliasesSuperType());
    }
    if (StringUtils.hasLength(this.properties.getTypeHandlersPackage())) {
      factory.setTypeHandlersPackage(this.properties.getTypeHandlersPackage());
    }
    if (!ObjectUtils.isEmpty(this.typeHandlers)) {
      factory.setTypeHandlers(this.typeHandlers);
    }
    Resource[] mapperLocations = this.properties.resolveMapperLocations();
    if (!ObjectUtils.isEmpty(mapperLocations)) {
      factory.setMapperLocations(mapperLocations);
    }
    // TODO 修改源码支持定义 TransactionFactory
    this.getBeanThen(TransactionFactory.class, factory::setTransactionFactory);

    // TODO 对源码做了一定的修改(因为源码适配了老旧的mybatis版本,但我们不需要适配)
    Class<? extends LanguageDriver> defaultLanguageDriver = this.properties.getDefaultScriptingLanguageDriver();
    if (!ObjectUtils.isEmpty(this.languageDrivers)) {
      factory.setScriptingLanguageDrivers(this.languageDrivers);
    }
    Optional.ofNullable(defaultLanguageDriver).ifPresent(factory::setDefaultScriptingLanguageDriver);

    // TODO 自定义枚举包
    if (StringUtils.hasLength(this.properties.getTypeEnumsPackage())) {
      factory.setTypeEnumsPackage(this.properties.getTypeEnumsPackage());
    }
    // TODO 此处必为非 NULL
    GlobalConfig globalConfig = this.properties.getGlobalConfig();
    // TODO 注入填充器
    this.getBeanThen(MetaObjectHandler.class, globalConfig::setMetaObjectHandler);
    // TODO 注入主键生成器
    this.getBeansThen(IKeyGenerator.class, i -> globalConfig.getDbConfig().setKeyGenerators(i));
    // TODO 注入sql注入器
    this.getBeanThen(ISqlInjector.class, globalConfig::setSqlInjector);
    // TODO 注入ID生成器
    this.getBeanThen(IdentifierGenerator.class, globalConfig::setIdentifierGenerator);
    // TODO 设置 GlobalConfig 到 MybatisSqlSessionFactoryBean
    factory.setGlobalConfig(globalConfig);
    return factory.getObject();
  }

  /**
   * 搬运自 MybatisPlusAutoConfiguration.sqlSessionTemplate
   * @param sqlSessionFactoryPrimary
   * @return
   */
  @Bean
  public SqlSessionTemplate sqlSessionTemplatePrimary(SqlSessionFactory sqlSessionFactoryPrimary) {
    ExecutorType executorType = this.properties.getExecutorType();
    if (executorType != null) {
      return new SqlSessionTemplate(sqlSessionFactoryPrimary, executorType);
    } else {
      return new SqlSessionTemplate(sqlSessionFactoryPrimary);
    }
  }

  /**
   * 检查spring容器里是否有对应的bean,有则进行消费
   *
   * @param clazz    class
   * @param consumer 消费
   * @param <T>      泛型
   */
  private <T> void getBeansThen(Class<T> clazz, Consumer<List<T>> consumer) {
    if (this.applicationContext.getBeanNamesForType(clazz, false, false).length > 0) {
      final Map<String, T> beansOfType = this.applicationContext.getBeansOfType(clazz);
      List<T> clazzList = new ArrayList<>();
      beansOfType.forEach((k, v) -> clazzList.add(v));
      consumer.accept(clazzList);
    }
  }

  /**
   * 检查spring容器里是否有对应的bean,有则进行消费
   *
   * @param clazz    class
   * @param consumer 消费
   * @param <T>      泛型
   */
  private <T> void getBeanThen(Class<T> clazz, Consumer<T> consumer) {
    if (this.applicationContext.getBeanNamesForType(clazz, false, false).length > 0) {
      consumer.accept(this.applicationContext.getBean(clazz));
    }
  }

  private void applyConfiguration(MybatisSqlSessionFactoryBean factory) {
    // TODO 使用 MybatisConfiguration
    MybatisConfiguration configuration = this.properties.getConfiguration();
    if (configuration == null && !StringUtils.hasText(this.properties.getConfigLocation())) {
      configuration = new MybatisConfiguration();
    }
    if (configuration != null && !CollectionUtils.isEmpty(this.configurationCustomizers)) {
      for (ConfigurationCustomizer customizer : this.configurationCustomizers) {
        customizer.customize(configuration);
      }
    }
    factory.setConfiguration(configuration);
  }
}
