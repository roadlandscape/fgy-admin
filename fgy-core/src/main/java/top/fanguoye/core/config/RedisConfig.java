package top.fanguoye.core.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import top.fanguoye.core.interfaces.BaseEnum;

import java.io.IOException;

/**
 * @Author：Vance
 * @Description：redis config
 * @Date：2022/1/9 23:39
 */
@Configuration
@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {

  @Bean
  public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    // 配置连接工厂
    template.setConnectionFactory(connectionFactory);

    // 定义Jackson2JsonRedisSerializer序列化对象
    Jackson2JsonRedisSerializer<Object> jacksonSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
    ObjectMapper om = new ObjectMapper();
    SimpleModule simpleModule = new SimpleModule();
    // 配置实现 BaseEnum 接口的枚举序列化
    simpleModule.addSerializer(BaseEnum.class, new JsonSerializer<BaseEnum>() {
      @Override
      public void serialize(BaseEnum baseEnum, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeNumber(baseEnum.getCode());
      }
    });
    om.registerModules(simpleModule);
    // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
    om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
    // 序列化时允许非常量字段均输出类型，序列化时输出类型，为了反序列化时使用
    om.activateDefaultTyping(LaissezFaireSubTypeValidator.instance,
        ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
    jacksonSerializer.setObjectMapper(om);

    // 使用StringRedisSerializer来序列化和反序列化redis的key值
    template.setKeySerializer(RedisSerializer.string());
    // 使用 JSON 序列化方式，序列化 VALUE
    template.setValueSerializer(jacksonSerializer);

    // Hash的key也采用StringRedisSerializer的序列化方式
    template.setHashKeySerializer(RedisSerializer.string());
    // 使用 JSON 序列化方式，序列化 VALUE
    template.setHashValueSerializer(jacksonSerializer);

    template.afterPropertiesSet();
    return template;
  }
}
