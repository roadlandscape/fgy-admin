package top.fanguoye.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * @Author：Vance
 * @Description：线程池配置
 * @Date：2022/2/16 17:54
 */
@EnableAsync
@Configuration
public class TaskPoolConfig {

  // @Bean
  public ThreadPoolTaskExecutor taskExecutor1() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    // 线程池创建时的初始化线程数
    executor.setCorePoolSize(8);
    // 线程池的最大线程数
    executor.setMaxPoolSize(15);
    // 用来缓冲执行任务的队列
    executor.setQueueCapacity(30);
    // 线程终止前允许保持空闲的时间
    executor.setKeepAliveSeconds(60);
    // 是否允许核心线程超时
    executor.setAllowCoreThreadTimeOut(true);
    // 是否等待剩余任务完成后才关闭应用
    executor.setWaitForTasksToCompleteOnShutdown(true);
    // 应用关闭时，等待剩余任务完成的最大时间
    executor.setAwaitTerminationSeconds(60);
    // 线程名的前缀
    executor.setThreadNamePrefix("executor-1-");
    // AbortPolicy策略：默认策略，如果线程池队列满了丢掉这个任务并且抛出RejectedExecutionException异常。
    // DiscardPolicy策略：如果线程池队列满了，会直接丢掉这个任务并且不会有任何异常。
    // DiscardOldestPolicy策略：如果队列满了，会将最早进入队列的任务删掉腾出空间，再尝试加入队列。
    // CallerRunsPolicy策略：如果添加到线程池失败，那么主线程会自己去执行该任务，不会等待线程池中的线程去执行。
    executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    return executor;
  }

  // @Bean
  public ThreadPoolTaskExecutor taskExecutor2() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(8);
    executor.setMaxPoolSize(15);
    executor.setQueueCapacity(30);
    executor.setKeepAliveSeconds(60);
    executor.setAllowCoreThreadTimeOut(true);
    executor.setWaitForTasksToCompleteOnShutdown(true);
    executor.setAwaitTerminationSeconds(60);
    executor.setThreadNamePrefix("executor-2-");
    executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    return executor;
  }
}
