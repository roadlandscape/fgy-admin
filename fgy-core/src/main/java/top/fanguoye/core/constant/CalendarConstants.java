package top.fanguoye.core.constant;

/**
 * @Author：Vance
 * @Description：日期时间常量
 * @Date：2021/12/14 22:14
 */
public interface CalendarConstants {

  /** 日期时间格式 */
  String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  /** 日期格式1 */
  String DATE_FORMAT = "yyyy-MM-dd";
  /** 日期格式2 */
  String DATE_FORMAT2 = "yyyyMMdd";
  /** 时间格式 */
  String TIME_FORMAT = "HH:mm:ss";
  /** 月日格式 */
  String MONTH_DAY_FORMAT = "MM-dd";
  /** 年月格式 */
  String YEAR_MONTH_FORMAT = "yyyy-MM";

  /** cron表达式格式，包含年月 */
  String CRON_EXPRESSION1 = "ss mm HH dd MM ?";
  /** cron表达式格式，不包含年月 */
  String CRON_EXPRESSION2 = "ss mm HH * * ?";
}
