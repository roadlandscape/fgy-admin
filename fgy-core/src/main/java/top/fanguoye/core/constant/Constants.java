package top.fanguoye.core.constant;

/**
 * @Author：Vance
 * @Description：通用常量信息
 * @Date：2021/12/14 10:39
 */
public interface Constants {

    /**
     * 定时任务白名单配置（仅允许访问的包名，如其他需要可以自行添加）
     */
    String[] JOB_WHITELIST_STR = { "top.fanguoye" };

    /**
     * 定时任务默认分组
     */
    String DEFAULT_GROUP = "默认";

    /**
     * UTF-8 字符集
     */
    String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    String GBK = "GBK";

    /**
     * http请求
     */
    String HTTP = "http://";

    /**
     * https请求
     */
    String HTTPS = "https://";

    /**
     * 支持的图片类型
     */
    String[] IMAGE_TYPE = { "jpg", "png", "gif" };

    /**
     * 单页分页条数限制
     */
    long PAGINATION_MAX_LIMIT = 1000L;

    /**
     * sql限制只查询一条
     */
    String LIMIT1 = "limit 1";

    /**
     * 父子节点中，不存在父节点时默认填充值
     */
    long DEFAULT_PARENT_NODE = 0L;

    /**
     * 关联另一张表时，外键允许临时为空的填充值
     */
    long TMP_FOREIGN_KEY = -1L;

    /**
     * 管理员标识
     */
    String ADMIN_SIGN = "admin";

    /**
     * 管理员权限标识
     */
    String ADMIN_PERMS_SIGN = "*:*:*";

    /**
     * 生产环境变量
     */
    String PRO_ENVI_VAR = "pro";

    /**
     * 头像访问路径前缀
     */
    String AVATAR_PATH = "/file/avatar/";
}