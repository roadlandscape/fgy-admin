package top.fanguoye.core.constant;

/**
 * @Author：Vance
 * @Description：redis key
 * @Date：2022/1/2 16:44
 */
public interface RedisConstants {

  /**
   * 登录用户信息 key
   */
  String LOGIN_INFO_KEY = "login_info:";

  /**
   * 微信基础支持的access_token key
   */
  String WECHAT_BASE_ACCESS_TOKEN = "wechat:base_access_token";

  /**
   * 防止重复提交 key
   */
  String REPEAT_SUBMIT_KEY = "repeat_submit:";
}
