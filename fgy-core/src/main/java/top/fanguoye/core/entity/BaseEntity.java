package top.fanguoye.core.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import top.fanguoye.core.interfaces.Update;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：除了 多对多关联中间表的实体 外，其他数据库实体类都必须继承BaseEntity
 * @Date：2021/12/13 16:24
 */
@Getter
@Setter
@Accessors(chain = true)
public class BaseEntity implements Serializable {

  private static final long serialVersionUID = 2L;

  @TableId(type = IdType.ASSIGN_ID)
  @NotNull(groups = Update.class)
  private Long id;
  @ApiModelProperty("创建人")
  @TableField(fill = FieldFill.INSERT)
  private Long createBy;
  @ApiModelProperty("创建时间")
  @OrderBy
  @TableField(fill = FieldFill.INSERT)
  private LocalDateTime createTime;
  @ApiModelProperty("修改人")
  @TableField(fill = FieldFill.UPDATE)
  private Long updateBy;
  @ApiModelProperty("修改时间")
  @TableField(fill = FieldFill.UPDATE)
  private LocalDateTime updateTime;

  @ApiModelProperty("创建人姓名")
  @TableField(fill = FieldFill.INSERT)
  private String creator;
  @ApiModelProperty("修改人姓名")
  @TableField(fill = FieldFill.UPDATE)
  private String updater;

  public static final String ID = "id";
  public static final String CREATE_BY = "create_by";
  public static final String CREATE_TIME = "create_time";
  public static final String UPDATE_BY = "update_by";
  public static final String UPDATE_TIME = "update_time";
  public static final String CREATOR = "creator";
  public static final String UPDATER = "updater";

  /**
   * 清空不允许前端更新的字段值
   */
  public void clearForbidUpdateField() {
    this.setCreateBy(null)
        .setCreator(null)
        .setCreateTime(null)
        .setUpdateBy(null)
        .setUpdater(null)
        .setUpdateTime(null);
  }
}
