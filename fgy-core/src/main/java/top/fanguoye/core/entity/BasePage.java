package top.fanguoye.core.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Setter;

/**
 * @Author：Vance
 * @Description：分页基础，所有需要分页的dto统一继承
 * @Date：2021/12/14 22:54
 */
@Setter
public class BasePage {

  @ApiModelProperty("当前页")
  private Long current;
  @ApiModelProperty("每页显示条数，默认 20")
  private Long size;
  @ApiModelProperty("是否进行 count 查询")
  private Boolean searchCount;

  public Long getCurrent() {
    if (current == null || current <= 0) {
      return 1L;
    }
    return current;
  }

  public Long getSize() {
    if (size == null) {
      return 20L;
    }
    return size;
  }

  public Boolean getSearchCount() {
    if (searchCount == null) {
      return true;
    }
    return searchCount;
  }
}
