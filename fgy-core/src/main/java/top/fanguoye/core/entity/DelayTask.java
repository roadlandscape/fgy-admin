package top.fanguoye.core.entity;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @Author：Vance
 * @Description：延时任务
 * @Date：2022/2/28 15:00
 */
public class DelayTask implements Delayed {

  private final long expire;
  private final TaskEntity taskEntity;

  public DelayTask(TaskEntity taskEntity) {
    super();
    this.taskEntity = taskEntity;
    this.expire = taskEntity.getDelayTime() == null ? 0 : taskEntity.getDelayTime() + System.currentTimeMillis();
  }

  @Override
  public long getDelay(TimeUnit unit) {
    return unit.convert(this.expire - System.currentTimeMillis(), unit);
  }

  @Override
  public int compareTo(Delayed o) {
    long time = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
    return (int) time;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    DelayTask delayTask = (DelayTask) o;
    return (this.taskEntity.getGroup() + this.taskEntity.getUniqueKey())
        .equals(delayTask.getTaskEntity().getGroup() + delayTask.getTaskEntity().getUniqueKey());
  }

  public long getExpire() {
    return expire;
  }

  public TaskEntity getTaskEntity() {
    return taskEntity;
  }

  @Override
  public String toString() {
    return "DelayTask{" +
        "expire=" + expire +
        ", taskEntity=" + taskEntity +
        '}';
  }
}
