package top.fanguoye.core.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.Operator;

import java.io.Serializable;

/**
 * @Author：Vance
 * @Description：统一过滤字段实体
 * @Date：2021/12/14 15:35
 */
@ApiModel(value = "过滤字段实体")
@Data
@Accessors(chain = true)
public class FilterCondition implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "筛选字段名")
  private String field;
  @ApiModelProperty(value = "筛选类型")
  private Operator type;
  @ApiModelProperty(value = "筛选字段值1，根据筛选类型来决定是否赋值")
  private Object value1;
  @ApiModelProperty(value = "筛选字段值2，根据筛选类型来决定是否赋值")
  private Object value2;

}
