package top.fanguoye.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：Vance
 * @Description：法定假节日dto
 * @Date：2022/6/30 10:27
 */
@Data
@Accessors(chain = true)
public class HolidayDto implements Serializable {

  private static final long serialVersionUID = 1L;

  private String $schema;
  private String $id;
  private String year;
  private String[] papers;
  private List<DayObj> days;

  public String get$schema() {
    return $schema;
  }

  public void set$schema(String $schema) {
    this.$schema = $schema;
  }

  public String get$id() {
    return $id;
  }

  public void set$id(String $id) {
    this.$id = $id;
  }

  @Data
  public static class DayObj {

    /** 节日名称 */
    private String name;
    /** 日期 */
    private String date;
    /** 是否是休息日 */
    private Boolean isOffDay;
  }
}
