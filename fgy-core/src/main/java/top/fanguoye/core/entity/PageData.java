package top.fanguoye.core.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：Vance
 * @Description：分页数据
 * @Date：2021/12/14 17:28
 */
@ApiModel(value = "分页数据")
@Data
@Accessors(chain = true)
public class PageData<T> implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("当前页")
  private long current;
  @ApiModelProperty("每页显示条数")
  private long size;
  @ApiModelProperty("总记录数")
  private long total;
  @ApiModelProperty("总页数")
  private long pages;
  @ApiModelProperty("数据列表")
  private List<T> records;
}
