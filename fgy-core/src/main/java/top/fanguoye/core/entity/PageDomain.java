package top.fanguoye.core.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：Vance
 * @Description：分页条件
 * @Date：2021/12/14 17:22
 */
@ApiModel(value = "分页条件")
@Data
@Accessors(chain = true)
public class PageDomain extends BasePage implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("过滤字段信息")
  private List<FilterCondition> filterConditions;
  @ApiModelProperty("排序字段信息")
  private List<SortCondition> sortConditions;
}
