package top.fanguoye.core.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：Vance
 * @Description：统一查询条件实体
 * @Date：2021/12/14 16:20
 */
@ApiModel(value = "查询条件实体")
@Data
@Accessors(chain = true)
public class QueryCondition implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("过滤字段")
  private List<FilterCondition> filterConditions;
  @ApiModelProperty("排序字段")
  private List<SortCondition> sortConditions;
}
