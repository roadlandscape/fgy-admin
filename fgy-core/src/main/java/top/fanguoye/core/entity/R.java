package top.fanguoye.core.entity;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.HttpStatus;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：请求响应统一数据格式
 * @Date：2021/12/14 10:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class R<T> implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final Map<String, String> MSG_MAP = new HashMap<String, String>() {{
    put("userAccount", "账号");
    put("password", "密码");
  }};

  private int code;
  private String msg;
  private T data;

  public static R ok() {
    return R.resp(HttpStatus.SUCCESS);
  }

  public static R ok(String msg) {
    return R.resp(HttpStatus.SUCCESS.getCode(), msg);
  }

  public static R ok(Object data) {
    return R.resp(HttpStatus.SUCCESS, data);
  }

  public static R ok(String msg, Object data) {
    return R.resp(HttpStatus.SUCCESS.getCode(), msg, data);
  }

  public static R error() {
    return R.resp(HttpStatus.ERROR);
  }

  public static R error(String msg) {
    return R.resp(HttpStatus.ERROR.getCode(), msg);
  }

  public static R resp(int code) {
    return new R().setCode(code);
  }

  public static R resp(int code, String msg) {
    return R.resp(code, msg, null);
  }

  public static R resp(int code, Object data) {
    return new R(code, null, data);
  }

  public static R resp(int code, String msg, Object data) {
    if (StrUtil.isNotEmpty(msg)) {
      Set<Map.Entry<String, String>> entries = MSG_MAP.entrySet();
      for (Map.Entry<String, String> entry : entries) {
        msg = msg.replace(entry.getKey(), entry.getValue());
      }
    }

    return new R(code, msg, data);
  }

  public static R resp(HttpStatus httpStatus) {
    return R.resp(httpStatus, null);
  }

  public static R resp(HttpStatus httpStatus, Object data) {
    return R.resp(httpStatus.getCode(), httpStatus.getMsg(), data);
  }
}