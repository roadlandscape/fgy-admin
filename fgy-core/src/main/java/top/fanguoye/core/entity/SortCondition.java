package top.fanguoye.core.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.SortType;

import java.io.Serializable;

/**
 * @Author：Vance
 * @Description：统一排序字段实体
 * @Date：2021/12/14 15:55
 */
@ApiModel(value = "排序字段实体")
@Data
@Accessors(chain = true)
public class SortCondition implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty(value = "排序字段")
  private String field;
  @ApiModelProperty(value = "排序类型")
  private SortType type;

}
