package top.fanguoye.core.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author：Vance
 * @Description：任务Entity
 * @Date：2022/2/21 17:51
 */
@Data
@Accessors(chain = true)
public class TaskEntity implements Serializable {

  private static final long serialVersionUID = 2L;

  /** 任务唯一标识 */
  private String uniqueKey;
  /** 分组 */
  private String group;
  /** 任务描述 */
  private String desc;
  /** cron表达式（定时任务[触发多次]中使用） */
  private String cronExpression;
  /** 延时时长，单位毫秒（延时任务[只触发一次]中使用） */
  private Long delayTime;
  /** 调用方法 */
  private String invokeMethod;
}
