package top.fanguoye.core.enums;

/**
 * @Author：Vance
 * @Description：算法类型
 * @Date：2022/1/19 20:34
 */
public enum AlgorithmType {

  AES,
  RSA
}
