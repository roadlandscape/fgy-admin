package top.fanguoye.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

/**
 * @Author：Vance
 * @Description：聊天类型
 * @Date：2022/3/18 10:24
 */
public enum ChatType implements BaseEnum {

  SINGLE(1, "单聊"),
  GROUP(2, "群聊");

  @EnumValue
  private final int code;
  private final String desc;

  ChatType(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  @Override
  public int getCode() {
    return code;
  }

  @JsonCreator
  public static ChatType getEnum(Integer code) {
    return ClassUtils.getEnum(code, ChatType.class);
  }
}
