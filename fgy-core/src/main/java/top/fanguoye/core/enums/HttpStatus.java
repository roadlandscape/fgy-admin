package top.fanguoye.core.enums;

/**
 * @Author：Vance
 * @Description：http状态码
 * @Date：2021/12/14 10:48
 */
public enum HttpStatus {

  SUCCESS(200, "操作成功!"),
  UNAUTHORIZED(401, "凭证认证失败!"),
  FORBIDDEN(403, "没有权限操作，请联系管理员授权!"),
  ABSENT(404, "请求url不存在!"),
  ERROR(500, "服务器错误!"),
  UNAVAILABLE(503, "系统繁忙!"),

  DATA_NOT_FOUND(600, "数据获取失败!"),
  DATA_INSERT_FAILED(601, "数据插入失败!"),
  DATA_DELETE_FAILED(602, "数据删除失败!"),
  DATA_UPDATE_FAILED(603, "数据更新失败!"),
  DATA_REPEAT_COMMIT(604, "请勿重复提交!"),

  AUTH_ACCOUNT_EXIST(701, "账号已存在!"),
  AUTH_USER_PASSWORD(702, "用户账号或密码不正确!"),
  AUTH_CAPTCHA(703, "验证码不正确!"),
  AUTH_ACCOUNT_LOCKED(704, "帐号已锁定!"),
  AUTH_ACCOUNT_EXPIRED(705, "帐号已过期!"),
  AUTH_ACCOUNT_DISABLED(706, "帐号不可用，请联系管理员!"),
  AUTH_ERROR(707, "登录未知错误，请联系管理员!"),

  AUTH_TOKEN_VERIFICATION_FAILED(801, "令牌(token)验证失败!"),
  AUTH_TOKEN_CREATE_FAILED(802, "令牌(token)创建失败!"),
  AUTH_TOKEN_EXPIRED_FAILED(803, "令牌(token)已过期!");

  private final int code;
  private final String msg;

  HttpStatus(int code, String msg) {
    this.code = code;
    this.msg = msg;
  }

  public int getCode() {
    return code;
  }

  public String getMsg() {
    return msg;
  }
}
