package top.fanguoye.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

/**
 * @Author：Vance
 * @Description：菜单类型
 * @Date：2021/12/19 16:23
 */
public enum MenuType implements BaseEnum {

  CATALOG(1, "目录"),
  MENU(2, "菜单"),
  BUTTON(3, "按钮");

  @EnumValue
  private final int code;
  private final String desc;

  MenuType(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  @Override
  public int getCode() {
    return this.code;
  }

  @JsonCreator
  public static MenuType getEnum(Integer code) {
    return ClassUtils.getEnum(code, MenuType.class);
  }
}
