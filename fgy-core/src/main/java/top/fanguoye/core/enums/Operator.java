package top.fanguoye.core.enums;

/**
 * @Author：Vance
 * @Description：运算符
 * @Date：2021/12/14 18:46
 */
public enum Operator {

  /** 等于 = */
  eq,
  /** 不等于 <> */
  ne,
  /** 大于 > */
  gt,
  /** 大于等于 >= */
  ge,
  /** 小于 < */
  lt,
  /** 小于等于 <= */
  le,
  /** between 值1 and 值2 */
  between,
  /** not between 值1 and 值2 */
  notBetween,
  /** 模糊匹配like '%值%' */
  like,
  /** 模糊匹配not like '%值%' */
  notLike,
  /** like '%值' */
  likeLeft,
  /** like '值%' */
  likeRight,
  /** 字段 IS NULL */
  isNull,
  /** 字段 IS NOT NULL */
  isNotNull
}
