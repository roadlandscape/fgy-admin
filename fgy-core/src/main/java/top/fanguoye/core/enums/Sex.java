package top.fanguoye.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

/**
 * @Author：Vance
 * @Description：性别枚举
 * @Date：2021/12/16 11:53
 */
public enum Sex implements BaseEnum {

  MALE(1, "男"),
  FEMALE(2, "女"),
  UNKNOWN(3, "未知");

  @EnumValue
  private final int code;
  private final String desc;

  Sex(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  @Override
  public int getCode() {
    return this.code;
  }

  @JsonCreator
  public static Sex getEnum(Integer code) {
    return ClassUtils.getEnum(code, Sex.class);
  }
}
