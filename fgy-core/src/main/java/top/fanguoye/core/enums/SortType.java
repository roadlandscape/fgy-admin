package top.fanguoye.core.enums;

/**
 * @Author：Vance
 * @Description：排序类型
 * @Date：2021/12/14 18:06
 */
public enum SortType {

  /** 升序 */
  ASC,
  /** 降序 */
  DESC
}
