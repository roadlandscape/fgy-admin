package top.fanguoye.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

/**
 * @Author：Vance
 * @Description：状态
 * @Date：2021/12/19 15:20
 */
public enum Status implements BaseEnum {

  NORMAL(1, "正常"),
  DISABLE(2, "停用");

  @EnumValue
  private final int code;
  private final String desc;

  Status(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  @Override
  public int getCode() {
    return this.code;
  }

  @JsonCreator
  public static Status getEnum(Integer code) {
    return ClassUtils.getEnum(code, Status.class);
  }
}
