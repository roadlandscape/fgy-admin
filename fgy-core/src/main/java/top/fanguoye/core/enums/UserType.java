package top.fanguoye.core.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

/**
 * @Author：Vance
 * @Description：用户类型
 * @Date：2021/12/19 15:22
 */
public enum UserType implements BaseEnum {

  ADMIN(1, "管理员"),
  COMMON(2, "普通用户");

  @EnumValue
  private final int code;
  private final String desc;

  UserType(int code, String desc) {
    this.code = code;
    this.desc = desc;
  }

  @Override
  public int getCode() {
    return this.code;
  }

  @JsonCreator
  public static UserType getEnum(Integer code) {
    return ClassUtils.getEnum(code, UserType.class);
  }
}
