package top.fanguoye.core.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author：Vance
 * @Description：业务异常
 * @Date：2021/12/18 17:41
 */
@Getter
@Setter
public final class ServiceException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private int code = 500;
  private String msg;

  public ServiceException() {
    super();
  }

  public ServiceException(String msg) {
    super(msg);
    this.msg = msg;
  }

  public ServiceException(int code, String msg) {
    super(msg);
    this.code = code;
    this.msg = msg;
  }

  public ServiceException(Throwable throwable) {
    super(throwable);
    this.msg = throwable.getMessage();
  }

  public ServiceException(String message, Throwable throwable) {
    super(message, throwable);
    this.msg = message;
  }
}
