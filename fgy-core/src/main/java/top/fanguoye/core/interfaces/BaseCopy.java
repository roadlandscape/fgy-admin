package top.fanguoye.core.interfaces;

import org.mapstruct.BeanMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

/**
 * @Author：Vance
 * @Description：实体类属性之间互相赋值统一接口
 * @Date：2021/12/29 23:01
 */
public interface BaseCopy<F, T> {

  /**
   * F 转 T
   * @param f
   * @return
   */
  T toT(F f);

  /**
   * T 转 F
   * @param t
   * @return
   */
  F toF(T t);

  /**
   * F集合 转 T集合
   * @param fList
   * @return
   */
  List<T> toT(List<F> fList);

  /**
   * T集合 转 F集合
   * @param tList
   * @return
   */
  List<F> toF(List<T> tList);

  /**
   * 更新 F，如果t中与f匹配的属性值为空，则f中匹配的属性也会置为空
   * @param f
   * @param t
   */
  void updateF(@MappingTarget F f, T t);

  /**
   * 更新 T，如果f中与t匹配的属性值为空，则t中匹配的属性也会置为空
   * @param t
   * @param f
   */
  void updateT(@MappingTarget T t, F f);

  /**
   * 更新 F，如果t中与f匹配的属性值为空，则忽略赋值
   * @param f
   * @param t
   */
  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  void updateFIgnoreNull(@MappingTarget F f, T t);

  /**
   * 更新 T，如果f中与t匹配的属性值为空，则忽略赋值
   * @param t
   * @param f
   */
  @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
  void updateTIgnoreNull(@MappingTarget T t, F f);
}
