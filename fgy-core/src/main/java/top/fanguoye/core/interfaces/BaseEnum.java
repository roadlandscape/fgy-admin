package top.fanguoye.core.interfaces;

/**
 * @Author：Vance
 * @Description：通用枚举接口，指定序列化枚举使用的值
 * @Date：2021/12/27 00:03
 */
public interface BaseEnum {

  /**
   * 获取枚举编码,用于序列化
   * @return
   */
  int getCode();
}
