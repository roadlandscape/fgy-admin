package top.fanguoye.core.interfaces;

import javax.websocket.Session;

/**
 * @Author：Vance
 * @Description：WebSocket 服务端接口
 * @Date：2022/2/18 17:50
 */
public interface IWebSocketServer {

  /**
   * 连接建立成功调用
   *
   * @param session
   * @param connect 连接的唯一标识
   */
  void onOpen(Session session, String connect);

  /**
   * 收到客户端消息后调用
   *
   * @param message
   * @param session
   */
  void onMessage(String message, Session session);

  /**
   * 连接关闭调用
   */
  void onClose();

  /**
   * 发生错误/异常时调用
   *
   * @param session
   * @param error
   */
  void onError(Session session, Throwable error);

}
