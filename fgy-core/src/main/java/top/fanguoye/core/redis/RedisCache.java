package top.fanguoye.core.redis;

import cn.hutool.core.collection.CollectionUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import top.fanguoye.core.constant.StringPool;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Author：Vance
 * @Description：spring redis 工具类
 * @Date：2022/1/2 16:50
 */
@Component
@RequiredArgsConstructor
public class RedisCache {

  private final RedisTemplate redisTemplate;

  /**
   * 缓存基本的对象，Integer、String、实体类等
   * @param key 缓存的键值
   * @param value 缓存的值
   */
  public <T> void setCacheObject(final String key, final T value) {
    redisTemplate.opsForValue().set(key, value);
  }

  /**
   * 缓存基本的对象，Integer、String、实体类等
   * @param key 缓存的键
   * @param value 缓存的值
   * @param timeout 过期时间
   * @param timeUnit 时间颗粒度
   */
  public <T> void setCacheObject(final String key, final T value, final Integer timeout, final TimeUnit timeUnit) {
    redisTemplate.opsForValue().set(key, value, timeout, timeUnit);
  }

  /**
   * 设置有效时间
   * @param key Redis键
   * @param timeout 超时时间
   * @return true=设置成功；false=设置失败
   */
  public boolean expire(final String key, final long timeout) {
    return expire(key, timeout, TimeUnit.SECONDS);
  }

  /**
   * 设置有效时间
   * @param key Redis键
   * @param timeout 超时时间
   * @param unit 时间单位
   * @return true=设置成功；false=设置失败
   */
  public boolean expire(final String key, final long timeout, final TimeUnit unit) {
    return redisTemplate.expire(key, timeout, unit);
  }

  /**
   * 获得缓存的基本对象
   * @param key 缓存键值
   * @return 缓存键值对应的数据
   */
  public <T> T getCacheObject(final String key) {
    ValueOperations<String, T> operation = redisTemplate.opsForValue();
    return operation.get(key);
  }

  /**
   * 获取key过期日期，单位为秒
   * @param key 缓存键值
   * @return 当返回值等于null时 key在事务或管道中;返回值为-1时 此键值没有设置过期日期;返回值为-2时 不存在此键
   */
  public Long getExpire(final String key) {
    return redisTemplate.opsForValue().getOperations().getExpire(key);
  }

  /**
   * 获取key过期日期
   * @param key 缓存键值
   * @param unit 时间单位
   * @return 当返回值等于null时 key在事务或管道中;返回值为-1时 此键值没有设置过期日期;返回值为-2时 不存在此键
   */
  public Long getExpire(final String key, final TimeUnit unit) {
    return redisTemplate.opsForValue().getOperations().getExpire(key, unit);
  }

  /**
   * 删除单个对象
   * @param key 缓存的键
   * @return 是否删除成功
   */
  public boolean delObject(final String key) {
    return redisTemplate.delete(key);
  }

  /**
   * 删除多个对象
   * @param keys 缓存的键
   * @return 是否删除成功
   */
  public boolean delObject(final List<String> keys) {
    return redisTemplate.delete(keys) > 0;
  }

  /**
   * 模糊匹配 key 进行删除
   * @param likeKey 缓存的键(likeKey*)
   * @return 是否删除成功
   */
  public boolean delObjects(final String likeKey) {
    Set<String> keys = this.keys(likeKey + StringPool.ASTERISK);
    if (CollectionUtil.isEmpty(keys)) {
      return false;
    }

    for (String key : keys) {
      this.delObject(key);
    }

    return true;
  }


  /**
   * 删除集合对象
   * @param collection 多个对象
   * @return
   */
  public long delObject(final Collection collection) {
    return redisTemplate.delete(collection);
  }

  /**
   * 缓存List数据
   * @param key 缓存的键值
   * @param dataList 待缓存的List数据
   * @return 缓存的对象
   */
  public <T> long setCacheList(final String key, final List<T> dataList) {
    Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
    return count == null ? 0 : count;
  }

  /**
   * 缓存List数据
   * @param key 缓存的键
   * @param dataList 缓存的值
   * @param timeout 过期时间
   * @param timeUnit 时间颗粒度
   * @return 缓存的对象
   */
  public <T> long setCacheList(final String key, final List<T> dataList, final Integer timeout, final TimeUnit timeUnit) {
    Long count = redisTemplate.opsForList().rightPushAll(key, dataList);
    if (count != null && count > 0) {
      this.expire(key, timeout, timeUnit);
    }
    return count == null ? 0 : count;
  }

  /**
   * 获得缓存的list对象
   * @param key 缓存的键值
   * @return 缓存键值对应的数据
   */
  public <T> List<T> getCacheList(final String key) {
    return redisTemplate.opsForList().range(key, 0, -1);
  }

  /**
   * 缓存Set
   * @param key 缓存键值
   * @param dataSet 缓存的数据
   * @return 缓存数据的对象
   */
  public <T> BoundSetOperations<String, T> setCacheSet(final String key, final Set<T> dataSet) {
    BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
    for (T t : dataSet) {
      setOperation.add(t);
    }
    return setOperation;
  }

  /**
   * 缓存Set
   * @param key 缓存键值
   * @param dataSet 缓存的数据
   * @param timeout 过期时间
   * @param timeUnit 时间颗粒度
   * @return 缓存数据的对象
   */
  public <T> BoundSetOperations<String, T> setCacheSet(final String key, final List<T> dataSet, final Integer timeout, final TimeUnit timeUnit) {
    BoundSetOperations<String, T> setOperation = redisTemplate.boundSetOps(key);
    for (T t : dataSet) {
      setOperation.add(t);
    }
    this.expire(key, timeout, timeUnit);
    return setOperation;
  }

  /**
   * 获得缓存的set
   * @param key
   * @return
   */
  public <T> Set<T> getCacheSet(final String key) {
    return redisTemplate.opsForSet().members(key);
  }

  /**
   * 判断是否为set集合的元素
   * @param key
   * @param value
   * @return
   */
  public boolean isMemberSet(final String key, Object value) {
    return redisTemplate.opsForSet().isMember(key, value);
  }

  /**
   * ZSet增加一个值（自动按分数升序排序）
   *
   * @param key
   * @param value
   * @param score
   */
  public boolean addCacheZSet(final String key, final Object value, final double score) {
    return redisTemplate.opsForZSet().add(key, value, score);
  }

  /**
   * 删除ZSet中的数据
   *
   * @param key
   * @param values
   * @return
   */
  public boolean delCacheZSetValue(final String key, final Object... values) {
    return redisTemplate.opsForZSet().remove(key, values) > 0;
  }

  /**
   * 获取ZSet指定下标之间的值
   *
   * @param key
   * @param start
   * @param end
   * @param reverse 是否逆序
   * @return
   */
  public <T> Set<T> getCacheZSetValue(final String key, final long start, final  long end, final boolean reverse) {
    if (reverse) {
      return redisTemplate.opsForZSet().reverseRange(key, start, end);
    } else {
      return redisTemplate.opsForZSet().range(key, start, end);
    }
  }

  /**
   * 缓存Map
   * @param key
   * @param dataMap
   */
  public <T> void setCacheMap(final String key, final Map<String, T> dataMap) {
    if (dataMap != null) {
      redisTemplate.opsForHash().putAll(key, dataMap);
    }
  }

  /**
   * 获得缓存的Map
   * @param key
   * @return
   */
  public <T> Map<String, T> getCacheMap(final String key) {
    return redisTemplate.opsForHash().entries(key);
  }

  /**
   * 往Hash中存入数据
   * @param key Redis键
   * @param hKey Hash键
   * @param value 值
   */
  public <T> void setCacheMapValue(final String key, final String hKey, final T value) {
    redisTemplate.opsForHash().put(key, hKey, value);
  }

  /**
   * 获取Hash中的数据
   * @param key Redis键
   * @param hKey Hash键
   * @return Hash中的对象
   */
  public <T> T getCacheMapValue(final String key, final String hKey) {
    HashOperations<String, String, T> opsForHash = redisTemplate.opsForHash();
    return opsForHash.get(key, hKey);
  }

  /**
   * 删除Hash中的数据
   * @param key
   * @param mapKey
   */
  public void delCacheMapValue(final String key, final String mapKey) {
    HashOperations hashOperations = redisTemplate.opsForHash();
    hashOperations.delete(key, mapKey);
  }

  /**
   * 获取多个Hash中的数据
   * @param key Redis键
   * @param hKeys Hash键集合
   * @return Hash对象集合
   */
  public <T> List<T> getMultiCacheMapValue(final String key, final Collection<Object> hKeys) {
    return redisTemplate.opsForHash().multiGet(key, hKeys);
  }

  /**
   * 获得缓存的基本对象列表
   * @param pattern 匹配字符串
   * @return 对象列表
   */
  public Set<String> keys(final String pattern) {
    return redisTemplate.keys(pattern);
  }
}
