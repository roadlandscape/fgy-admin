package top.fanguoye.core.request;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.GetRequest;
import com.dtflys.forest.annotation.Var;
import com.dtflys.forest.callback.OnError;
import top.fanguoye.core.entity.HolidayDto;

/**
 * @Author：Vance
 * @Description：法定节假日 http 请求
 * @Date：2022/6/30 10:20
 */
@BaseRequest(baseURL = "#{holiday-url}")
public interface HolidayRequest {

  /**
   * 获取指定年份法定节假日数据
   *
   * @param year
   * @param onError
   * @return
   */
  @GetRequest(url = "/{year}.json")
  HolidayDto get(@Var("year") int year, OnError onError);
}
