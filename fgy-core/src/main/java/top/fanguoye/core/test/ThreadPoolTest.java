package top.fanguoye.core.test;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;

/**
 * @Author：Vance
 * @Description：多线程使用示例
 * @Date：2022/2/22 11:19
 */
// @Component
@RequiredArgsConstructor
public class ThreadPoolTest {

  private final ThreadPoolTaskExecutor taskExecutor1;
  private final ThreadPoolTaskExecutor taskExecutor2;

  /*
   * executor.execute(Runnable task) 适用于没有返回值的情况。未捕获的异常会抛出
   * executor.submit(Runnable task) 适用于没返回值的情况，未捕获的异常不会抛出
   * executor.submit(Callable task) 有返回值
   */

  /**
   * 没有返回值，不需要管任务执行结果如何
   */
  @PostConstruct
  public void test1() {
    for (int i = 0; i < 50; i++) {
      int finalI = i;

      taskExecutor1.execute(() -> {
        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        System.out.println(finalI);
      });
    }

    for (int i = 0; i < 50; i++) {
      System.out.println("main" + i);
    }
  }

  /**
   * 没有返回值，等所有任务执行完毕再往下执行
   */
  @PostConstruct
  public void test2() throws InterruptedException {
    // 定义多线程数量，一般就是任务数量
    final CountDownLatch latch = new CountDownLatch(50);
    for (int i = 0; i < 50; i++) {
      int finalI = i;

      taskExecutor2.execute(() -> {
        try {
          Thread.sleep(100);
          System.out.println(finalI);
        } catch (InterruptedException e) {
          e.printStackTrace();
        } finally {
          // 每个任务执行完成后对数量进行减一操作
          latch.countDown();
        }
      });
    }

    // 等待,不断检测数量是否为0，为零是执行后面的操作
    latch.await();
    System.out.println("任务执行完毕！");
  }

  /**
   * 有返回值
   * @throws Exception
   */
  @PostConstruct
  public void test3() throws Exception {
    List<Future<Integer>> iFutureList = new ArrayList<>();

    for (int i = 0; i < 50; i++) {
      int finalI = i;

      Future<Integer> iFuture = taskExecutor2.submit(() -> {
        Thread.sleep(100);
        return finalI;
      });

      iFutureList.add(iFuture);
    }

    for (Future<Integer> iFuture : iFutureList) {
      // get方法是阻塞的，会等待线程执行完成后再取值
      Integer i = iFuture.get();
      System.out.println(i);
    }
  }
}
