package top.fanguoye.core.utils;

import cn.hutool.json.JSONUtil;
import top.fanguoye.core.enums.AlgorithmType;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：AES加密工具类
 * @Date：2022/1/19 18:02
 */
public class AesEncryptUtils {

  /**
   * 参数分别代表 算法名称/加密模式/数据填充方式
   */
  private static final String ALGORITHM = "AES/ECB/PKCS5Padding";

  /**
   * 加密
   * @param content 加密的字符串
   * @param encryptKey key值
   * @return
   * @throws Exception
   */
  public static String encrypt(String content, String encryptKey) throws Exception {
    Cipher cipher = getCipher(encryptKey, Cipher.ENCRYPT_MODE);
    byte[] b = cipher.doFinal(content.getBytes(StandardCharsets.UTF_8));
    // 采用base64算法进行转码,避免出现中文乱码
    return Base64.getEncoder().encodeToString(b);
  }

  /**
   * 解密
   * @param encrypt 已加密数据
   * @param decryptKey 解密的key值
   * @return
   * @throws Exception
   */
  public static String decrypt(byte[] encrypt, String decryptKey) throws Exception {
    Cipher cipher = getCipher(decryptKey, Cipher.DECRYPT_MODE);
    // 采用base64算法进行转码,避免出现中文乱码
    byte[] encryptBytes = Base64.getDecoder().decode(encrypt);
    byte[] decryptBytes = cipher.doFinal(encryptBytes);
    return new String(decryptBytes, StandardCharsets.UTF_8);
  }

  private static Cipher getCipher(String encryptKey, int mode) throws Exception {
    KeyGenerator generator = KeyGenerator.getInstance(AlgorithmType.AES.toString());
    generator.init(128);
    Cipher cipher = Cipher.getInstance(ALGORITHM);
    cipher.init(mode, new SecretKeySpec(encryptKey.getBytes(), AlgorithmType.AES.toString()));
    return cipher;
  }

  public static void main(String[] args) throws Exception {
    Map<String,String> map = new HashMap<>();
    map.put("key","value");
    map.put("中文","汉字");
    String content = JSONUtil.toJsonStr(map);
    System.out.println("加密前：" + content);

    String encrypt = encrypt(content, "OiJsInR5cpXVCJ94");
    System.out.println("加密后：" + encrypt);

    String decrypt = decrypt(encrypt.getBytes(StandardCharsets.UTF_8), "OiJsInR5cpXVCJ94");
    System.out.println("解密后：" + decrypt);
  }
}
