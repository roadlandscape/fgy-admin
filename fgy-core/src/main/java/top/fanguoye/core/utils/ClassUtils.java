package top.fanguoye.core.utils;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.ClassMetadata;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import top.fanguoye.core.MyApplicationRunner;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.core.interfaces.BaseEnum;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @Author：Vance
 * @Description：类工具
 * @Date：2021/12/28 09:19
 */
@Slf4j
public class ClassUtils {

  public static void main(String[] args) throws IOException {
    Set<Class<BaseEnum>> extendsBaseEnumClass = ClassUtils.getExtendsBaseEnumClass("top.fanguoye.core.enums");
    System.out.println(extendsBaseEnumClass);
  }

  /**
   * 根据枚举成员变量获取对应的枚举值
   * @param code
   * @param enumClass
   * @param <T>
   * @return
   */
  public static <T> T getEnum(Integer code, Class<T> enumClass) {
    if (code == null) {
      return null;
    }
    T t = (T) MyApplicationRunner.ENUM_MAP.get(enumClass).get(code);
    if (t == null) {
      throw new IllegalArgumentException("参数有误！");
    }
    return t;
  }

  /**
   * 扫描继承了 BaseEnum接口 的枚举（搬运自mybatis-plus）
   * @param typeEnumsPackage 自定义枚举包
   */
  public static Set<Class<BaseEnum>> getExtendsBaseEnumClass(String typeEnumsPackage) throws IOException {
    if (StrUtil.isEmpty(typeEnumsPackage)) {
      return null;
    }
    Set<Class<BaseEnum>> classes;
    if (typeEnumsPackage.contains(StringPool.STAR) && !typeEnumsPackage.contains(StringPool.COMMA)
        && !typeEnumsPackage.contains(StringPool.SEMICOLON)) {
      classes = scanClasses(typeEnumsPackage, BaseEnum.class);
      if (classes.isEmpty()) {
        log.warn("Can't find class in '[" + typeEnumsPackage + "]' package. Please check your configuration.");
      }
    } else {
      classes = new HashSet<>();
      String[] typeEnumsPackageArray = StringUtils.tokenizeToStringArray(typeEnumsPackage,
          ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);
      Assert.notNull(typeEnumsPackageArray, "not find typeEnumsPackage:" + typeEnumsPackage);
      Stream.of(typeEnumsPackageArray).forEach(typePackage -> {
        try {
          Set<Class<BaseEnum>> scanTypePackage = scanClasses(typePackage, BaseEnum.class);
          if (scanTypePackage.isEmpty()) {
            log.warn("Can't find class in '[" + typePackage + "]' package. Please check your configuration.");
          } else {
            classes.addAll(scanTypePackage);
          }
        } catch (IOException e) {
          throw new ServiceException("Cannot scan class in '[" + typePackage + "]' package", e);
        }
      });
    }
    return classes;
  }

  private static <T> Set<Class<T>> scanClasses(String packagePatterns, Class<T> assignableType) throws IOException {
    Set<Class<T>> classes = new HashSet<>();
    String[] packagePatternArray = StringUtils.tokenizeToStringArray(packagePatterns,
        ConfigurableApplicationContext.CONFIG_LOCATION_DELIMITERS);
    for (String packagePattern : packagePatternArray) {
      Resource[] resources = new PathMatchingResourcePatternResolver()
          .getResources(ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
          + org.springframework.util.ClassUtils.convertClassNameToResourcePath(packagePattern) + "/**/*.class");
      for (Resource resource : resources) {
        try {
          ClassMetadata classMetadata = new CachingMetadataReaderFactory()
              .getMetadataReader(resource).getClassMetadata();
          Class<T> clazz = (Class<T>) Resources.classForName(classMetadata.getClassName());
          if (assignableType == null || assignableType.isAssignableFrom(clazz)) {
            classes.add(clazz);
          }
        } catch (Throwable e) {
          log.warn("Cannot load the '" + resource + "'. Cause by " + e.toString());
        }
      }
    }
    return classes;
  }
}
