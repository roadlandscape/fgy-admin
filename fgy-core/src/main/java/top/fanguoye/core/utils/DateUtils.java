package top.fanguoye.core.utils;

import cn.hutool.core.date.LocalDateTimeUtil;
import top.fanguoye.core.constant.CalendarConstants;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;

/**
 * @Author：Vance
 * @Description：时间工具类
 * @Date：2022/2/28 13:54
 */
public class DateUtils {

  public static void main(String[] args) {
    String cron = getCron(LocalDateTime.now());
    System.out.println(cron);
  }

  /**
   * LocalDateTime 转 cron表达式
   *
   * @param time
   * @return
   */
  public static String getCron(LocalDateTime time) {
    if (time == null) {
      return null;
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CalendarConstants.CRON_EXPRESSION1);
    return formatter.format(time);
  }

  /**
   * LocalTime 转 cron表达式
   *
   * @param time
   * @return
   */
  public static String getCron(LocalTime time) {
    if (time == null) {
      return null;
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(CalendarConstants.CRON_EXPRESSION2);
    return formatter.format(time);
  }

  /**
   * 判断日期是否是今天
   *
   * @param time
   * @return
   */
  public static boolean isToday(LocalDateTime time) {
    if (time == null) {
      return false;
    }
    String todayString = LocalDateTimeUtil.format(LocalDate.now(), CalendarConstants.DATE_FORMAT);
    String timeString = LocalDateTimeUtil.format(time, CalendarConstants.DATE_FORMAT);
    return todayString.equals(timeString);
  }

  /**
   * 获取本周周一时间
   *
   * @param localDateTime
   * @return
   */
  public static LocalDateTime getMondayOfWeek(LocalDateTime localDateTime) {
    LocalDateTime monday = localDateTime.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    return LocalDateTime.of(monday.getYear(), monday.getMonth(), monday.getDayOfMonth(), 0, 0);
  }

  /**
   * 获取一周时间数据
   *
   * @return
   */
  public static LocalDate[] getOneWeekDate(LocalDate date) {
    LocalDate[] oneWeekDate = new LocalDate[7];
    LocalDate monday = date.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    oneWeekDate[0] = monday;
    for (int i = 1; i < 7; i++) {
      oneWeekDate[i] = monday.plusDays(i);
    }
    return oneWeekDate;
  }

  /**
   * 获取星期字符串
   *
   * @param date
   * @return
   */
  public static String getWeekStr(LocalDate date) {
    String[] weekArr = new String[]{ "周一", "周二", "周三", "周四", "周五", "周六", "周日" };
    return weekArr[date.getDayOfWeek().getValue() - 1];
  }

  /**
   * 获取一个月时间数据
   *
   * @return
   */
  public static LocalDate[] getOneMonthDate(YearMonth date) {
    LocalDate firstDay = LocalDate.of(date.getYear(), date.getMonth(), 1);
    int lastDay = firstDay.with(TemporalAdjusters.lastDayOfMonth()).getDayOfMonth();
    LocalDate[] oneWeekDate = new LocalDate[lastDay];
    oneWeekDate[0] = firstDay;
    for (int i = 1; i < lastDay; i++) {
      oneWeekDate[i] = firstDay.plusDays(i);
    }
    return oneWeekDate;
  }
}
