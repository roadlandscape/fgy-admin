package top.fanguoye.core.utils;

import cn.hutool.core.io.FileTypeUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.ArrayUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;
import top.fanguoye.core.constant.CalendarConstants;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.exception.ServiceException;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

/**
 * @Author：Vance
 * @Description：文件工具类
 * @Date：2021/12/22 23:15
 */
@Slf4j
public class FileUtils {

  /**
   * 上传文件
   * @param file
   * @param folder
   * @param filename
   * @return
   */
  public static String upload(MultipartFile file, String folder, String filename) {
    if (Validator.hasChinese(filename)) {
      throw new ServiceException("文件名不能含有中文！");
    }

    String subDir = LocalDate.now().format(DateTimeFormatter.ofPattern(CalendarConstants.DATE_FORMAT2));
    if (!folder.contains(File.separator)) {
      if (folder.contains(StringPool.SLASH)) {
        folder = folder.replace(StringPool.SLASH, File.separator);
      } else if (folder.contains(StringPool.BACK_SLASH)) {
        folder = folder.replace(StringPool.BACK_SLASH, File.separator);
      }
    }
    String path = folder + File.separator + subDir + File.separator + filename;
    File dest;
    try {
      // getCanonicalFile 可解析正确各种路径
      dest = new File(path).getCanonicalFile();
    } catch (IOException e) {
      log.error("文件路径解析出错:", e);
      throw new ServiceException("文件路径解析出错！");
    }
    // 检测是否存在目录
    if (!dest.getParentFile().exists()) {
      if (!dest.getParentFile().mkdirs()) {
        throw new ServiceException("创建文件夹失败！");
      }
    }

    try {
      file.transferTo(dest);
      return StrUtils.joinSlash(subDir, filename);
    } catch (IOException e) {
      log.error("文件上传失败:", e);
      throw new ServiceException("文件上传失败！");
    }
  }

  /**
   * 上传文件
   * @param file
   * @param folder
   * @return
   */
  public static String upload(MultipartFile file, String folder) {
    String filename = file.getOriginalFilename();
    String extensionName = FileUtil.extName(filename);
    String uuid = UUID.randomUUID().toString().replaceAll(StringPool.DASH, StringPool.EMPTY);
    filename = StrUtils.joinPoint(uuid, extensionName);
    return FileUtils.upload(file, folder, filename);
  }

  /**
   * 是否是图片类型，并且是系统支持的类型
   * @param file
   * @return
   */
  public static boolean isImage(MultipartFile file) {
    try {
      String type = FileTypeUtil.getType(file.getInputStream());
      return type != null && ArrayUtil.containsIgnoreCase(Constants.IMAGE_TYPE, type);
    } catch (Exception e) {
      log.error("文件解析出错:", e);
      throw new ServiceException("文件解析出错！");
    }
  }

  /**
   * byte转化为KB、MB、GB
   * @param file
   * @return
   */
  public static String getFileSizeDesc(MultipartFile file) {
    long size = file.getSize();
    return UnitUtils.byteToKbOrMbOrGbOrTb(size);
  }
}
