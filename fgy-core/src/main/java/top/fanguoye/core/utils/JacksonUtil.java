package top.fanguoye.core.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import top.fanguoye.core.entity.BaseEntity;
import top.fanguoye.core.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：Vance
 * @Description：Jackson 工具类
 * @Date：2022/3/6 22:40
 */
@Slf4j
public class JacksonUtil {

  private static final ObjectMapper OBJECT_MAPPER = SpringUtil.getBean(ObjectMapper.class);
  // private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  public static void main(String[] args) {
    BaseEntity baseEntity1 = new BaseEntity();
    baseEntity1.setId(1L);
    baseEntity1.setCreator("fanguoye@163.com");
    String baseEntityStr = JacksonUtil.obj2String(baseEntity1);

    BaseEntity baseEntity2 = JacksonUtil.string2Obj(baseEntityStr, BaseEntity.class);
    baseEntity2.setId(2L);
    baseEntity2.setCreator("fanguoye@126.com");

    List<BaseEntity> baseEntityList = new ArrayList<>();
    baseEntityList.add(baseEntity1);
    baseEntityList.add(baseEntity2);
    String baseEntityListJson = JacksonUtil.obj2String(baseEntityList);

    List<BaseEntity> baseEntityListBean1 = JacksonUtil.string2Obj(baseEntityListJson, new TypeReference<List<BaseEntity>>() {});
    if (baseEntityListBean1 != null) {
      baseEntityListBean1.forEach(baseEntity -> {
        System.out.println(baseEntity.getId() + " : " + baseEntity.getCreator());
      });
    }

    List<BaseEntity> baseEntityListBean2 = JacksonUtil.string2Obj(baseEntityListJson, List.class, BaseEntity.class);
    if (baseEntityListBean2 != null) {
      baseEntityListBean2.forEach(baseEntity -> {
        System.out.println(baseEntity.getId() + " : " + baseEntity.getCreator());
      });
    }
  }

  /**
   * 对象转Json格式字符串
   * @param obj 对象
   * @return Json格式字符串
   */
  public static <T> String obj2String(T obj) {
    if (obj == null) {
      return null;
    }
    try {
      return obj instanceof String ? (String) obj : OBJECT_MAPPER.writeValueAsString(obj);
    } catch (JsonProcessingException e) {
      throw new ServiceException(e);
    }
  }

  /**
   * 字符串转换为自定义对象
   * @param str 要转换的字符串
   * @param clazz 自定义对象的class对象
   * @return 自定义对象
   */
  public static <T> T string2Obj(String str, Class<T> clazz) {
    if (StrUtil.isEmpty(str) || clazz == null) {
      return null;
    }
    try {
      return clazz.equals(String.class) ? (T) str : OBJECT_MAPPER.readValue(str, clazz);
    } catch (JsonProcessingException e) {
      throw new ServiceException(e);
    }
  }

  /**
   * 字符串转换为带泛型的对象
   * @param str 要转换的字符串
   * @param typeReference 转化类型 new TypeReference<List<BaseEntity>>() {}
   * @return 对象
   */
  public static <T> T string2Obj(String str, TypeReference<T> typeReference) {
    if (StrUtil.isEmpty(str) || typeReference == null) {
      return null;
    }
    try {
      return (T) (typeReference.getType().equals(String.class) ? str : OBJECT_MAPPER.readValue(str, typeReference));
    } catch (JsonProcessingException e) {
      throw new ServiceException(e);
    }
  }

  /**
   * 字符串转换为带泛型的对象
   * @param str 要转换的字符串
   * @param collectionClass 外层对象
   * @param elementClasses 泛型
   * @return 对象
   */
  public static <T> T string2Obj(String str, Class<?> collectionClass, Class<?>... elementClasses) {
    JavaType javaType = OBJECT_MAPPER.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    try {
      return OBJECT_MAPPER.readValue(str, javaType);
    } catch (JsonProcessingException e) {
      throw new ServiceException(e);
    }
  }
}
