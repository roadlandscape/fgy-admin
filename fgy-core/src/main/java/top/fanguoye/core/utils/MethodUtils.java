package top.fanguoye.core.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import top.fanguoye.core.constant.StringPool;

import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author：Vance
 * @Description：方法工具类
 * @Date：2022/2/21 16:34
 */
public class MethodUtils {

  public static void main(String[] args) throws Exception {
    MethodUtils.invokeMethod("top.fanguoye.core.utils.ClassUtils" +
        ".getExtendsBaseEnumClass('top.fanguoye.core.enums')");
  }

  /**
   * 执行方法
   * @param beanMethod
   * @throws Exception
   */
  public static void invokeMethod(String beanMethod) throws Exception {
    String classMethod = beanMethod;
    classMethod = StrUtil.subBefore(classMethod, StringPool.LEFT_BRACKET, false);
    String beanName = StrUtil.subBefore(classMethod, StringPool.DOT, true);
    String methodName = StrUtil.subAfter(classMethod, StringPool.DOT, true);
    Object bean;
    if (beanName.contains(StringPool.DOT)) {
      bean = Class.forName(beanName).newInstance();
    } else {
      bean = SpringUtil.getBean(beanName);
    }

    List<Object[]> methodParams = getMethodParams(beanMethod);
    if (CollectionUtil.isNotEmpty(methodParams)) {
      Class<?>[] classes = new Class<?>[methodParams.size()];
      Object[] params = new Object[methodParams.size()];
      for (int i = 0; i < methodParams.size(); i++) {
        params[i] = methodParams.get(i)[0];
        classes[i] = (Class<?>) methodParams.get(i)[1];
      }
      Method method = bean.getClass().getDeclaredMethod(methodName, classes);
      method.invoke(bean, params);
    } else {
      Method method = bean.getClass().getDeclaredMethod(methodName);
      method.invoke(bean);
    }
  }

  private static List<Object[]> getMethodParams(String invokeMethod) {
    String paramsStr = StrUtil.subBetween(invokeMethod, StringPool.LEFT_BRACKET, StringPool.RIGHT_BRACKET);
    if (StrUtil.isBlank(paramsStr)) {
      return null;
    }
    String[] methodParams = paramsStr.split(",(?=([^\"']*[\"'][^\"']*[\"'])*[^\"']*$)");
    List<Object[]> classes = new LinkedList<>();
    for (String methodParam : methodParams) {
      String param = StrUtil.trimToEmpty(methodParam);
      if (StrUtil.startWithAny(param, StringPool.SINGLE_QUOTE, StringPool.QUOTE)) {
        // String字符串类型，以'或"开头
        classes.add(new Object[]{StrUtil.sub(param, 1, param.length() - 1), String.class});
      } else if (StringPool.TRUE.equalsIgnoreCase(param) || StringPool.FALSE.equalsIgnoreCase(param)) {
        // boolean布尔类型，等于true或者false
        classes.add(new Object[]{Boolean.valueOf(param), Boolean.class});
      } else if (StrUtil.endWithAny(param, StringPool.L, StringPool.l)) {
        // long长整形，以L、l结尾
        classes.add(new Object[]{Long.valueOf(StrUtil.sub(param, 0, param.length() - 1)), Long.class});
      } else if (StrUtil.endWithAny(param, StringPool.D, StringPool.d)) {
        // double浮点类型，以D、d结尾
        classes.add(new Object[]{Double.valueOf(StrUtil.sub(param, 0, param.length() - 1)), Double.class});
      } else {
        // 其他类型归类为整形
        classes.add(new Object[]{Integer.valueOf(param), Integer.class});
      }
    }
    return classes;
  }
}
