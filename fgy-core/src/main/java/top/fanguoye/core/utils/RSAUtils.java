package top.fanguoye.core.utils;

import cn.hutool.core.map.MapUtil;
import top.fanguoye.core.enums.AlgorithmType;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：RSA公钥/私钥/签名工具包
 * 字符串格式的密钥在未在特殊说明情况下都为BASE64编码格式
 * 由于非对称加密速度极其缓慢，一般数据不使用它来加密而是使用对称加密,
 * 非对称加密算法可以用来对对称加密的密钥加密，这样保证密钥的安全也就保证了数据的安全
 * @Date：2022/1/20 09:01
 */
public class RSAUtils {

  /** 获取公钥的key */
  private static final String PUBLIC_KEY = "RSAPublicKey";
  /** 获取私钥的key */
  private static final String PRIVATE_KEY = "RSAPrivateKey";

  /** 签名算法 */
  public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";

  /** RSA最大加密明文大小 */
  private static final int MAX_ENCRYPT_BLOCK = 117;
  /** RSA最大解密密文大小 */
  private static final int MAX_DECRYPT_BLOCK = 128;
  /** RSA 位数 如果采用2048 上面最大加密和最大解密则须填写: 245 256 */
  private static final int INITIALIZE_LENGTH = 1024;

  public static void main(String[] args) throws Exception {
    Map<String, Object> map = genKeyPair();
    String privateKey = getPrivateKey(map);
    String publicKey = getPublicKey(map);
    System.out.println("私钥：" + privateKey);
    System.out.println("公钥：" + publicKey);
    System.out.println("------------------");
    String sign = sign("测试", privateKey);
    System.out.println(sign);
    boolean verify = verify("测试", publicKey, sign);
    System.out.println(verify);
    System.out.println("------------------");

    String vance = encryptByPublicKey("测试", publicKey);
    System.out.println(vance);
    String decryptVance = decryptByPrivateKey(vance, privateKey);
    System.out.println(decryptVance);
    System.out.println("------------------");
    String abc = encryptByPrivateKey("测试", privateKey);
    System.out.println(abc);
    String decryptAbc = decryptByPublicKey(abc.getBytes(StandardCharsets.UTF_8), publicKey);
    System.out.println(decryptAbc);

  }

  /**
   * 生成密钥对(公钥和私钥)
   * @return
   * @throws Exception
   */
  public static Map<String, Object> genKeyPair() throws Exception {
    KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(AlgorithmType.RSA.toString());
    keyPairGen.initialize(INITIALIZE_LENGTH);
    KeyPair keyPair = keyPairGen.generateKeyPair();
    RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
    RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();
    Map<String, Object> keyMap = MapUtil.newHashMap(2);
    keyMap.put(PUBLIC_KEY, publicKey);
    keyMap.put(PRIVATE_KEY, privateKey);
    return keyMap;
  }

  /**
   * 获取私钥
   * @param keyMap 密钥对
   * @return
   * @throws Exception
   */
  public static String getPrivateKey(Map<String, Object> keyMap) throws Exception {
    Key key = (Key) keyMap.get(PRIVATE_KEY);
    return Base64.getEncoder().encodeToString(key.getEncoded());
  }

  /**
   * 获取公钥
   * @param keyMap 密钥对
   * @return
   * @throws Exception
   */
  public static String getPublicKey(Map<String, Object> keyMap) throws Exception {
    Key key = (Key) keyMap.get(PUBLIC_KEY);
    return Base64.getEncoder().encodeToString(key.getEncoded());
  }

  /**
   * 用私钥对信息生成数字签名
   * @param data 数据
   * @param privateKey 私钥(BASE64编码)
   * @return
   * @throws Exception
   */
  public static String sign(String data, String privateKey) throws Exception {
    byte[] keyBytes = Base64.getDecoder().decode(privateKey);
    PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
    PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
    Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
    signature.initSign(privateK);
    signature.update(data.getBytes(StandardCharsets.UTF_8));
    return Base64.getEncoder().encodeToString(signature.sign());
  }

  /**
   * 校验数字签名
   * @param data 已加密数据
   * @param publicKey 公钥(BASE64编码)
   * @param sign 数字签名
   * @return
   * @throws Exception
   *
   */
  public static boolean verify(String data, String publicKey, String sign) throws Exception {
    byte[] keyBytes = Base64.getDecoder().decode(publicKey);
    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
    PublicKey publicK = keyFactory.generatePublic(keySpec);
    Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
    signature.initVerify(publicK);
    signature.update(data.getBytes(StandardCharsets.UTF_8));
    return signature.verify(Base64.getDecoder().decode(sign));
  }

  /**
   * 公钥加密
   * @param data 源数据
   * @param publicKey 公钥(BASE64编码)
   * @return
   * @throws Exception
   */
  public static String encryptByPublicKey(String data, String publicKey) throws Exception {
    byte[] keyBytes = Base64.getDecoder().decode(publicKey);
    X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
    Key publicK = keyFactory.generatePublic(x509KeySpec);
    Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
    cipher.init(Cipher.ENCRYPT_MODE, publicK);
    return Base64.getEncoder()
        .encodeToString(getEncryptedData(data.getBytes(StandardCharsets.UTF_8), cipher));
  }

  /**
   * 私钥加密
   * @param data 源数据
   * @param privateKey 私钥(BASE64编码)
   * @return
   * @throws Exception
   */
  public static String encryptByPrivateKey(String data, String privateKey) throws Exception {
    byte[] keyBytes = Base64.getDecoder().decode(privateKey);
    PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
    Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
    Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
    cipher.init(Cipher.ENCRYPT_MODE, privateK);
    return Base64.getEncoder()
        .encodeToString(getEncryptedData(data.getBytes(StandardCharsets.UTF_8), cipher));
  }

  /**
   * 公钥解密
   * @param encryptedData 已加密数据
   * @param publicKey 公钥(BASE64编码)
   * @return
   * @throws Exception
   */
  public static String decryptByPublicKey(byte[] encryptedData, String publicKey) throws Exception {
    byte[] rs = Base64.getDecoder().decode(encryptedData);
    byte[] keyBytes = Base64.getDecoder().decode(publicKey);
    X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
    Key publicK = keyFactory.generatePublic(x509KeySpec);
    Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
    cipher.init(Cipher.DECRYPT_MODE, publicK);
    return getDecryptedData(rs, cipher);
  }

  /**
   * 私钥解密
   * @param encryptedData 已加密数据
   * @param privateKey 私钥(BASE64编码)
   * @return
   * @throws Exception
   */
  public static String decryptByPrivateKey(String encryptedData, String privateKey) throws Exception {
    byte[] rs = Base64.getDecoder().decode(encryptedData);
    byte[] keyBytes = Base64.getDecoder().decode(privateKey);
    PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
    Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
    Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
    cipher.init(Cipher.DECRYPT_MODE, privateK);
    return getDecryptedData(rs, cipher);
  }

  private static byte[] getEncryptedData(byte[] data, Cipher cipher) throws Exception {
    return getProcessData(data, cipher, MAX_ENCRYPT_BLOCK);
  }


  private static String getDecryptedData(byte[] encryptedData, Cipher cipher) throws Exception {
    return new String(getProcessData(encryptedData, cipher, MAX_DECRYPT_BLOCK), StandardCharsets.UTF_8);
  }

  private static byte[] getProcessData(byte[] data, Cipher cipher, int blockSize) throws Exception {
    int inputLen = data.length;
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    int offSet = 0;
    byte[] cache;
    int i = 0;
    // 对数据分段加密
    while (inputLen - offSet > 0) {
      if (inputLen - offSet > blockSize) {
        cache = cipher.doFinal(data, offSet, blockSize);
      } else {
        cache = cipher.doFinal(data, offSet, inputLen - offSet);
      }
      out.write(cache, 0, cache.length);
      i++;
      offSet = i * blockSize;
    }
    byte[] encryptedData = out.toByteArray();
    out.close();
    return encryptedData;
  }
}
