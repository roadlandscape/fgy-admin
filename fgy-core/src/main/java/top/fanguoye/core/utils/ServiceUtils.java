package top.fanguoye.core.utils;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import top.fanguoye.core.entity.BaseEntity;
import top.fanguoye.core.entity.FilterCondition;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.SortCondition;
import top.fanguoye.core.enums.Operator;
import top.fanguoye.core.enums.SortType;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author：Vance
 * @Description：业务service
 * @Date：2021/12/15 16:31
 */
public class ServiceUtils {

  /**
   * 构建对象封装操作类QueryWrapper
   * @param condition
   * @param tableAlias 表的别名
   * @return
   */
  public static QueryWrapper builderQueryWrapper(QueryCondition condition, String tableAlias) {
    //　字段条件过滤
    List<FilterCondition> filterConditions = condition.getFilterConditions();
    QueryWrapper queryWrapper = new QueryWrapper();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, tableAlias);
    }
    // 排序字段
    List<SortCondition> sortConditions = condition.getSortConditions();
    if (CollectionUtil.isNotEmpty(sortConditions)) {
      List<String> ascField = new ArrayList<>();
      List<String> descField = new ArrayList<>();
      for (SortCondition sortCondition : sortConditions) {
        if (StrUtil.isBlank(sortCondition.getField())) {
          continue;
        }
        String field = StrUtil.toUnderlineCase(sortCondition.getField());
        if (StrUtil.isNotEmpty(tableAlias)) {
          field = StrUtils.joinPoint(tableAlias, field);
        }
        if (SortType.DESC.equals(sortCondition.getType())) {
          descField.add(field);
        } else {
          ascField.add(field);
        }
      }
      queryWrapper.orderByAsc(ascField);
      queryWrapper.orderByDesc(descField);
    } else {
      String field = BaseEntity.CREATE_TIME;
      if (StrUtil.isNotEmpty(tableAlias)) {
        field = StrUtils.joinPoint(tableAlias, field);
      }
      queryWrapper.orderByDesc(field);
    }
    return queryWrapper;
  }

  /**
   * 构建对象封装操作类QueryWrapper
   * @param filterConditions
   * @param tableAlias 表的别名
   * @return
   */
  public static QueryWrapper builderQueryWrapper(List<FilterCondition> filterConditions, String tableAlias) {
    QueryWrapper wrapper = new QueryWrapper();
    for (FilterCondition filterCondition : filterConditions) {
      String field = filterCondition.getField();
      if (StrUtil.isBlank(field)) {
        continue;
      }

      Object value1 = filterCondition.getValue1();
      Object value2 = filterCondition.getValue2();
      if (Operator.between.equals(filterCondition.getType()) || Operator.notBetween.equals(filterCondition.getType())) {
        if (StrUtil.isBlankIfStr(value1) || StrUtil.isBlankIfStr(value2)) {
          continue;
        }
      } else if (Operator.isNull.equals(filterCondition.getType()) || Operator.isNotNull.equals(filterCondition.getType())) {
        // 啥也不干
      } else {
        if (StrUtil.isBlankIfStr(value1)) {
          continue;
        }
      }

      field = StrUtil.toUnderlineCase(field);
      if (StrUtil.isNotEmpty(tableAlias)) {
        field = StrUtils.joinPoint(tableAlias, field);
      }

      switch (filterCondition.getType()) {
        case ne:
          wrapper.ne(field, value1);
          break;
        case gt:
          wrapper.gt(field, value1);
          break;
        case ge:
          wrapper.ge(field, value1);
          break;
        case lt:
          wrapper.lt(field, value1);
          break;
        case le:
          wrapper.le(field, value1);
          break;
        case between:
          wrapper.between(field, value1, value2);
          break;
        case notBetween:
          wrapper.notBetween(field, value1, value2);
          break;
        case like:
          wrapper.like(field, value1);
          break;
        case notLike:
          wrapper.notLike(field, value1);
          break;
        case likeLeft:
          wrapper.likeLeft(field, value1);
          break;
        case likeRight:
          wrapper.likeRight(field, value1);
          break;
        case isNull:
          wrapper.isNull(field);
          break;
        case isNotNull:
          wrapper.isNotNull(field);
          break;
        case eq:
        default:
          wrapper.eq(field, value1);
          break;
      }
    }
    return wrapper;
  }

  /**
   * 构建排序元素载体
   * @param sortConditions
   * @param tableAlias 表的别名
   * @return
   */
  public static List<OrderItem> builderOrderItem(List<SortCondition> sortConditions, String tableAlias) {
    List<OrderItem> orders = new ArrayList<>();
    if (CollectionUtil.isEmpty(sortConditions)) {
      OrderItem orderItem = new OrderItem();
      String field = BaseEntity.CREATE_TIME;
      if (StrUtil.isNotEmpty(tableAlias)) {
        field = StrUtils.joinPoint(tableAlias, field);
      }
      orderItem.setColumn(field);
      orderItem.setAsc(false);
      orders.add(orderItem);
    } else {
      for (SortCondition sortCondition : sortConditions) {
        if (StrUtil.isBlank(sortCondition.getField())) {
          continue;
        }
        OrderItem orderItem = new OrderItem();
        String field = StrUtil.toUnderlineCase(sortCondition.getField());
        if (StrUtil.isNotEmpty(tableAlias)) {
          field = StrUtils.joinPoint(tableAlias, field);
        }
        orderItem.setColumn(field);
        orderItem.setAsc(!SortType.DESC.equals(sortCondition.getType()));
        orders.add(orderItem);
      }
    }
    return orders;
  }
}
