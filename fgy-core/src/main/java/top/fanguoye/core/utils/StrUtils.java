package top.fanguoye.core.utils;

import cn.hutool.core.util.StrUtil;
import top.fanguoye.core.constant.StringPool;

/**
 * @Author：Vance
 * @Description：字符串工具类
 * @Date：2021/12/25 13:49
 */
public class StrUtils {

  /**
   * 使用 . 拼接起来
   * @param values
   * @return
   */
  public static String joinPoint(Object... values) {
    return StrUtil.join(StringPool.DOT, values);
  }

  /**
   * 使用 / 拼接起来
   * @param values
   * @return
   */
  public static String joinSlash(Object... values) {
    return StrUtil.join(StringPool.SLASH, values);
  }
}
