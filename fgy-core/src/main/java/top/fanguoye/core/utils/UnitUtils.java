package top.fanguoye.core.utils;

import java.text.DecimalFormat;

/**
 * @Author：Vance
 * @Description：单位工具类
 * @Date：2022/4/8 11:32
 */
public class UnitUtils {

  /**
   * byte转化为KB、MB、GB、TB
   *
   * @param size
   * @return
   */
  public static String byteToKbOrMbOrGbOrTb(long size) {
    StringBuilder fileSizeDesc = new StringBuilder();
    DecimalFormat format = new DecimalFormat("###.0");
    if (size >= Math.pow(1024, 4)) {
      double i = (size / Math.pow(1024, 4));
      fileSizeDesc.append(format.format(i)).append("TB");
    } else if (size >= Math.pow(1024, 3)) {
      double i = (size / Math.pow(1024, 3));
      fileSizeDesc.append(format.format(i)).append("GB");
    } else if (size >= Math.pow(1024, 2)) {
      double i = (size / Math.pow(1024, 2));
      fileSizeDesc.append(format.format(i)).append("MB");
    } else if (size >= 1024) {
      double i = (size / 1024.0);
      fileSizeDesc.append(format.format(i)).append("KB");
    } else {
      if (size <= 0) {
        fileSizeDesc.append("0B");
      } else {
        fileSizeDesc.append((int) size).append("B");
      }
    }
    return fileSizeDesc.toString();
  }
}
