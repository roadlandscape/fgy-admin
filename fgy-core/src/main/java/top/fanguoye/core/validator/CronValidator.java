package top.fanguoye.core.validator;

import cn.hutool.core.util.StrUtil;
import org.springframework.scheduling.support.CronExpression;
import top.fanguoye.core.annotation.Cron;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author：Vance
 * @Description：Cron表达式校验器
 * @Date：2022/1/15 23:53
 */
public class CronValidator implements ConstraintValidator<Cron, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (StrUtil.isEmpty(value)) {
      return true;
    }

    return CronExpression.isValidExpression(value);
  }
}
