package top.fanguoye.core.validator;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import top.fanguoye.core.annotation.IP;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author：Vance
 * @Description：ip校验器
 * @Date：2022/3/14 10:56
 */
public class IPValidator implements ConstraintValidator<IP, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
    if (StrUtil.isEmpty(value)) {
      return true;
    }

    return Validator.isIpv4(value) || Validator.isIpv6(value);
  }
}
