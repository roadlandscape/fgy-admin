package top.fanguoye.core.validator;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import top.fanguoye.core.annotation.Mobile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author：Vance
 * @Description：手机号码校验器
 * @Date：2022/1/15 23:53
 */
public class MobileValidator implements ConstraintValidator<Mobile, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    if (StrUtil.isEmpty(value)) {
      return true;
    }

    return Validator.isMobile(value);
  }
}
