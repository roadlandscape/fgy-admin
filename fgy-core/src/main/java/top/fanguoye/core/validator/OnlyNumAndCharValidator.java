package top.fanguoye.core.validator;

import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;
import top.fanguoye.core.annotation.OnlyNumAndChar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @Author：Vance
 * @Description：是否仅包含字母和数字校验器
 * @Date：2022/3/14 10:47
 */
public class OnlyNumAndCharValidator implements ConstraintValidator<OnlyNumAndChar, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
    if (StrUtil.isEmpty(value)) {
      return true;
    }

    return Validator.isMatchRegex("^[a-z0-9A-Z]+$", value);
  }
}
