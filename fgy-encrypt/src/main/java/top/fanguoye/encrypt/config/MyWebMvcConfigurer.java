package top.fanguoye.encrypt.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author：Vance
 * @Description：WebMvc配置
 * @Date：2021/12/18 10:15
 */
@Configuration
public class MyWebMvcConfigurer implements WebMvcConfigurer {

  /**
   * 跨域支持
   * @param registry
   */
  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
        .allowedOriginPatterns("*")
        .allowedMethods("*")
        .allowCredentials(true)
        .maxAge(1800)
        .allowedHeaders("*");
  }
}
