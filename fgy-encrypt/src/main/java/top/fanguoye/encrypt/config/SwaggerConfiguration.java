package top.fanguoye.encrypt.config;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.time.LocalTime;
import java.time.MonthDay;
import java.time.Year;
import java.time.YearMonth;

/**
 * @Author：Vance
 * @Description：swagger相关配置
 * @Date：2021/12/13 21:18
 */
@Slf4j
@Configuration
public class SwaggerConfiguration {

  /** 是否开启swagger */
  @Value("${springfox.documentation.enabled:false}")
  private boolean enabled;

  @Bean
  public Docket createRestApi() {
    Docket docket = new Docket(DocumentationType.OAS_30)
        .enable(enabled)
        .apiInfo(apiInfo());
    // 配置文档识别数据类型
    directModelSubstitute(docket);
    return docket.select()
        // 扫描所有有注解的api，用这种方式更灵活
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
        .paths(PathSelectors.any())
        .build();
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title("encrypt加密模块_接口文档")
        .description("")
        .contact(new Contact("fanguoye", "http://www.fanguoye.top", "fanguoye@126.com"))
        .version("0.0.1-SNAPSHOT")
        .build();
  }

  /**
   * 配置文档识别数据类型
   */
  private void directModelSubstitute(Docket docket) {
    // 配置文档识别LocalTime为字符串（默认是HH:mm:ss）
    docket.directModelSubstitute(LocalTime.class, String.class)
        // 配置文档识别MonthDay为字符串（默认是MM-dd）
        .directModelSubstitute(MonthDay.class, String.class)
        // 配置文档识别YearMonth为字符串（默认是yyyy-MM）
        .directModelSubstitute(YearMonth.class, String.class)
        // 配置文档识别Year为字符串
        .directModelSubstitute(Year.class, String.class)
        // 配置文档识别Object为String
        .directModelSubstitute(Object.class, String.class);
  }
}
