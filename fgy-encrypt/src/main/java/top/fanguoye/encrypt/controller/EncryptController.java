package top.fanguoye.encrypt.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.crypto.asymmetric.RSA;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.fanguoye.encrypt.entity.EncryptCheckModel;
import top.fanguoye.encrypt.entity.EncryptionCreatorParam;
import top.fanguoye.encrypt.entity.R;
import top.fanguoye.encrypt.service.EncryptionCreatorService;
import top.fanguoye.encrypt.service.ServerInfoService;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：加密Controller
 * @Date：2022/2/10 11:55
 */
@Slf4j
@Api(tags = "encrypt加密")
@RestController
@RequestMapping("/encrypt")
@RequiredArgsConstructor
@Validated
public class EncryptController {

  private final ServerInfoService serverInfoService;
  private final EncryptionCreatorService encryptionCreatorService;

  @ApiOperation("获取服务器硬件信息")
  @GetMapping(value = "/getServerInfo")
  public R<EncryptCheckModel> getServerInfo() {
    try {
      EncryptCheckModel infos = serverInfoService.getServerInfos();
      return R.ok(infos);
    } catch (Exception e) {
      log.error("获取服务器硬件信息失败 - ",e);
      return R.error("获取服务器硬件信息失败:" + e.getMessage());
    }
  }

  @ApiOperation("生成公钥和私钥")
  @GetMapping(value = "/generateSecret")
  public R generateSecret() {
    Map<String, String> map = MapUtil.newHashMap(2);
    RSA rsa = new RSA();
    map.put("privateKey", rsa.getPrivateKeyBase64());
    map.put("publicKey", rsa.getPublicKeyBase64());
    return R.ok(map);
  }

  @ApiOperation("生成加密串")
  @PostMapping(value = "/generateEncryption")
  public R generateEncryption(@RequestBody @Validated EncryptionCreatorParam param) throws JsonProcessingException {
    return R.ok().setData(encryptionCreatorService.generateEncryption(param));
  }

  @ApiOperation("生成加密串(服务器硬件校验信息encryptCheckModel不需要传，自动获取当前服务器的硬件信息)")
  @PostMapping(value = "/generateEncryption2")
  public R generateEncryption2(@RequestBody @Validated EncryptionCreatorParam param) throws Exception {
    EncryptCheckModel infos = serverInfoService.getServerInfos();
    param.setEncryptCheckModel(infos);
    return R.ok().setData(encryptionCreatorService.generateEncryption(param));
  }

  @ApiOperation("解密加密串")
  @PostMapping(value = "/decryptEncryption")
  public R decryptEncryption(@RequestParam @NotBlank String publicKey, @RequestParam @NotBlank String encryption) throws JsonProcessingException {
    return R.ok().setData(encryptionCreatorService.decryptEncryption(publicKey, encryption));
  }
}
