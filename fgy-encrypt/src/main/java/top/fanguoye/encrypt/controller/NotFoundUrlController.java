package top.fanguoye.encrypt.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.encrypt.entity.R;

/**
 * @Author：Vance
 * @Description：处理404
 * @Date：2021/12/20 15:51
 */
@RestController
public class NotFoundUrlController {

  @RequestMapping
  public R error() {
    return R.resp(404, "请求url不存在!");
  }
}
