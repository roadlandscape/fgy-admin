package top.fanguoye.encrypt.converter;

import cn.hutool.core.util.StrUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import top.fanguoye.encrypt.constant.CalendarConstants;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @Author：Vance
 * @Description：springmvc接收前端数据 类型转换器
 * @Date：2021/12/14 10:40
 */
@Configuration
public class DataTypeConverters {

  @Bean
  public Converter<String, LocalDate> localDateConverter() {
    return new Converter<String, LocalDate>() {
      @Override
      public LocalDate convert(String source) {
        if (StrUtil.isBlank(source)) {
          return null;
        }

        DateTimeFormatter df = DateTimeFormatter.ofPattern(CalendarConstants.DATE_FORMAT);
        return LocalDate.parse(source, df);
      }
    };
  }

  @Bean
  public Converter<String, LocalDateTime> localDateTimeConverter() {
    return new Converter<String, LocalDateTime>() {
      @Override
      public LocalDateTime convert(String source) {
        if (StrUtil.isBlank(source)) {
          return null;
        }

        DateTimeFormatter df = DateTimeFormatter.ofPattern(CalendarConstants.DATE_TIME_FORMAT);
        return LocalDateTime.parse(source, df);
      }
    };
  }

  @Bean
  public Converter<String, LocalTime> localTimeConverter() {
    return new Converter<String, LocalTime>() {
      @Override
      public LocalTime convert(String source) {
        if (StrUtil.isBlank(source)) {
          return null;
        }

        DateTimeFormatter df = DateTimeFormatter.ofPattern(CalendarConstants.TIME_FORMAT);
        return LocalTime.parse(source, df);
      }
    };
  }

  @Bean
  public Converter<String, Date> dateConverter() {
    return new Converter<String, Date>() {
      @Override
      public Date convert(String source) {
        if (StrUtil.isBlank(source)) {
          return null;
        }

        SimpleDateFormat format = new SimpleDateFormat(CalendarConstants.DATE_TIME_FORMAT);
        try {
          return format.parse(source);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
      }
    };
  }

  @Bean
  public Converter<String, MonthDay> monthDayConverter() {
    return new Converter<String, MonthDay>() {
      @Override
      public MonthDay convert(String source) {
        if (StrUtil.isBlank(source)) {
          return null;
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(CalendarConstants.MONTH_DAY_FORMAT);
        return MonthDay.parse(source, dateTimeFormatter);
      }
    };
  }

  @Bean
  public Converter<String, YearMonth> yearMonthConverter() {
    return new Converter<String, YearMonth>() {
      @Override
      public YearMonth convert(String source) {
        if (StrUtil.isBlank(source)) {
          return null;
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(CalendarConstants.YEAR_MONTH_FORMAT);
        return YearMonth.parse(source, dateTimeFormatter);
      }
    };
  }
}
