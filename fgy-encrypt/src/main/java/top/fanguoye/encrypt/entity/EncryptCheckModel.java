package top.fanguoye.encrypt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：Vance
 * @Description：自定义的可被允许的服务器硬件信息的实体类（如果校验其他参数，可自行补充）
 * @Date：2022/2/10 11:00
 */
@Data
@Accessors(chain = true)
@ApiModel("自定义的可被允许的服务器硬件信息的实体类")
public class EncryptCheckModel implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 可被允许的IP地址
   */
  @ApiModelProperty(value = "可被允许的IP地址")
  private List<String> ipAddress;

  /**
   * 可被允许的MAC地址
   */
  @ApiModelProperty(value = "可被允许的MAC地址")
  private List<String> macAddress;

  /**
   * 可被允许的CPU序列号
   */
  @ApiModelProperty(value = "可被允许的CPU序列号")
  private String cpuSerial;

  /**
   * 可被允许的主板序列号
   */
  @ApiModelProperty(value = "可被允许的主板序列号")
  private String mainBoardSerial;

  /**
   * 是否永久有效
   */
  @ApiModelProperty(value = "是否永久有效，当失效时间expiryTime为空时自动赋值为true，无需手动赋值")
  private Boolean isAlwaysValid;
}
