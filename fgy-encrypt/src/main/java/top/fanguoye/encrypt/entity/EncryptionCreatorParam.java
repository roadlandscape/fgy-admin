package top.fanguoye.encrypt.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：生成加密串需要的参数实体类
 * @Date：2022/2/10 17:12
 */
@Data
@Accessors(chain = true)
@ApiModel("生成加密串需要的参数实体类")
public class EncryptionCreatorParam implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * 加密的私钥
   */
  @ApiModelProperty("加密的私钥")
  @NotBlank
  private String privateKey;

  /**
   * 生效时间
   */
  @ApiModelProperty(value = "生效时间，为空生效时间为当前时间", example = "2022-01-01 00:00:00")
  private LocalDateTime issuedTime;

  /**
   * 失效时间
   */
  @ApiModelProperty(value = "失效时间，为空表示永久有效", example = "2023-01-01 23:59:59")
  private LocalDateTime expiryTime;

  /**
   * 额外的服务器硬件校验信息，无需校验可去掉
   */
  @ApiModelProperty(value = "额外的服务器硬件校验信息，无需校验可为null")
  private EncryptCheckModel encryptCheckModel;
}
