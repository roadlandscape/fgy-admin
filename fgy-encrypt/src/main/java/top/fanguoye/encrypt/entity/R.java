package top.fanguoye.encrypt.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author：Vance
 * @Description：请求响应统一数据格式
 * @Date：2021/12/14 10:17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class R<T> implements Serializable {

  private static final long serialVersionUID = 1L;

  private int code;
  private String msg;
  private T data;

  public static R ok() {
    return R.resp(200, "请求成功！");
  }

  public static R ok(String msg) {
    return R.ok().setMsg(msg);
  }

  public static R ok(Object data) {
    return R.ok().setData(data);
  }

  public static R ok(String msg, Object data) {
    return R.ok().setMsg(msg).setData(data);
  }

  public static R error() {
    return R.resp(500, "请求失败！");
  }

  public static R error(String msg) {
    return R.error().setMsg(msg);
  }

  public static R resp(int code) {
    return new R().setCode(code);
  }

  public static R resp(int code, String msg) {
    return new R(code, msg, null);
  }

  public static R resp(int code, Object data) {
    return new R(code, null, data);
  }

  public static R resp(int code, String msg, Object data) {
    return new R(code, msg, data);
  }
}