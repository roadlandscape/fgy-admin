package top.fanguoye.encrypt.service;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.fanguoye.encrypt.entity.EncryptCheckModel;
import top.fanguoye.encrypt.entity.EncryptionCreatorParam;

import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：Encryption加密串service
 * @Date：2022/2/10 16:55
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class EncryptionCreatorService {

  private final ObjectMapper objectMapper;

  /**
   * 生成加密串
   * @return
   */
  public String generateEncryption(EncryptionCreatorParam param) throws JsonProcessingException {
    // 私钥加密，公钥解密
    RSA rsa = new RSA(param.getPrivateKey(), null);

    if (param.getIssuedTime() == null) {
      param.setIssuedTime(LocalDateTime.now());
    }
    boolean isAlwaysValid = false;
    if (param.getExpiryTime() == null) {
      isAlwaysValid = true;
    }
    EncryptCheckModel checkModel = param.getEncryptCheckModel();
    if (checkModel == null) {
      checkModel = new EncryptCheckModel().setIsAlwaysValid(isAlwaysValid);
    } else {
      checkModel.setIsAlwaysValid(isAlwaysValid);
    }
    param.setEncryptCheckModel(checkModel);
    param.setPrivateKey(null);

    return rsa.encryptBase64(objectMapper.writeValueAsString(param), KeyType.PrivateKey);
  }

  /**
   * 解密加密串
   * @param publicKey
   * @param encryption
   * @return
   */
  public EncryptionCreatorParam decryptEncryption(String publicKey, String encryption) throws JsonProcessingException {
    // 私钥加密，公钥解密
    RSA rsa = new RSA(null, publicKey);
    String decryptStr = rsa.decryptStr(encryption, KeyType.PublicKey);
    return objectMapper.readValue(decryptStr, EncryptionCreatorParam.class);
  }
}
