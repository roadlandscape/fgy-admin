package top.fanguoye.encrypt.service;

import cn.hutool.system.oshi.OshiUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import oshi.hardware.ComputerSystem;
import top.fanguoye.encrypt.entity.EncryptCheckModel;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author：Vance
 * @Description：获取服务器的硬件信息
 * @Date：2022/2/10 11:02
 */
@Slf4j
@Service
public class ServerInfoService {

  /**
   * 组装需要额外校验的参数
   * @return
   */
  public EncryptCheckModel getServerInfos() throws Exception {
    EncryptCheckModel result = new EncryptCheckModel();
    result.setIpAddress(this.getIpAddress());
    result.setMacAddress(this.getMacAddress());
    result.setCpuSerial(this.getCPUSerial());
    result.setMainBoardSerial(this.getMainBoardSerial());
    return result;
  }


  /**
   * 获取IP地址
   * @return
   * @throws Exception
   */
  public List<String> getIpAddress() throws Exception {
    List<String> result = null;

    //获取所有网络接口
    List<InetAddress> inetAddresses = getLocalAllInetAddress();

    if (inetAddresses != null && inetAddresses.size() > 0) {
      result = inetAddresses.stream()
          .map(InetAddress::getHostAddress).distinct().map(String::toLowerCase).collect(Collectors.toList());
    }

    return result;
  }

  /**
   * 获取Mac地址
   * @return
   * @throws Exception
   */
  public List<String> getMacAddress() throws Exception {
    List<String> result = null;

    // 1. 获取所有网络接口
    List<InetAddress> inetAddresses = getLocalAllInetAddress();

    if (inetAddresses != null && inetAddresses.size() > 0) {
      //2. 获取所有网络接口的Mac地址
      result = inetAddresses.stream().map(this::getMacByInetAddress).distinct().collect(Collectors.toList());
    }

    return result;
  }

  /**
   * 获取CPU序列号
   * @return
   * @throws Exception
   */
  public String getCPUSerial() throws Exception {
    ComputerSystem system = OshiUtil.getSystem();
    return system.getHardwareUUID();
  }

  /**
   * 获取主板序列号
   * @return
   * @throws Exception
   */
  public String getMainBoardSerial() throws Exception {
    ComputerSystem system = OshiUtil.getSystem();
    return system.getBaseboard().getSerialNumber();
  }


  /**
   * 获取当前服务器所有符合条件的InetAddress
   * @return
   * @throws Exception
   */
  public List<InetAddress> getLocalAllInetAddress() throws Exception {
    List<InetAddress> result = new ArrayList<>();

    // 遍历所有的网络接口
    Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
    while (networkInterfaces.hasMoreElements()) {
      NetworkInterface iface = networkInterfaces.nextElement();
      Enumeration<InetAddress> inetAddresses = iface.getInetAddresses();
      while (inetAddresses.hasMoreElements()) {
        InetAddress inetAddr = inetAddresses.nextElement();
        // 排除LoopbackAddress、SiteLocalAddress、LinkLocalAddress、MulticastAddress类型的IP地址
        if (!inetAddr.isLoopbackAddress()
            && !inetAddr.isLinkLocalAddress() && !inetAddr.isMulticastAddress()){
          result.add(inetAddr);
        }
      }
    }

    return result;
  }


  /**
   * 获取某个网络接口的Mac地址
   * @param inetAddr
   * @return
   */
  public String getMacByInetAddress(InetAddress inetAddr) {
    try {
      byte[] mac = NetworkInterface.getByInetAddress(inetAddr).getHardwareAddress();
      StringBuilder stringBuilder = new StringBuilder();

      for (int i = 0; i < mac.length; i++) {
        if (i != 0) {
          stringBuilder.append("-");
        }

        // 将十六进制byte转化为字符串
        String temp = Integer.toHexString(mac[i] & 0xff);
        if (temp.length() == 1) {
          stringBuilder.append("0").append(temp);
        } else {
          stringBuilder.append(temp);
        }
      }

      return stringBuilder.toString().toUpperCase();
    } catch (SocketException e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
