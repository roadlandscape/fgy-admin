package top.fanguoye.es.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.R;
import top.fanguoye.es.entity.DirectionStatisticDto;
import top.fanguoye.es.service.TrafficFlowService;

import java.util.Map;

/**
 * @Author：Vance
 * @Description：交通流量统计
 * @Date：2022/7/19 10:27
 */
@Api(tags = "交通流量统计")
@RestController
@RequestMapping("/traffic-flow/statistic")
@RequiredArgsConstructor
public class TrafficFlowController {

  private final TrafficFlowService trafficFlowService;

  @ApiOperation("方向统计")
  @PostMapping(value = "/direction")
  public R statisticDirection(@RequestBody(required = false) DirectionStatisticDto dto) {
    Map<String, Object> data = trafficFlowService.statisticDirection(dto);
    return R.ok(data);
  }
}
