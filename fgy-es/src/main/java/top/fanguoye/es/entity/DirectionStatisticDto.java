package top.fanguoye.es.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * @Author：Vance
 * @Description：方向统计dto
 * @Date：2022/7/21 09:10
 */
@Data
@Accessors(chain = true)
@ApiModel("方向统计dto")
public class DirectionStatisticDto implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("卡口编号")
  private List<String> bayonetCodes;

  @ApiModelProperty("开始时间")
  private LocalDate startDate;

  @ApiModelProperty("结束时间")
  private LocalDate endDate;

  public LocalDate getStartDate() {
    if (startDate == null) {
      return LocalDate.now().minusDays(7);
    }
    return startDate;
  }

  public LocalDate getEndDate() {
    if (endDate == null) {
      return LocalDate.now();
    }
    return endDate;
  }
}
