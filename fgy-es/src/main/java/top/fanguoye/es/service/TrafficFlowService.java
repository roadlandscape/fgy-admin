package top.fanguoye.es.service;

import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.frameworkset.elasticsearch.client.ClientInterface;
import org.frameworkset.elasticsearch.client.ResultUtil;
import org.frameworkset.elasticsearch.entity.ESDatas;
import org.springframework.stereotype.Service;
import top.fanguoye.es.entity.DirectionStatisticDto;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：交通流量 service
 * @Date：2022/7/19 11:05
 */
@Service
public class TrafficFlowService {

  private final ClientInterface configClientUtil;

  public TrafficFlowService(BBossESStarter bbossESStarter) {
    this.configClientUtil = bbossESStarter.getConfigRestClient("esmapper/TrafficFlowMapper.xml");
  }

  public Map<String, Object> statisticDirection(DirectionStatisticDto dto) {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    String startDateFormat = dto.getStartDate().format(formatter);
    String endDateFormat = dto.getEndDate().format(formatter);
    Map<String, Object> conditionMap = new HashMap<>();
    conditionMap.put("bayonetCodes", dto.getBayonetCodes());
    // xml中不支持对jdk8日期格式化，故在此处理
    conditionMap.put("startDate", startDateFormat + " 00:00:00");
    conditionMap.put("endDate", endDateFormat + " 23:59:59");
    conditionMap.put("extendedStartDate", startDateFormat);
    conditionMap.put("extendedEndDate", endDateFormat);
    ESDatas<Map> mapESDatas = configClientUtil.searchList(
        "dwd_lz_passcar_202206/_search","statisticDirection", conditionMap, Map.class);

    // 表头数据
    List<String> tableHeader = new ArrayList<String>(){{
      add("设备名称");
      add("方向");
    }};
    // 表格内容数据
    List<Map<String, List<Map<String, List<Integer>>>>> tableData = new ArrayList<>();

    // 根据设备编号统计的接口
    List<Map<String, Object>> groupBayonetCode = mapESDatas.getAggregationBuckets("groupBayonetCode");
    for (Map<String, Object> bayonetCodeMap : groupBayonetCode) {
      Map<String, List<Map<String, List<Integer>>>> bayonetCodeResultMap = new HashMap<>();
      List<Map<String, List<Integer>>> groupDirectionResult = new ArrayList<>();
      String bayonetCode = (String) bayonetCodeMap.get("key");
      // 根据方向统计的结果
      List<Map<String, Object>> groupDirection = (List<Map<String, Object>>) ResultUtil.getAggBuckets(bayonetCodeMap, "groupDirection");
      for (Map<String, Object> directionMap : groupDirection) {
        Map<String, List<Integer>> directionResultMap = new HashMap<>();
        List<Integer> countResultList = new ArrayList<>();
        String direction = (String) directionMap.get("key");
        // 根据过车时间统计的结果
        List<Map<String, Object>> groupPassTime = (List<Map<String, Object>>) ResultUtil.getAggBuckets(directionMap, "groupPassTime");
        for (Map<String, Object> groupPassTimeMap : groupPassTime) {
          String passTime = (String) groupPassTimeMap.get("key_as_string");
          int count = (int) groupPassTimeMap.get("doc_count");
          tableHeader.add(passTime);
          countResultList.add(count);
        }
        directionResultMap.put(direction, countResultList);
        groupDirectionResult.add(directionResultMap);
      }

      bayonetCodeResultMap.put(bayonetCode, groupDirectionResult);
      tableData.add(bayonetCodeResultMap);
    }

    Map<String, Object> resultMap = new HashMap<>();
    resultMap.put("tableHeader", tableHeader);
    resultMap.put("tableData", tableData);
    return resultMap;
  }
}