package top.fanguoye.generator;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import top.fanguoye.generator.constants.GeneratorInfoConstant;
import top.fanguoye.generator.constants.JdbcConstant;

import java.util.Collections;

/**
 * @Author：Vance
 * @Description：代码生成信息
 * @Date：2021/12/11 10:21
 */
public class FastGenerator {

  public static void main(String[] args) {
    FastAutoGenerator.create(JdbcConstant.url, JdbcConstant.username, JdbcConstant.password)
        .globalConfig(builder -> {
          builder.author(GeneratorInfoConstant.username) // 设置作者
            .enableSwagger() // 开启swagger
            .commentDate(GeneratorInfoConstant.commentDate) // 注释日期
            .fileOverride() // 覆盖已生成的文件
            .outputDir(GeneratorInfoConstant.outputDir); // 指定输出目录
        })
        .packageConfig(builder -> {
          builder.parent(GeneratorInfoConstant.parentPackage) // 设置父包名
              .moduleName(GeneratorInfoConstant.moduleName) // 设置父包模块名
              .entity(GeneratorInfoConstant.entityPackageName) // 实体类包名
              .pathInfo(Collections.singletonMap(OutputFile.mapperXml, GeneratorInfoConstant.xmlPathInfo)); // 设置mapperXml生成路径
        })
        .strategyConfig(builder -> {
          builder.addInclude(GeneratorInfoConstant.tables) // 设置需要生成的表名
              .addTablePrefix(GeneratorInfoConstant.tablePrefix)
              .entityBuilder() // Entity策略配置
              .superClass(GeneratorInfoConstant.entitySuperClass)
              .idType(IdType.ASSIGN_ID)
              .addSuperEntityColumns(GeneratorInfoConstant.superEntityColumns)
              .enableColumnConstant()
              .enableLombok()
              .enableChainModel()
              // .formatFileName(GeneratorInfoConstant.entityFileNameFormat) // 实体类的文件名格式化
              .controllerBuilder() // Controller策略配置
              .enableHyphenStyle()
              .enableRestStyle()
              .mapperBuilder() // Mapper策略配置
              .enableBaseResultMap()
              .enableBaseColumnList();
      }).execute();
  }
}
