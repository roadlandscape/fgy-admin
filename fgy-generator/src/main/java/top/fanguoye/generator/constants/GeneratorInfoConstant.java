package top.fanguoye.generator.constants;

/**
 * @Author：Vance
 * @Description：用户信息
 * @Date：2021/12/13 15:36
 */
public interface GeneratorInfoConstant {

  /** 作者 */
  String username = "Vance";
  /** 注释日期 */
  String commentDate = "yyyy/MM/dd HH:mm";
  /** 输出目录 */
  String outputDir = "e://generator";
  /** mapperXml生成路径 */
  String xmlPathInfo = "e://generator/mapper";
  /** 父包名 */
  String parentPackage = "top.fanguoye";
  /** 父包模块名 */
  String moduleName = "";
  /** 实体类的包名 */
  String entityPackageName = "entity.po";
  /** 增加过滤表前缀 */
  String tablePrefix = "t_data_";
  /** 实体类的父类 */
  String entitySuperClass = "top.fanguoye.core.entity.BaseEntity";
  /** 实体类父类公共字段 */
  String[] superEntityColumns = {"id", "create_by", "create_time", "update_by", "update_time", "creator", "updater"};
  /** 实体类的文件名格式化 */
  String entityFileNameFormat = "%sPo";

  /** 需要生成的表名 */
  String[] tables = { "message" };
}
