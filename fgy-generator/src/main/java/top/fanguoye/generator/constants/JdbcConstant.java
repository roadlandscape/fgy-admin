package top.fanguoye.generator.constants;

/**
 * @Author：Vance
 * @Description：数据库相关
 * @Date：2021/12/13 15:34
 */
public interface JdbcConstant {

  String url = "jdbc:mysql://127.0.0.1:3306/test01?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai&useSSL=false&connectTimeout=30000&socketTimeout=60000&rewriteBatchedStatements=true";
  String username = "root";
  String password = "root";
}
