package top.fanguoye.im.entity.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.entity.BasePage;
import top.fanguoye.core.enums.ChatType;

import javax.validation.constraints.NotNull;

/**
 * @Author：Vance
 * @Description：im聊天消息分页查询dto
 * @Date：2022/3/20 17:38
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ImMessagePageDto对象", description = "im聊天消息分页查询dto")
public class ImMessagePageDto extends BasePage {

  @ApiModelProperty(value = "如果是单聊，是接收者的ID，如果是多聊，是群的ID", required = true)
  @NotNull
  private Long chatId;

  @ApiModelProperty(value = "聊天类型：1单聊 2群聊", allowableValues = "1,2")
  private ChatType type;
}
