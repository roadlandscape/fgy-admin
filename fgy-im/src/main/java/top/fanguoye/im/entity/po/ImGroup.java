package top.fanguoye.im.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.interfaces.Insert;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * im聊天群
 * </p>
 *
 * @author Vance
 * @since 2022/03/20 23:11
 */
@Data
@Accessors(chain = true)
@TableName("im_group")
@ApiModel(value = "ImGroup对象", description = "im聊天群")
public class ImGroup extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("群名称")
    @NotBlank(groups = Insert.class)
    private String name;

    @ApiModelProperty("群头像")
    private String avatar;

    @ApiModelProperty("群主id")
    private Long masterId;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("逻辑删除")
    private Boolean deleted;

    /** 表别名 */
    public static final String TABLE_ALIAS = "ig";

    public static final String NAME = "name";
    public static final String AVATAR = "avatar";
    public static final String MASTER_ID = "master_id";
    public static final String REMARK = "remark";
    public static final String DELETED = "deleted";
}
