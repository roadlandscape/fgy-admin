package top.fanguoye.im.entity.po;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * im群用户关联表
 * </p>
 *
 * @author Vance
 * @since 2022/03/20 23:11
 */
@Data
@Accessors(chain = true)
@TableName("im_group_user")
@ApiModel(value = "ImGroupUser对象", description = "im群用户关联表")
public class ImGroupUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @ApiModelProperty("群id")
    private Long groupId;

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty(value = "入群时间")
    private LocalDateTime createTime;

    /** 表别名 */
    public static final String TABLE_ALIAS = "igu";

    public static final String GROUP_ID = "group_id";
    public static final String USER_ID = "user_id";
}
