package top.fanguoye.im.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.ChatType;

/**
 * <p>
 * im聊天消息
 * </p>
 *
 * @author Vance
 * @since 2022/03/20 17:17
 */
@Data
@Accessors(chain = true)
@TableName("im_message")
@ApiModel(value = "ImMessage对象", description = "im聊天消息")
public class ImMessage extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("如果是单聊，是接收者的ID，如果是多聊，是群的ID")
    private Long chatId;

    @ApiModelProperty("发送者id")
    private Long fromId;

    @ApiModelProperty("发送内容")
    private String content;

    @ApiModelProperty("聊天类型：1单聊 2群聊")
    private ChatType type;

    @ApiModelProperty("逻辑删除")
    private Boolean deleted;

    /** 表别名 */
    public static final String TABLE_ALIAS = "im";

    public static final String CHAT_ID = "chat_id";
    public static final String FROM_ID = "from_id";
    public static final String CONTENT = "content";
    public static final String TYPE = "type";
    public static final String DELETED = "deleted";
}
