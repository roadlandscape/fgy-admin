package top.fanguoye.im.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * im聊天消息读取
 * </p>
 *
 * @author Vance
 * @since 2022/03/20 17:17
 */
@Data
@Accessors(chain = true)
@TableName("im_msg_read")
@ApiModel(value = "ImMsgRead对象", description = "im聊天消息读取")
public class ImMsgRead extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("im消息id")
    private Long imId;

    @ApiModelProperty("如果是单聊，是接收者的ID，如果是多聊，是群的ID")
    private Long chatId;

    @ApiModelProperty("发送者id")
    private Long fromId;

    @ApiModelProperty("消息读取者id")
    private Long readId;

    @ApiModelProperty("是否已读 0:未读 1:已读")
    @TableField("`read`")
    private Boolean read;

    /** 表别名 */
    public static final String TABLE_ALIAS = "imr";

    public static final String IM_ID = "im_id";
    public static final String CHAT_ID = "chat_id";
    public static final String FROM_ID = "from_id";
    public static final String READ_ID = "read_id";
    public static final String READ = "read";
}
