package top.fanguoye.im.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.ChatType;

import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：
 * @Date：2022/3/20 20:07
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "ImMessageVo对象", description = "im聊天消息vo")
public class ImMessageVo {

  private Long id;

  @ApiModelProperty("如果是单聊，是接收者的ID，如果是多聊，是群的ID")
  private Long chatId;

  @ApiModelProperty("发送者id")
  private Long fromId;
  @ApiModelProperty("发送者名称")
  private String fromName;

  @ApiModelProperty("发送内容")
  private String content;

  @ApiModelProperty("聊天类型：1单聊 2群聊")
  private ChatType type;

  @ApiModelProperty("创建时间")
  private LocalDateTime createTime;

  @ApiModelProperty("是否是自己发送的信息")
  private Boolean isSelf;
}
