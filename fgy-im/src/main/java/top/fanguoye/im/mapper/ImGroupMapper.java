package top.fanguoye.im.mapper;

import top.fanguoye.im.entity.po.ImGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * im聊天群 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
public interface ImGroupMapper extends BaseMapper<ImGroup> {

}
