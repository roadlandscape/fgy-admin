package top.fanguoye.im.mapper;

import top.fanguoye.im.entity.po.ImGroupUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * im群用户关联表 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
public interface ImGroupUserMapper extends BaseMapper<ImGroupUser> {

}
