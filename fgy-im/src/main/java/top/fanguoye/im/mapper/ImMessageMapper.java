package top.fanguoye.im.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import top.fanguoye.im.entity.po.ImMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.fanguoye.im.entity.vo.ImMessageVo;

/**
 * <p>
 * im聊天消息 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
public interface ImMessageMapper extends BaseMapper<ImMessage> {

  /**
   * 分页查询聊天记录
   * @param page
   * @param queryWrapper
   * @param fromId
   * @return
   */
  Page<ImMessageVo> getPage(Page<ImMessageVo> page, @Param(Constants.WRAPPER) Wrapper<ImMessage> queryWrapper, Long fromId);
}
