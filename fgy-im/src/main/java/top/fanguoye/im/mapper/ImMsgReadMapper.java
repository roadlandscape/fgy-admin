package top.fanguoye.im.mapper;

import top.fanguoye.im.entity.po.ImMsgRead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * im聊天消息读取 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
public interface ImMsgReadMapper extends BaseMapper<ImMsgRead> {

}
