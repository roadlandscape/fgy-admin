package top.fanguoye.im.service;

import top.fanguoye.im.entity.po.ImGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Set;

/**
 * <p>
 * im聊天群 服务类
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
public interface IImGroupService extends IService<ImGroup> {

  /**
   * 添加群成员
   * @param groupId
   * @param userIds
   */
  void addUsers(Long groupId, Set<Long> userIds);

  /**
   * 移除群成员
   * @param groupId
   * @param userIds
   */
  void removeUsers(Long groupId, Set<Long> userIds);
}
