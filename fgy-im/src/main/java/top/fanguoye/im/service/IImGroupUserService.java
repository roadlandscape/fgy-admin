package top.fanguoye.im.service;

import top.fanguoye.im.entity.po.ImGroupUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * im群用户关联表 服务类
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
public interface IImGroupUserService extends IService<ImGroupUser> {

  /**
   * 通过群id查询群用户
   * @param groupId
   * @return
   */
  List<ImGroupUser> selectByGroupId(Long groupId);
}
