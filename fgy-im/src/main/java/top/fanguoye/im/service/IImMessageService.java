package top.fanguoye.im.service;

import top.fanguoye.core.entity.PageData;
import top.fanguoye.im.entity.dto.ImMessagePageDto;
import top.fanguoye.im.entity.po.ImMessage;
import com.baomidou.mybatisplus.extension.service.IService;
import top.fanguoye.im.entity.vo.ImMessageVo;

/**
 * <p>
 * im聊天消息 服务类
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
public interface IImMessageService extends IService<ImMessage> {

  /**
   * 分页查询聊天记录
   * @param pageDto
   * @param fromId
   * @return
   */
  PageData<ImMessageVo> page(ImMessagePageDto pageDto, Long fromId);
}
