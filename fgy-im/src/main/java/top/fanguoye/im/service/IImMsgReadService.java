package top.fanguoye.im.service;

import top.fanguoye.im.entity.po.ImMsgRead;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * im聊天消息读取 服务类
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
public interface IImMsgReadService extends IService<ImMsgRead> {

}
