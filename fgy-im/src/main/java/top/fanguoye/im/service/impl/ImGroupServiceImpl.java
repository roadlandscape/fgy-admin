package top.fanguoye.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import top.fanguoye.im.entity.po.ImGroup;
import top.fanguoye.im.entity.po.ImGroupUser;
import top.fanguoye.im.mapper.ImGroupMapper;
import top.fanguoye.im.service.IImGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.fanguoye.im.service.IImGroupUserService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * im聊天群 服务实现类
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
@Service("imGroupService")
@RequiredArgsConstructor
public class ImGroupServiceImpl extends ServiceImpl<ImGroupMapper, ImGroup> implements IImGroupService {

  private final IImGroupUserService imGroupUserService;

  @Override
  public void addUsers(Long groupId, Set<Long> userIds) {
    LocalDateTime now = LocalDateTime.now();
    List<ImGroupUser> list = new ArrayList<>(userIds.size());
    for (Long userId : userIds) {
      ImGroupUser imGroupUser = new ImGroupUser();
      imGroupUser.setGroupId(groupId).setUserId(userId).setCreateTime(now);
      list.add(imGroupUser);
    }
    imGroupUserService.saveBatch(list);
  }

  @Override
  public void removeUsers(Long groupId, Set<Long> userIds) {
    LambdaQueryWrapper<ImGroupUser> queryWrapper = new LambdaQueryWrapper<>();
    queryWrapper.in(ImGroupUser::getUserId, userIds).eq(ImGroupUser::getGroupId, groupId);
    imGroupUserService.remove(queryWrapper);
  }
}
