package top.fanguoye.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import top.fanguoye.im.entity.po.ImGroupUser;
import top.fanguoye.im.mapper.ImGroupUserMapper;
import top.fanguoye.im.service.IImGroupUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * im群用户关联表 服务实现类
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
@Service("imGroupUserService")
public class ImGroupUserServiceImpl extends ServiceImpl<ImGroupUserMapper, ImGroupUser> implements IImGroupUserService {

  @Override
  public List<ImGroupUser> selectByGroupId(Long groupId) {
    LambdaQueryWrapper<ImGroupUser> queryWrapper = new LambdaQueryWrapper<>();
    queryWrapper.eq(ImGroupUser::getGroupId, groupId);
    return this.list(queryWrapper);
  }
}
