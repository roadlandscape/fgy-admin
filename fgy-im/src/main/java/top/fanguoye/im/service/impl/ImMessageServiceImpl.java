package top.fanguoye.im.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import top.fanguoye.core.entity.BaseEntity;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.enums.ChatType;
import top.fanguoye.core.utils.StrUtils;
import top.fanguoye.im.entity.dto.ImMessagePageDto;
import top.fanguoye.im.entity.po.ImMessage;
import top.fanguoye.im.entity.po.ImMsgRead;
import top.fanguoye.im.entity.vo.ImMessageVo;
import top.fanguoye.im.mapper.ImMessageMapper;
import top.fanguoye.im.service.IImMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.fanguoye.im.service.IImMsgReadService;

/**
 * <p>
 * im聊天消息 服务实现类
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
@Service("imMessageService")
@RequiredArgsConstructor
public class ImMessageServiceImpl extends ServiceImpl<ImMessageMapper, ImMessage> implements IImMessageService {

  private final IImMsgReadService imMsgReadService;

  @Override
  public PageData<ImMessageVo> page(ImMessagePageDto pageDto, Long fromId) {
    Page<ImMessageVo> page = Page.of(pageDto.getCurrent(), pageDto.getSize(), pageDto.getSearchCount());
    QueryWrapper<ImMessage> queryWrapper = new QueryWrapper<>();

    ChatType type = pageDto.getType();
    if (type != null && type.equals(ChatType.GROUP)) {
      queryWrapper.eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.CHAT_ID), pageDto.getChatId());
    } else {
      type = ChatType.SINGLE;
      queryWrapper.and(
          wrapper -> wrapper.eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.CHAT_ID), pageDto.getChatId())
              .eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.FROM_ID), fromId)
      ).or(
          wrapper -> wrapper.eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.CHAT_ID), fromId)
              .eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.FROM_ID), pageDto.getChatId())
      );
    }
    queryWrapper.eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.TYPE), type)
        .eq(StrUtils.joinPoint(ImMessage.TABLE_ALIAS, ImMessage.DELETED), false)
        .orderByDesc(BaseEntity.CREATE_TIME);
    this.baseMapper.getPage(page, queryWrapper, fromId);

    // 封装返回结果
    PageData<ImMessageVo> data = new PageData<>();
    data.setCurrent(page.getCurrent())
        .setSize(page.getSize())
        .setTotal(page.getTotal())
        .setPages(page.getPages())
        .setRecords(page.getRecords());

    LambdaUpdateWrapper<ImMsgRead> updateWrapper = new LambdaUpdateWrapper<>();
    if (type.equals(ChatType.SINGLE)) {
      updateWrapper.eq(ImMsgRead::getChatId, fromId).eq(ImMsgRead::getFromId, pageDto.getChatId());
    } else {
      updateWrapper.eq(ImMsgRead::getChatId, pageDto.getChatId());
    }
    updateWrapper.eq(ImMsgRead::getReadId, fromId)
        .eq(ImMsgRead::getRead, false)
        .set(ImMsgRead::getRead, true);
    imMsgReadService.update(updateWrapper);
    return data;
  }
}
