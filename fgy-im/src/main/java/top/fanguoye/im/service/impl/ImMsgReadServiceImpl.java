package top.fanguoye.im.service.impl;

import top.fanguoye.im.entity.po.ImMsgRead;
import top.fanguoye.im.mapper.ImMsgReadMapper;
import top.fanguoye.im.service.IImMsgReadService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * im聊天消息读取 服务实现类
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
@Service("imMsgReadService")
public class ImMsgReadServiceImpl extends ServiceImpl<ImMsgReadMapper, ImMsgRead> implements IImMsgReadService {

}
