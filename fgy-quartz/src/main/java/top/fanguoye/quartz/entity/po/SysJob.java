package top.fanguoye.quartz.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import top.fanguoye.core.annotation.Cron;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.interfaces.Insert;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 定时任务调度
 * </p>
 *
 * @author Vance
 * @since 2022/01/14 15:24
 */
@Data
@Accessors(chain = true)
@TableName("sys_job")
@ApiModel(value = "SysJob对象", description = "定时任务调度")
public class SysJob extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("任务名称")
    @NotBlank(groups = Insert.class)
    private String jobName;

    @ApiModelProperty("任务组名")
    private String jobGroup;

    @ApiModelProperty("调用方法")
    @NotBlank(groups = Insert.class)
    private String invokeMethod;

    @ApiModelProperty("cron表达式")
    @NotBlank(groups = Insert.class)
    @Cron
    private String cronExpression;

    @ApiModelProperty("报警邮箱，多个用逗号分隔")
    private String email;

    @ApiModelProperty(value = "任务状态 1:正常 2:停用", allowableValues = "1,2")
    private Status status;

    @ApiModelProperty("失败后是否暂停")
    private Boolean failurePause;

    @ApiModelProperty("备注")
    private String remark;

    public static final String TASK_PROPERTIES = "TASK_PROPERTIES";

    /** 表别名 */
    public static final String TABLE_ALIAS = "j";

    public static final String JOB_NAME = "job_name";
    public static final String JOB_GROUP = "job_group";
    public static final String INVOKE_METHOD = "invoke_method";
    public static final String CRON_EXPRESSION = "cron_expression";
    public static final String CONCURRENT = "concurrent";
    public static final String EMAIL = "email";
    public static final String STATUS = "status";
    public static final String FAILURE_PAUSE = "failure_pause";
    public static final String REMARK = "remark";
}
