package top.fanguoye.quartz.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 定时任务调度日志
 * </p>
 *
 * @author Vance
 * @since 2022/01/17 16:59
 */
@Data
@Accessors(chain = true)
@TableName("sys_job_log")
@ApiModel(value = "SysJobLog对象", description = "定时任务调度日志")
public class SysJobLog extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("任务名称")
    private String jobName;

    @ApiModelProperty("任务组名")
    private String jobGroup;

    @ApiModelProperty("调用方法")
    private String invokeMethod;

    @ApiModelProperty("日志信息")
    private String jobMessage;

    @ApiModelProperty("执行状态")
    private Boolean status;

    @ApiModelProperty("异常信息")
    private String exceptionInfo;

    @ApiModelProperty("执行开始时间")
    private LocalDateTime startTime;

    @ApiModelProperty("执行结束时间")
    private LocalDateTime stopTime;

    /** 表别名 */
    public static final String TABLE_ALIAS = "jl";

    public static final String JOB_NAME = "job_name";
    public static final String JOB_GROUP = "job_group";
    public static final String INVOKE_METHOD = "invoke_method";
    public static final String JOB_MESSAGE = "job_message";
    public static final String STATUS = "status";
    public static final String EXCEPTION_INFO = "exception_info";
    public static final String START_TIME = "start_time";
    public static final String STOP_TIME = "stop_time";
}
