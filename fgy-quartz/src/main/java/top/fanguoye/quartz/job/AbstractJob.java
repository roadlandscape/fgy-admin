package top.fanguoye.quartz.job;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import top.fanguoye.core.enums.Status;
import top.fanguoye.quartz.entity.po.SysJob;
import top.fanguoye.quartz.entity.po.SysJobLog;
import top.fanguoye.quartz.service.ISysJobLogService;
import top.fanguoye.quartz.service.ISysJobService;
import top.fanguoye.quartz.utils.ScheduleUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：抽象job
 * @Date：2022/1/14 16:05
 */
@Slf4j
public abstract class AbstractJob implements Job {

  private static final ThreadLocal<LocalDateTime> THREAD_LOCAL = new ThreadLocal<>();

  /**
   * 执行方法，由子类重载
   * @param context 工作执行上下文对象
   * @param sysJob 系统计划任务
   * @throws Exception 执行过程中的异常
   */
  protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception {
    ScheduleUtils.invokeScheduleJob(sysJob);
  }

  @SneakyThrows
  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    SysJob sysJob = null;
    try {
      sysJob = (SysJob) context.getMergedJobDataMap().get(SysJob.TASK_PROPERTIES);
      before(context, sysJob);
      if (sysJob != null) {
        doExecute(context, sysJob);
      }
      after(context, sysJob, null);
    } catch (Exception e) {
      log.error("任务执行异常 - ", e);
      after(context, sysJob, e);
    } finally {
      THREAD_LOCAL.remove();
    }
  }

  /**
   * 执行前
   * @param context
   * @param sysJob
   */
  protected void before(JobExecutionContext context, SysJob sysJob) {
    THREAD_LOCAL.set(LocalDateTime.now());
  }

  /**
   * 执行后
   *
   * @param context 工作执行上下文对象
   * @param sysJob 系统计划任务
   */
  protected void after(JobExecutionContext context, SysJob sysJob, Exception e) throws SchedulerException {
    LocalDateTime startTime = THREAD_LOCAL.get();

    SysJobLog sysJobLog = new SysJobLog();
    sysJobLog.setJobName(sysJob.getJobName());
    sysJobLog.setJobGroup(sysJob.getJobGroup());
    sysJobLog.setInvokeMethod(sysJob.getInvokeMethod());
    sysJobLog.setStartTime(startTime);
    sysJobLog.setStopTime(LocalDateTime.now());
    long millis = Duration.between(sysJobLog.getStartTime(), sysJobLog.getStopTime()).toMillis();
    sysJobLog.setJobMessage(sysJobLog.getJobName() + " 总共耗时：" + millis + "毫秒");
    if (e != null) {
      log.error("执行任务失败 - ", e);
      sysJobLog.setStatus(false);
      StringWriter sw = new StringWriter();
      e.printStackTrace(new PrintWriter(sw, true));
      sysJobLog.setExceptionInfo(sw.toString());
    }
    SpringUtil.getBean(ISysJobLogService.class).save(sysJobLog);

    if (e != null) {
      ISysJobService sysJobService = SpringUtil.getBean(ISysJobService.class);
      sysJob.setStatus(Status.DISABLE);
      // 执行失败，更改状态
      sysJobService.updateById(sysJob);
      // 暂停任务
      Scheduler scheduler = SpringUtil.getBean(Scheduler.class);
      scheduler.pauseJob(ScheduleUtils.getJobKey(sysJob));
      // 邮箱报警 todo
      if (StrUtil.isNotBlank(sysJob.getEmail())) {
        String[] emails = sysJob.getEmail().split("[,，]");
        System.out.println(emails);
      }
    }
  }
}
