package top.fanguoye.quartz.job;

import org.quartz.DisallowConcurrentExecution;

/**
 * @Author：Vance
 * @Description：禁止并发执行job
 * @Date：2022/1/14 17:31
 */
@DisallowConcurrentExecution
public class DisallowConcurrentExecutionJob extends AbstractJob {
}
