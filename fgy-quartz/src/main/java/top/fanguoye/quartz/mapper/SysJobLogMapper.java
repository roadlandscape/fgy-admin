package top.fanguoye.quartz.mapper;

import top.fanguoye.quartz.entity.po.SysJobLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务调度日志 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/01/17 16:59
 */
public interface SysJobLogMapper extends BaseMapper<SysJobLog> {

  /**
   * 清空定时任务调度日志
   */
  void cleanJobLog();
}
