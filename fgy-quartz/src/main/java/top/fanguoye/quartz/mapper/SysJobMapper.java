package top.fanguoye.quartz.mapper;

import top.fanguoye.quartz.entity.po.SysJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 定时任务调度 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/01/14 15:24
 */
public interface SysJobMapper extends BaseMapper<SysJob> {

}
