package top.fanguoye.quartz.service;

import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.quartz.entity.po.SysJobLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 定时任务调度日志 服务类
 * </p>
 * @author Vance
 * @since 2022/01/17 16:59
 */
public interface ISysJobLogService extends IService<SysJobLog> {

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysJobLog> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysJobLog> page(PageDomain pageDomain);

  /**
   * 清空定时任务调度日志
   */
  void cleanJobLog();
}
