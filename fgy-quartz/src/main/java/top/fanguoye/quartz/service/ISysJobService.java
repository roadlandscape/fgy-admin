package top.fanguoye.quartz.service;

import org.quartz.SchedulerException;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.quartz.entity.po.SysJob;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 定时任务调度 服务类
 * </p>
 * @author Vance
 * @since 2022/01/14 15:24
 */
public interface ISysJobService extends IService<SysJob> {

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysJob> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysJob> page(PageDomain pageDomain);

  /**
   * 执行一次定时任务
   * @param id
   * @throws SchedulerException
   */
  void run(Long id) throws SchedulerException;
}
