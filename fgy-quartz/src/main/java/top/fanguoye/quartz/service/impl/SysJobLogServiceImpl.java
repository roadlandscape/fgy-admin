package top.fanguoye.quartz.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.quartz.entity.po.SysJobLog;
import top.fanguoye.quartz.mapper.SysJobLogMapper;
import top.fanguoye.quartz.service.ISysJobLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 定时任务调度日志 服务实现类
 * </p>
 * @author Vance
 * @since 2022/01/17 16:59
 */
@Service("sysJobLogService")
public class SysJobLogServiceImpl extends ServiceImpl<SysJobLogMapper, SysJobLog> implements ISysJobLogService {

  @Override
  public List<SysJobLog> list(QueryCondition condition) {
    if (condition == null) {
      return this.list();
    }
    QueryWrapper wrapper = ServiceUtils.builderQueryWrapper(condition, null);
    return this.list(wrapper);
  }

  @Override
  public PageData<SysJobLog> page(PageDomain pageDomain) {
    Page<SysJobLog> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), null);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      QueryWrapper queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, null);
      this.page(page, queryWrapper);
    } else {
      this.page(page);
    }
    // 封装返回结果
    PageData<SysJobLog> data = new PageData<>();
    data.setCurrent(page.getCurrent())
      .setSize(page.getSize())
      .setTotal(page.getTotal())
      .setPages(page.getPages())
      .setRecords(page.getRecords());
    return data;
  }

  @Override
  public void cleanJobLog() {
    this.baseMapper.cleanJobLog();
  }

}
