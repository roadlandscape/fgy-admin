package top.fanguoye.quartz.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.quartz.JobDataMap;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.transaction.annotation.Transactional;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.quartz.entity.po.SysJob;
import top.fanguoye.quartz.mapper.SysJobMapper;
import top.fanguoye.quartz.service.ISysJobService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.fanguoye.quartz.utils.ScheduleUtils;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * <p>
 * 定时任务调度 服务实现类
 * </p>
 * @author Vance
 * @since 2022/01/14 15:24
 */
@Service("sysJobService")
@RequiredArgsConstructor
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements ISysJobService {

  private final Scheduler scheduler;

  /**
   * 项目启动时，初始化定时器 主要是防止手动修改数据库导致未同步到定时任务处理
   * 注：不能手动修改数据库ID和任务组名，否则会导致脏数据
   * @throws SchedulerException
   */
  @PostConstruct
  public void init() throws SchedulerException {
    scheduler.clear();
    List<SysJob> jobList = this.list();
    for (SysJob job : jobList) {
      ScheduleUtils.createScheduleJob(job);
    }
  }

  @Override
  public List<SysJob> list(QueryCondition condition) {
    if (condition == null) {
      return this.list();
    }
    QueryWrapper wrapper = ServiceUtils.builderQueryWrapper(condition, null);
    return this.list(wrapper);
  }

  @Override
  public PageData<SysJob> page(PageDomain pageDomain) {
    Page<SysJob> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), null);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      QueryWrapper queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, null);
      this.page(page, queryWrapper);
    } else {
      this.page(page);
    }
    // 封装返回结果
    PageData<SysJob> data = new PageData<>();
    data.setCurrent(page.getCurrent())
      .setSize(page.getSize())
      .setTotal(page.getTotal())
      .setPages(page.getPages())
      .setRecords(page.getRecords());
    return data;
  }

  @Transactional(rollbackFor = Exception.class)
  @Override
  public void run(Long id) throws SchedulerException {
    SysJob sysJob = this.getById(id);
    if (sysJob == null) {
      throw new ServiceException("没有找到对应的任务！");
    }
    JobDataMap dataMap = new JobDataMap();
    dataMap.put(SysJob.TASK_PROPERTIES, sysJob);
    scheduler.triggerJob(ScheduleUtils.getJobKey(sysJob), dataMap);
  }

}
