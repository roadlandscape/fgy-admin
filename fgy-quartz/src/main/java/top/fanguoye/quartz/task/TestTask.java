package top.fanguoye.quartz.task;

import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Component;

/**
 * @Author：Vance
 * @Description：定时任务调度测试
 * @Date：2022/1/16 22:50
 */
@Component("testTask")
public class TestTask {

  public void multipleParams(String s, Boolean b, Long l, Double d, Integer i) {
    System.out.println(StrUtil.format("=========执行多参方法： 字符串类型{}，布尔类型{}，长整型{}，浮点型{}，整形{}========="
        , s, b, l, d, i));
  }

  public void params(String params) {
    System.out.println("================执行有参方法（抛异常）：" + params + "================");
    int i = 1/0;
  }

  public void noParams() {
    System.out.println("================执行无参方法================");
  }
}
