package top.fanguoye.quartz.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.quartz.*;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.core.utils.MethodUtils;
import top.fanguoye.quartz.entity.po.SysJob;
import top.fanguoye.quartz.job.DisallowConcurrentExecutionJob;

import java.text.ParseException;
import java.util.Date;

/**
 * @Author：Vance
 * @Description：定时任务工具类
 * @Date：2022/1/14 17:47
 */
@Slf4j
public class ScheduleUtils {

  /**
   * 创建定时任务
   * @param sysJob
   * @throws SchedulerException
   */
  public static void createScheduleJob(SysJob sysJob) throws SchedulerException {
    // 构建job信息
    Class<? extends Job> jobClass = getQuartzJobClass(sysJob);
    JobKey jobKey = getJobKey(sysJob);
    JobDetail jobDetail = JobBuilder.newJob(jobClass).withIdentity(jobKey)
        .withDescription(sysJob.getRemark()).build();
    // 放入参数，运行时的方法可以获取
    jobDetail.getJobDataMap().put(SysJob.TASK_PROPERTIES, sysJob);

    // 表达式调度构建器
    CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(sysJob.getCronExpression());
    cronScheduleBuilder = handleCronScheduleMisfirePolicy(sysJob, cronScheduleBuilder);

    // 构建trigger
    CronTrigger trigger = TriggerBuilder.newTrigger().withDescription(sysJob.getRemark())
        .withIdentity(getTriggerKey(sysJob)).withSchedule(cronScheduleBuilder).build();

    Scheduler scheduler = SpringUtil.getBean(Scheduler.class);
    if (scheduler.checkExists(jobKey)) {
      // 防止创建时存在数据问题 先移除，然后再执行创建操作
      scheduler.deleteJob(jobKey);
    }
    scheduler.scheduleJob(jobDetail, trigger);

    if (Status.DISABLE.equals(sysJob.getStatus())) {
      scheduler.pauseJob(jobKey);
    }
  }

  /**
   * 修改定时任务
   * @param oldJob
   * @param newJob
   */
  public static void updateScheduleJob(SysJob oldJob, SysJob newJob) throws SchedulerException {
    Scheduler scheduler = SpringUtil.getBean(Scheduler.class);
    JobKey jobKey = ScheduleUtils.getJobKey(oldJob);
    if (scheduler.checkExists(jobKey)) {
      scheduler.deleteJob(jobKey);
    }
    ScheduleUtils.createScheduleJob(newJob);
  }

  /**
   * 触发job
   * @param sysJob
   * @throws Exception
   */
  public static void invokeScheduleJob(SysJob sysJob) throws Exception {
    MethodUtils.invokeMethod(sysJob.getInvokeMethod());
  }

  /**
   * 获取job
   * @param sysJob 执行计划
   * @return 具体执行任务类
   */
  private static Class<? extends Job> getQuartzJobClass(SysJob sysJob) {
    return DisallowConcurrentExecutionJob.class;
  }

  /**
   * 获取JobKey
   * @param sysJob
   * @return
   */
  public static JobKey getJobKey(SysJob sysJob) {
    if (StrUtil.isBlank(sysJob.getJobGroup())) {
      sysJob.setJobGroup(Constants.DEFAULT_GROUP);
    }
    return JobKey.jobKey(String.valueOf(sysJob.getId()), sysJob.getJobGroup());
  }

  /**
   * 设置定时任务策略
   * @param sysJob
   * @param cb
   * @return
   */
  private static CronScheduleBuilder handleCronScheduleMisfirePolicy(SysJob sysJob, CronScheduleBuilder cb) {
    /*
     * 以错过的第一个频率时间立刻开始执行
     * 重做错过的所有频率周期后
     * 当下一次触发频率发生时间大于当前时间后，再按照正常的Cron频率依次执行
     */
    // return cb.withMisfireHandlingInstructionIgnoreMisfires();
    /*
     * 以当前时间为触发频率立刻触发一次执行
     * 然后按照Cron频率依次执行
     */
    // return cb.withMisfireHandlingInstructionFireAndProceed();
    /*
     * 不触发立即执行
     * 等待下次Cron触发频率到达时刻开始按照Cron频率依次执行
     */
    return cb.withMisfireHandlingInstructionDoNothing();
  }

  /**
   * 获取触发key
   * @param sysJob
   * @return
   */
  public static TriggerKey getTriggerKey(SysJob sysJob) {
    return TriggerKey.triggerKey(String.valueOf(sysJob.getId()), sysJob.getJobGroup());
  }

  /**
   * 根据给定的Cron表达式返回下一个执行时间
   * @param cronExpression Cron表达式
   * @return Date 下次Cron表达式执行时间
   */
  public static Date getNextExecution(String cronExpression) {
    try {
      CronExpression cron = new CronExpression(cronExpression);
      return cron.getNextValidTimeAfter(new Date(System.currentTimeMillis()));
    } catch (ParseException e) {
      log.error("cron表达式解析失败 - ", e);
      throw new ServiceException("cron表达式解析失败!");
    }
  }
}
