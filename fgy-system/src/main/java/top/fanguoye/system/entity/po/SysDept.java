package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.interfaces.Insert;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 部门信息
 * </p>
 *
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Data
@Accessors(chain = true)
@TableName("sys_dept")
@ApiModel(value = "SysDept对象", description = "部门信息")
public class SysDept extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("父部门id")
    private Long parentId;

    @ApiModelProperty(value = "祖级列表", hidden = true)
    private String ancestors;

    @ApiModelProperty("部门名称")
    @NotBlank(groups = Insert.class)
    private String deptName;

    @ApiModelProperty("显示顺序")
    @OrderBy(sort = 1, asc = true)
    private Integer orderNum;

    @ApiModelProperty("负责人")
    private String leader;

    @ApiModelProperty("联系电话")
    private String phone;

    @ApiModelProperty("邮箱")
    @Email
    private String email;

    @ApiModelProperty(value = "状态 1:正常 2:停用", allowableValues = "1,2")
    private Status status;

    /** 表别名 */
    public static final String TABLE_ALIAS = "d";
}
