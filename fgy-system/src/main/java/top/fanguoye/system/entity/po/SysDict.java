package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.interfaces.Insert;

import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 字典
 * </p>
 *
 * @author Vance
 * @since 2022/01/12 13:37
 */
@Data
@Accessors(chain = true)
@TableName("sys_dict")
@ApiModel(value = "SysDict对象", description = "字典")
public class SysDict extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("字典名称")
    @NotBlank(groups = Insert.class)
    private String dictName;

    @ApiModelProperty("描述")
    @TableField("`desc`")
    private String desc;

    /** 表别名 */
    public static final String TABLE_ALIAS = "dict";

    public static final String DICT_NAME = "dict_name";
    public static final String DESC = "desc";
}
