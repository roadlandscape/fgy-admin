package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.interfaces.Insert;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 字典详情
 * </p>
 *
 * @author Vance
 * @since 2022/01/12 13:37
 */
@Data
@Accessors(chain = true)
@TableName("sys_dict_detail")
@ApiModel(value = "SysDictDetail对象", description = "字典详情")
public class SysDictDetail extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("字典id")
    @NotNull(groups = Insert.class)
    private Long dictId;

    @ApiModelProperty("字典标签")
    @NotBlank(groups = Insert.class)
    private String label;

    @ApiModelProperty("字典值")
    @NotBlank(groups = Insert.class)
    private String value;

    @ApiModelProperty("描述")
    @TableField("`desc`")
    private String desc;

    @ApiModelProperty("显示顺序")
    @OrderBy(sort = 1, asc = true)
    private Integer orderNum;

    /** 表别名 */
    public static final String TABLE_ALIAS = "dict_detail";

    public static final String DICT_ID = "dict_id";
    public static final String LABEL = "label";
    public static final String VALUE = "value";
    public static final String DESC = "desc";
    public static final String ORDER_NUM = "order_num";
}
