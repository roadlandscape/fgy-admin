package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.MenuType;
import top.fanguoye.core.enums.Status;

/**
 * <p>
 * 菜单权限表
 * </p>
 *
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Data
@Accessors(chain = true)
@TableName("sys_menu")
@ApiModel(value = "SysMenu对象", description = "菜单权限表")
public class SysMenu extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("菜单名称")
    private String menuName;

    @ApiModelProperty("父菜单ID")
    private Long parentId;

    @ApiModelProperty("显示顺序")
    @OrderBy(sort = 1, asc = true)
    private Integer orderNum;

    @ApiModelProperty("路由地址")
    private String path;

    @ApiModelProperty("组件路径")
    private String component;

    @ApiModelProperty("路由参数")
    private String query;

    @ApiModelProperty("是否为外链")
    private Boolean isFrame;

    @ApiModelProperty("是否缓存")
    private Boolean isCache;

    @ApiModelProperty(value = "菜单类型（1目录 2菜单 3按钮）", allowableValues = "1,2,3")
    private MenuType menuType;

    @ApiModelProperty("菜单是否显示")
    private Boolean visible;

    @ApiModelProperty(value = "菜单状态（1正常 2停用）", allowableValues = "1,2")
    private Status status;

    @ApiModelProperty("权限标识")
    private String perms;

    @ApiModelProperty("菜单图标")
    private String icon;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("子菜单")
    @TableField(exist = false)
    private List<SysMenu> childrenMenus;

    /** 表别名 */
    public static final String TABLE_ALIAS = "m";
}
