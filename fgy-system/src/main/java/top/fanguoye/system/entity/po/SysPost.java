package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.Status;

/**
 * <p>
 * 岗位信息
 * </p>
 *
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Data
@Accessors(chain = true)
@TableName("sys_post")
@ApiModel(value = "SysPost对象", description = "岗位信息")
public class SysPost extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("岗位编码")
    private String postCode;

    @ApiModelProperty("岗位名称")
    private String postName;

    @ApiModelProperty("显示顺序")
    @OrderBy(sort = 1, asc = true)
    private Integer orderNum;

    @ApiModelProperty(value = "状态 1:正常 2:停用", allowableValues = "1,2")
    private Status status;

    @ApiModelProperty("备注")
    private String remark;

}
