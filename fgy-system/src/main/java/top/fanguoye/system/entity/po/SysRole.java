package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.OrderBy;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import top.fanguoye.core.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.enums.Status;

/**
 * <p>
 * 角色信息
 * </p>
 *
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Data
@Accessors(chain = true)
@TableName("sys_role")
@ApiModel(value = "SysRole对象", description = "角色信息")
public class SysRole extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("角色名称")
    private String roleName;

    @ApiModelProperty("角色权限字符串")
    private String roleKey;

    @ApiModelProperty("显示顺序")
    @OrderBy(sort = 1, asc = true)
    private Integer orderNum;

    @ApiModelProperty(value = "状态 1:正常 2:停用", allowableValues = "1,2")
    private Status status;

    @ApiModelProperty("备注")
    private String remark;

}
