package top.fanguoye.system.entity.po;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.core.annotation.Mobile;
import top.fanguoye.core.entity.BaseEntity;
import top.fanguoye.core.enums.Sex;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.enums.UserType;
import top.fanguoye.core.interfaces.Insert;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：系统用户信息
 * @Date：2021/12/14 11:42
 */
@Data
@Accessors(chain = true)
@ApiModel(value = "系统用户信息")
@TableName("sys_user")
public class SysUser extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("部门ID")
  private Long deptId;

  @ApiModelProperty("部门名称")
  @TableField(exist = false)
  private String deptName;

  @ApiModelProperty(value = "用户账号")
  @NotBlank(groups = Insert.class)
  private String userAccount;

  @ApiModelProperty("用户昵称")
  private String nickName;

  @ApiModelProperty("密码")
  private String password;

  @ApiModelProperty(value = "1:管理员 2:普通用户", allowableValues = "1,2")
  private UserType userType;

  @ApiModelProperty("邮箱")
  @Email
  private String email;

  @ApiModelProperty("联系电话")
  @Mobile
  private String phone;

  @ApiModelProperty(value = "1:男 2:女 3:未知", allowableValues = "1,2,3")
  private Sex sex;

  @ApiModelProperty("头像地址")
  private String avatar;

  @ApiModelProperty(value = "账户状态 1:正常 2:停用", allowableValues = "1,2")
  private Status status;

  @ApiModelProperty(value = "逻辑删除", hidden = true)
  private Boolean deleted;

  @ApiModelProperty("备注")
  private String remark;

  @ApiModelProperty("最后登录IP")
  private String loginIp;

  @ApiModelProperty("最后登录时间")
  private LocalDateTime loginDate;

  /** 表别名 */
  public static final String TABLE_ALIAS = "u";

  public static final String DELETED = "deleted";
  public static final String USER_ACCOUNT = "user_account";
}
