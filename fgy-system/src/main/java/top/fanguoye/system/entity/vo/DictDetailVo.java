package top.fanguoye.system.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import top.fanguoye.system.entity.po.SysDictDetail;

import java.io.Serializable;
import java.util.List;

/**
 * @Author：Vance
 * @Description：字典详情vo
 * @Date：2022/1/12 14:28
 */
@Data
@Accessors(chain = true)
@ApiModel("字典详情vo")
public class DictDetailVo implements Serializable {

  private static final long serialVersionUID = 1L;

  @ApiModelProperty("字典id")
  private Long dictId;

  @ApiModelProperty("字典名称")
  private String dictName;

  @ApiModelProperty("描述")
  private String desc;

  @ApiModelProperty("字典详情")
  private List<SysDictDetail> detailList;
}
