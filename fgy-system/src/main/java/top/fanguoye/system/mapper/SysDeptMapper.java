package top.fanguoye.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.fanguoye.system.entity.po.SysDept;

/**
 * <p>
 * 部门信息 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
