package top.fanguoye.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.fanguoye.system.entity.vo.DictDetailVo;
import top.fanguoye.system.entity.po.SysDictDetail;

import java.util.List;

/**
 * <p>
 * 字典详情 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
public interface SysDictDetailMapper extends BaseMapper<SysDictDetail> {

  /**
   * 根据字典名称查询字典所有详情
   * @param dictName
   * @return
   */
  List<DictDetailVo> selectByDictName(String dictName);
}
