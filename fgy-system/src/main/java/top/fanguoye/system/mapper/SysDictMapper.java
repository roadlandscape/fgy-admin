package top.fanguoye.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.fanguoye.system.entity.po.SysDict;

/**
 * <p>
 * 字典 Mapper 接口
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
