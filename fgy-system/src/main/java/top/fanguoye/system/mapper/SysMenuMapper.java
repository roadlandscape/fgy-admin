package top.fanguoye.system.mapper;

import top.fanguoye.system.entity.po.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 菜单权限表 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

  /**
   * 根据用户ID查询权限
   * @param userId
   * @return
   */
  List<String> selectMenuPermsByUserId(Long userId);

  /**
   * 根据用户ID查询菜单树信息
   * @param userId
   * @return
   */
  List<SysMenu> selectMenuTreeByUserId(Long userId);
}
