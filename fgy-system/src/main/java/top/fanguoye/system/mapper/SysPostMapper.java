package top.fanguoye.system.mapper;

import top.fanguoye.system.entity.po.SysPost;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 岗位信息 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysPostMapper extends BaseMapper<SysPost> {

}
