package top.fanguoye.system.mapper;

import top.fanguoye.system.entity.po.SysRoleDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和部门关联表 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysRoleDeptMapper extends BaseMapper<SysRoleDept> {

}
