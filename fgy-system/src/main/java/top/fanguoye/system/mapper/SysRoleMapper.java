package top.fanguoye.system.mapper;

import top.fanguoye.system.entity.po.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.Set;

/**
 * <p>
 * 角色信息 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

  /**
   * 根据用户ID查询角色
   * @param userId
   * @return
   */
  Set<String> selectRoleKeysByUserId(Long userId);
}
