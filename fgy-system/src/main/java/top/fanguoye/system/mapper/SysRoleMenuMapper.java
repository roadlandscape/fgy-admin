package top.fanguoye.system.mapper;

import top.fanguoye.system.entity.po.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色和菜单关联表 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

}
