package top.fanguoye.system.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import top.fanguoye.system.entity.po.SysUser;

import java.util.List;

/**
 * @Author：Vance
 * @Description：系统用户mapper
 * @Date：2021/12/14 15:49
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

  /**
   * 分页查询
   * @param page
   * @param wrapper
   * @return
   */
  Page<SysUser> getPage(Page<SysUser> page, @Param(Constants.WRAPPER) Wrapper wrapper);

  /**
   * 列表查询
   * @param wrapper
   * @return
   */
  List<SysUser> getList(@Param(Constants.WRAPPER) Wrapper wrapper);

  /**
   * 根据用户账号获取用户
   * @param wrapper
   * @return
   */
  SysUser selectByUserAccount(@Param(Constants.WRAPPER) Wrapper<SysUser> wrapper);
}
