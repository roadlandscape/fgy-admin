package top.fanguoye.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.fanguoye.system.entity.po.SysUserPost;

/**
 * <p>
 * 用户与岗位关联表 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {

}
