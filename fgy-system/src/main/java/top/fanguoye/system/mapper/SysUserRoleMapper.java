package top.fanguoye.system.mapper;

import top.fanguoye.system.entity.po.SysUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户和角色关联表 Mapper 接口
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
