package top.fanguoye.system.service;

import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.po.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 部门信息 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysDeptService extends IService<SysDept> {

    /**
   * 列表查询
   * @param condition
   * @return
   */
    List<SysDept> list(QueryCondition condition);

    /**
     * 分页查询
     * @param pageDomain
     * @return
     */
    PageData<SysDept> page(PageDomain pageDomain);

    /**
     * 通过部门名称和父部门id查询
     * @param deptName
     * @param parentId
     * @return
     */
    Optional<SysDept> selectByNameAndParentId(String deptName, Long parentId);

    /**
     * 校验部门名称是否唯一
     * @param dept
     * @return
     */
    Boolean checkDeptNameUnique(SysDept dept);

    /**
     * 设置祖级列表
     * @param dept
     */
    void setAncestors(SysDept dept);
}
