package top.fanguoye.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.vo.DictDetailVo;
import top.fanguoye.system.entity.po.SysDictDetail;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 字典详情 服务类
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
public interface ISysDictDetailService extends IService<SysDictDetail> {

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysDictDetail> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysDictDetail> page(PageDomain pageDomain);

  /**
   * 根据字典id查询
   * @param dictIds
   * @return
   */
  List<SysDictDetail> selectByDictIds(Set<Long> dictIds);

  /**
   * 根据字典名称查询字典所有详情
   * @param dictName
   * @return
   */
  List<DictDetailVo> selectByDictName(String dictName);
}
