package top.fanguoye.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.po.SysDict;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 字典 服务类
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
public interface ISysDictService extends IService<SysDict> {

  /**
   * 校验字典名称是否唯一
   * @param sysDict
   * @return
   */
  Boolean checkDictNameUnique(SysDict sysDict);

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysDict> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysDict> page(PageDomain pageDomain);

  /**
   * 根据字典名称查询
   * @param dictName
   * @return
   */
  Optional<SysDict> selectByDictName(String dictName);
}
