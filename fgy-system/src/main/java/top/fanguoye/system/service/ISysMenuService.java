package top.fanguoye.system.service;

import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.po.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysMenuService extends IService<SysMenu> {

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysMenu> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysMenu> page(PageDomain pageDomain);

  /**
   * 根据用户ID查询权限
   * @param userId
   * @return
   */
  Set<String> selectMenuPermsByUserId(Long userId);

  /**
   * 根据用户ID查询菜单树信息
   * @param userId
   * @return
   */
  List<SysMenu> selectMenuTreeByUserId(Long userId);

  /**
   * 查询菜单树信息
   * @return
   */
  List<SysMenu> selectMenuTreeAll();
}
