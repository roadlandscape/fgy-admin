package top.fanguoye.system.service;

import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.po.SysPost;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 岗位信息 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysPostService extends IService<SysPost> {

    /**
   * 列表查询
   * @param condition
   * @return
   */
    List<SysPost> list(QueryCondition condition);

    /**
     * 分页查询
     * @param pageDomain
     * @return
     */
    PageData<SysPost> page(PageDomain pageDomain);

}
