package top.fanguoye.system.service;

import top.fanguoye.system.entity.po.SysRoleDept;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色和部门关联表 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysRoleDeptService extends IService<SysRoleDept> {

}
