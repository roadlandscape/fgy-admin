package top.fanguoye.system.service;

import top.fanguoye.system.entity.po.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色和菜单关联表 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
