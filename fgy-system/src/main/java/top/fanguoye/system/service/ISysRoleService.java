package top.fanguoye.system.service;

import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.po.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色信息 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysRoleService extends IService<SysRole> {

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysRole> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysRole> page(PageDomain pageDomain);

  /**
   * 根据用户ID查询角色
   * @param userId
   * @return
   */
  Set<String> selectRoleKeysByUserId(Long userId);
}
