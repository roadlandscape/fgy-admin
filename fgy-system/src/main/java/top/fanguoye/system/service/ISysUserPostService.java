package top.fanguoye.system.service;

import top.fanguoye.system.entity.po.SysUserPost;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与岗位关联表 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysUserPostService extends IService<SysUserPost> {

}
