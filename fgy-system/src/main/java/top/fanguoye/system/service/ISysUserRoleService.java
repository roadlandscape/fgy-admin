package top.fanguoye.system.service;

import top.fanguoye.system.entity.po.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户和角色关联表 服务类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
