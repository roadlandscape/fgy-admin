package top.fanguoye.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.system.entity.po.SysUser;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：系统用户service
 * @Date：2021/12/14 13:37
 */
public interface ISysUserService extends IService<SysUser> {

  /**
   * 列表查询
   * @param condition
   * @return
   */
  List<SysUser> list(QueryCondition condition);

  /**
   * 分页查询
   * @param pageDomain
   * @return
   */
  PageData<SysUser> page(PageDomain pageDomain);

  /**
   * 获取用户权限列表
   * @param userAccount
   * @return
   */
  Set<String> listPermissions(String userAccount);

  /**
   * 获取用户角色列表
   * @param userAccount
   * @return
   */
  Set<String> listRoles(String userAccount);

  /**
   * 根据用户账号获取用户
   * @param userAccount
   * @return
   */
  Optional<SysUser> selectByUserAccount(String userAccount);

  /**
   * 校验用户账号是否唯一
   * @param sysUser
   * @return
   */
  Boolean checkUserAccountUnique(SysUser sysUser);

  /**
   * 记录登录信息
   * @param id
   */
  void recordLoginInfo(Long id);
}
