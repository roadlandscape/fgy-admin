package top.fanguoye.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.system.entity.po.SysDept;
import top.fanguoye.system.mapper.SysDeptMapper;
import top.fanguoye.system.service.ISysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 部门信息 服务实现类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Service("sysDeptService")
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

  @Override
  public List<SysDept> list(QueryCondition condition) {
    if (condition == null) {
      return this.list();
    }
    QueryWrapper wrapper = ServiceUtils.builderQueryWrapper(condition, null);
    return this.list(wrapper);
  }

  @Override
  public PageData<SysDept> page(PageDomain pageDomain) {
    Page<SysDept> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), null);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      QueryWrapper queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, null);
      this.page(page, queryWrapper);
    } else {
      this.page(page);
    }
    // 封装返回结果
    PageData<SysDept> data = new PageData<>();
    data.setCurrent(page.getCurrent())
      .setSize(page.getSize())
      .setTotal(page.getTotal())
      .setPages(page.getPages())
      .setRecords(page.getRecords());
    return data;
  }

  @Override
  public Optional<SysDept> selectByNameAndParentId(String deptName, Long parentId) {
    LambdaQueryWrapper<SysDept> queryWrapper = new LambdaQueryWrapper<>();
    queryWrapper.eq(SysDept::getDeptName, deptName)
        .eq(SysDept::getParentId, parentId == null ? Constants.DEFAULT_PARENT_NODE : parentId);
    return Optional.ofNullable(this.getOne(queryWrapper));
  }

  @Override
  public Boolean checkDeptNameUnique(SysDept dept) {
    if (StrUtil.isEmpty(dept.getDeptName())) {
      return true;
    }

    Optional<SysDept> optionalDept = this.selectByNameAndParentId(dept.getDeptName(), dept.getParentId());
    if (!optionalDept.isPresent()) {
      return true;
    }
    if (dept.getId() != null) {
      SysDept dbDept = optionalDept.get();
      return dbDept.getId().equals(dept.getId()) && dept.getDeptName().equals(dbDept.getDeptName());
    } else {
      return false;
    }
  }

  @Override
  public void setAncestors(SysDept dept) {
    if (dept.getParentId() == null || dept.getParentId() == Constants.DEFAULT_PARENT_NODE) {
      dept.setAncestors(String.valueOf(Constants.DEFAULT_PARENT_NODE));
    } else {
      SysDept info = this.getById(dept.getParentId());
      dept.setAncestors(info.getAncestors() + StringPool.COMMA + dept.getParentId());
    }
  }

}
