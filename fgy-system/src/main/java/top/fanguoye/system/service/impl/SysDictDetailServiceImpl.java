package top.fanguoye.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.system.entity.vo.DictDetailVo;
import top.fanguoye.system.entity.po.SysDictDetail;
import top.fanguoye.system.mapper.SysDictDetailMapper;
import top.fanguoye.system.service.ISysDictDetailService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 字典详情 服务实现类
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
@Service("sysDictDetailService")
public class SysDictDetailServiceImpl extends ServiceImpl<SysDictDetailMapper, SysDictDetail> implements ISysDictDetailService {

  @Override
  public List<SysDictDetail> list(QueryCondition condition) {
    if (condition == null) {
      return this.list();
    }
    QueryWrapper wrapper = ServiceUtils.builderQueryWrapper(condition, null);
    return this.list(wrapper);
  }

  @Override
  public PageData<SysDictDetail> page(PageDomain pageDomain) {
    Page<SysDictDetail> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), null);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      QueryWrapper queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, null);
      this.page(page, queryWrapper);
    } else {
      this.page(page);
    }
    // 封装返回结果
    PageData<SysDictDetail> data = new PageData<>();
    data.setCurrent(page.getCurrent())
      .setSize(page.getSize())
      .setTotal(page.getTotal())
      .setPages(page.getPages())
      .setRecords(page.getRecords());
    return data;
  }

  @Override
  public List<SysDictDetail> selectByDictIds(Set<Long> dictIds) {
    LambdaQueryWrapper<SysDictDetail> wrapper = new LambdaQueryWrapper<>();
    wrapper.in(SysDictDetail::getDictId, dictIds);
    return this.list(wrapper);
  }

  @Override
  public List<DictDetailVo> selectByDictName(String dictName) {
    return this.baseMapper.selectByDictName(dictName);
  }

}
