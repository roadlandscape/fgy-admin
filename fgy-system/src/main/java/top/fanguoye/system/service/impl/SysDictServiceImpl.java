package top.fanguoye.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.system.entity.po.SysDict;
import top.fanguoye.system.mapper.SysDictMapper;
import top.fanguoye.system.service.ISysDictService;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 字典 服务实现类
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
@Service("sysDictService")
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {

  @Override
  public Boolean checkDictNameUnique(SysDict sysDict) {
    if (StrUtil.isEmpty(sysDict.getDictName())) {
      return true;
    }

    Optional<SysDict> sysDictOptional = this.selectByDictName(sysDict.getDictName());
    if (!sysDictOptional.isPresent()) {
      return true;
    }
    if (sysDict.getId() != null) {
      SysDict dbSysDict = sysDictOptional.get();
      return dbSysDict.getId().equals(sysDict.getId()) && sysDict.getDictName().equals(dbSysDict.getDictName());
    } else {
      return false;
    }
  }

  @Override
  public List<SysDict> list(QueryCondition condition) {
    if (condition == null) {
      return this.list();
    }
    QueryWrapper wrapper = ServiceUtils.builderQueryWrapper(condition, null);
    return this.list(wrapper);
  }

  @Override
  public PageData<SysDict> page(PageDomain pageDomain) {
    Page<SysDict> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), null);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      QueryWrapper queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, null);
      this.page(page, queryWrapper);
    } else {
      this.page(page);
    }
    // 封装返回结果
    PageData<SysDict> data = new PageData<>();
    data.setCurrent(page.getCurrent())
      .setSize(page.getSize())
      .setTotal(page.getTotal())
      .setPages(page.getPages())
      .setRecords(page.getRecords());
    return data;
  }

  @Override
  public Optional<SysDict> selectByDictName(String dictName) {
    LambdaQueryWrapper<SysDict> wrapper = new LambdaQueryWrapper<>();
    wrapper.eq(SysDict::getDictName, dictName);
    return Optional.ofNullable(this.getOne(wrapper));
  }
}
