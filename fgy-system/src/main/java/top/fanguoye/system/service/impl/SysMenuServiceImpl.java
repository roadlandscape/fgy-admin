package top.fanguoye.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.enums.MenuType;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.system.entity.po.SysMenu;
import top.fanguoye.system.mapper.SysMenuMapper;
import top.fanguoye.system.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 菜单权限表 服务实现类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

  @Override
  public List<SysMenu> list(QueryCondition condition) {
    if (condition == null) {
      return this.list();
    }
    QueryWrapper wrapper = ServiceUtils.builderQueryWrapper(condition, null);
    return this.list(wrapper);
  }

  @Override
  public PageData<SysMenu> page(PageDomain pageDomain) {
    Page<SysMenu> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), null);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      QueryWrapper queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, null);
      this.page(page, queryWrapper);
    } else {
      this.page(page);
    }
    // 封装返回结果
    PageData<SysMenu> data = new PageData<>();
    data.setCurrent(page.getCurrent())
      .setSize(page.getSize())
      .setTotal(page.getTotal())
      .setPages(page.getPages())
      .setRecords(page.getRecords());
    return data;
  }

  @Override
  public Set<String> selectMenuPermsByUserId(Long userId) {
    List<String> perms = this.baseMapper.selectMenuPermsByUserId(userId);
    Set<String> permsSet = new HashSet<>();
    for (String perm : perms) {
      if (StrUtil.isBlank(perm)) {
        continue;
      }
      permsSet.addAll(Arrays.asList(perm.trim().split(StringPool.COMMA)));
    }
    return permsSet;
  }

  @Override
  public List<SysMenu> selectMenuTreeByUserId(Long userId) {
    return this.baseMapper.selectMenuTreeByUserId(userId);
  }

  @Override
  public List<SysMenu> selectMenuTreeAll() {
    LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
    queryWrapper.in(SysMenu::getMenuType, MenuType.CATALOG, MenuType.MENU).eq(SysMenu::getStatus, Status.NORMAL);
    return this.list(queryWrapper);
  }

}
