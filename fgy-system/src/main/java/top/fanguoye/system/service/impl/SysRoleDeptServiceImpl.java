package top.fanguoye.system.service.impl;

import top.fanguoye.system.entity.po.SysRoleDept;
import top.fanguoye.system.mapper.SysRoleDeptMapper;
import top.fanguoye.system.service.ISysRoleDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和部门关联表 服务实现类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Service("sysRoleDeptService")
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {
}
