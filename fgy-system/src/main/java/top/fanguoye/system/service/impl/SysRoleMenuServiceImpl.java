package top.fanguoye.system.service.impl;

import top.fanguoye.system.entity.po.SysRoleMenu;
import top.fanguoye.system.mapper.SysRoleMenuMapper;
import top.fanguoye.system.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色和菜单关联表 服务实现类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Service("sysRoleMenuService")
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {
}
