package top.fanguoye.system.service.impl;

import top.fanguoye.system.entity.po.SysUserPost;
import top.fanguoye.system.mapper.SysUserPostMapper;
import top.fanguoye.system.service.ISysUserPostService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户与岗位关联表 服务实现类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Service("sysUserPostService")
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPost> implements ISysUserPostService {
}
