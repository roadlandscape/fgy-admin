package top.fanguoye.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.fanguoye.system.entity.po.SysUserRole;
import top.fanguoye.system.mapper.SysUserRoleMapper;
import top.fanguoye.system.service.ISysUserRoleService;

/**
 * <p>
 * 用户和角色关联表 服务实现类
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Service("sysUserRoleService")
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
}
