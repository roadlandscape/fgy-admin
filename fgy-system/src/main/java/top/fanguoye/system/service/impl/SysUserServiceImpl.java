package top.fanguoye.system.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.enums.UserType;
import top.fanguoye.core.utils.ServiceUtils;
import top.fanguoye.core.utils.ServletUtils;
import top.fanguoye.core.utils.StrUtils;
import top.fanguoye.system.entity.po.SysUser;
import top.fanguoye.system.mapper.SysUserMapper;
import top.fanguoye.system.service.ISysMenuService;
import top.fanguoye.system.service.ISysRoleService;
import top.fanguoye.system.service.ISysUserService;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：系统用户service实现
 * @Date：2021/12/14 15:48
 */
@Service("sysUserService")
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

  private final ISysMenuService sysMenuService;
  private final ISysRoleService sysRoleService;

  @Override
  public List<SysUser> list(QueryCondition condition) {
    QueryWrapper wrapper;
    if (condition == null) {
      wrapper = new QueryWrapper();
      wrapper.orderByDesc(BaseEntity.CREATE_TIME);
    } else {
      wrapper = ServiceUtils.builderQueryWrapper(condition, SysUser.TABLE_ALIAS);
    }
    wrapper.eq(StrUtils.joinPoint(SysUser.TABLE_ALIAS, SysUser.DELETED), false);
    return this.baseMapper.getList(wrapper);
  }

  @Override
  public PageData<SysUser> page(PageDomain pageDomain) {
    Page<SysUser> page = Page.of(pageDomain.getCurrent(), pageDomain.getSize(), pageDomain.getSearchCount());
    // 排序字段
    List<OrderItem> orders = ServiceUtils.builderOrderItem(pageDomain.getSortConditions(), SysUser.TABLE_ALIAS);
    page.setOrders(orders);
    //　字段条件过滤
    List<FilterCondition> filterConditions = pageDomain.getFilterConditions();
    QueryWrapper queryWrapper;
    if (CollectionUtil.isNotEmpty(filterConditions)) {
      queryWrapper = ServiceUtils.builderQueryWrapper(filterConditions, SysUser.TABLE_ALIAS);
    } else {
      queryWrapper = new QueryWrapper();
    }
    queryWrapper.eq(StrUtils.joinPoint(SysUser.TABLE_ALIAS, SysUser.DELETED), false);
    this.baseMapper.getPage(page, queryWrapper);
    // 封装返回结果
    PageData<SysUser> data = new PageData<>();
    data.setCurrent(page.getCurrent())
        .setSize(page.getSize())
        .setTotal(page.getTotal())
        .setPages(page.getPages())
        .setRecords(page.getRecords());
    return data;
  }

  @Override
  public Set<String> listPermissions(String userAccount) {
    LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
    wrapper.eq(SysUser::getUserAccount, userAccount).select(SysUser::getId, SysUser::getUserType);
    SysUser user = this.getOne(wrapper);
    if (user == null) {
      return new HashSet<>();
    }

    if (UserType.ADMIN.equals(user.getUserType())) {
      Set<String> permsSet = new HashSet<>();
      permsSet.add(Constants.ADMIN_PERMS_SIGN);
      return permsSet;
    }

    return this.sysMenuService.selectMenuPermsByUserId(user.getId());
  }

  @Override
  public Set<String> listRoles(String userAccount) {
    LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
    wrapper.eq(SysUser::getUserAccount, userAccount).select(SysUser::getId, SysUser::getUserType);
    SysUser user = this.getOne(wrapper);
    if (user == null) {
      return new HashSet<>();
    }

    if (UserType.ADMIN.equals(user.getUserType())) {
      Set<String> rolesSet = new HashSet<>();
      rolesSet.add(Constants.ADMIN_SIGN);
      return rolesSet;
    }

    return this.sysRoleService.selectRoleKeysByUserId(user.getId());
  }

  @Override
  public Optional<SysUser> selectByUserAccount(String userAccount) {
    QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
    wrapper.eq(StrUtils.joinPoint(SysUser.TABLE_ALIAS, SysUser.USER_ACCOUNT), userAccount)
        .eq(StrUtils.joinPoint(SysUser.TABLE_ALIAS, SysUser.DELETED), false).last(Constants.LIMIT1);
    SysUser sysUser = this.baseMapper.selectByUserAccount(wrapper);
    return Optional.ofNullable(sysUser);
  }

  @Override
  public Boolean checkUserAccountUnique(SysUser sysUser) {
    if (StrUtil.isEmpty(sysUser.getUserAccount())) {
      return true;
    }

    Optional<SysUser> sysUserOptional = this.selectByUserAccount(sysUser.getUserAccount());
    if (!sysUserOptional.isPresent()) {
      return true;
    }
    if (sysUser.getId() != null) {
      SysUser dbSysUser = sysUserOptional.get();
      return dbSysUser.getId().equals(sysUser.getId()) && sysUser.getUserAccount().equals(dbSysUser.getUserAccount());
    } else {
      return false;
    }
  }

  @Override
  public void recordLoginInfo(Long id) {
    SysUser sysUser = new SysUser();
    sysUser.setId(id);
    sysUser.setLoginIp(ServletUtil.getClientIP(ServletUtils.getRequest()));
    sysUser.setLoginDate(LocalDateTime.now());
    this.updateById(sysUser);
  }
}
