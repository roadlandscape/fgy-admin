package top.fanguoye;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import org.apache.shiro.spring.boot.autoconfigure.ShiroAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

// @SpringBootApplication(exclude = {SecurityAutoConfiguration.class, MybatisPlusAutoConfiguration.class})
@SpringBootApplication(exclude = {ShiroAutoConfiguration.class, MybatisPlusAutoConfiguration.class})
// @MapperScan("top.fanguoye.*.mapper")
@EnableWebSocket
@EnableScheduling
// 表示通过aop框架暴露该代理对象,AopContext能够访问 https://blog.csdn.net/u013720069/article/details/112025020
@EnableAspectJAutoProxy(exposeProxy = true)
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class FgyAdminApplication {

  public static void main(String[] args) {
    SpringApplication.run(FgyAdminApplication.class, args);
  }

}
