package top.fanguoye.advice;

import cn.hutool.core.io.IoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;
import top.fanguoye.core.annotation.Decrypt;
import top.fanguoye.core.enums.AlgorithmType;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.core.utils.AesEncryptUtils;
import top.fanguoye.core.utils.RSAUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

/**
 * @Author：Vance
 * @Description：接口参数解密(接口参数必须使用@RequestBody修饰才有效)
 * @Date：2022/1/19 16:40
 */
@Slf4j
@ControllerAdvice
public class DecryptRequestBodyAdvice extends RequestBodyAdviceAdapter {

  @Value("${encrypt.AES.secret:}")
  private String aesSecret;

  @Value("${encrypt.RSA.pubkey:}")
  private String rsaPubKey;
  @Value("${encrypt.RSA.prikey:}")
  private String rsaPrikey;

  @Override
  public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
    return methodParameter.hasMethodAnnotation(Decrypt.class) || methodParameter.hasParameterAnnotation(Decrypt.class);
  }

  @Override
  public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter
      , Type targetType, Class<? extends HttpMessageConverter<?>> converterType) throws IOException {
    byte[] body = IoUtil.readBytes(inputMessage.getBody());
    try {
      Decrypt annotation = parameter.getParameterAnnotation(Decrypt.class);
      if (annotation == null) {
        annotation = parameter.getMethodAnnotation(Decrypt.class);
      }
      AlgorithmType type = annotation.value();
      String decrypt;
      if (AlgorithmType.AES.equals(type)) {
        decrypt = AesEncryptUtils.decrypt(body, this.aesSecret);
      } else if (AlgorithmType.RSA.equals(type)) {
        decrypt = RSAUtils.decryptByPublicKey(body, this.rsaPubKey);
      } else {
        return super.beforeBodyRead(inputMessage, parameter, targetType, converterType);
      }
      ByteArrayInputStream inputStream = new ByteArrayInputStream(decrypt.getBytes());
      return new HttpInputMessage() {
        @Override
        public InputStream getBody() throws IOException {
          return inputStream;
        }

        @Override
        public HttpHeaders getHeaders() {
          return inputMessage.getHeaders();
        }
      };
    } catch (Exception e) {
      log.error("参数解密失败 - ", e);
      throw new ServiceException("参数解密失败！");
    }
  }
}
