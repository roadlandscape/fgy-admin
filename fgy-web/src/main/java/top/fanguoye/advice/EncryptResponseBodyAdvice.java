package top.fanguoye.advice;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import top.fanguoye.core.annotation.Encrypt;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.AlgorithmType;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.utils.AesEncryptUtils;
import top.fanguoye.core.utils.RSAUtils;

/**
 * @Author：Vance
 * @Description：接口返回数据加密
 * @Date：2022/1/19 16:02
 */
@ControllerAdvice
@RequiredArgsConstructor
public class EncryptResponseBodyAdvice implements ResponseBodyAdvice<R> {

  private final ObjectMapper objectMapper;

  @Value("${encrypt.AES.secret:}")
  private String aesSecret;

  @Value("${encrypt.RSA.pubkey:}")
  private String rsaPubKey;
  @Value("${encrypt.RSA.prikey:}")
  private String rsaPrikey;

  @Override
  public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
    return returnType.hasMethodAnnotation(Encrypt.class);
  }

  @SneakyThrows
  @Override
  public R beforeBodyWrite(R r, MethodParameter returnType, MediaType selectedContentType
      , Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
    if (HttpStatus.SUCCESS.getCode() != r.getCode() || r.getData() == null) {
      return r;
    }
    String data = objectMapper.writeValueAsString(r.getData());
    Encrypt annotation = returnType.getMethodAnnotation(Encrypt.class);
    AlgorithmType type = annotation.value();
    String encrypt;
    if (AlgorithmType.AES.equals(type)) {
      encrypt = AesEncryptUtils.encrypt(data, this.aesSecret);
    } else if (AlgorithmType.RSA.equals(type)) {
      encrypt = RSAUtils.encryptByPrivateKey(data, this.rsaPrikey);
    } else {
      return r;
    }
    return r.setData(encrypt);
  }
}
