package top.fanguoye.config;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import top.fanguoye.authority.security.utils.SecurityUtils;
import top.fanguoye.authority.shiro.utils.UserUtils;
import top.fanguoye.core.entity.BaseEntity;

import java.time.LocalDateTime;

/**
 * @Author：Vance
 * @Description：字段自动填充
 * @Date：2021/12/14 10:33
 */
@Component
public class FillMetaObjectHandler implements MetaObjectHandler {

  @Override
  public void insertFill(MetaObject metaObject) {
    this.strictInsertFill(metaObject, StrUtil.toCamelCase(BaseEntity.CREATE_TIME), LocalDateTime::now, LocalDateTime.class);
    // this.strictInsertFill(metaObject, StrUtil.toCamelCase(BaseEntity.CREATE_BY), UserUtils::getLoginUserId, Long.class);
    this.strictInsertFill(metaObject, StrUtil.toCamelCase(BaseEntity.CREATE_BY), SecurityUtils::getLoginUserId, Long.class);
    // this.strictInsertFill(metaObject, BaseEntity.CREATOR, UserUtils::getLoginNickName, String.class);
    this.strictInsertFill(metaObject, BaseEntity.CREATOR, SecurityUtils::getLoginNickName, String.class);
  }

  @Override
  public void updateFill(MetaObject metaObject) {
    this.strictUpdateFill(metaObject, StrUtil.toCamelCase(BaseEntity.UPDATE_TIME), LocalDateTime::now, LocalDateTime.class);
    // this.strictUpdateFill(metaObject, StrUtil.toCamelCase(BaseEntity.UPDATE_BY), UserUtils::getLoginUserId, Long.class);
    this.strictUpdateFill(metaObject, StrUtil.toCamelCase(BaseEntity.UPDATE_BY), SecurityUtils::getLoginUserId, Long.class);
    // this.strictUpdateFill(metaObject, BaseEntity.UPDATER, UserUtils::getLoginNickName, String.class);
    this.strictUpdateFill(metaObject, BaseEntity.UPDATER, SecurityUtils::getLoginNickName, String.class);
  }
}
