package top.fanguoye.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.fanguoye.converter.IntegerCodeToEnumConverterFactory;
import top.fanguoye.converter.StringCodeToEnumConverterFactory;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.interceptor.SameUrlDataInterceptor;

/**
 * @Author：Vance
 * @Description：WebMvc配置
 * @Date：2021/12/18 10:15
 */
@Configuration
@RequiredArgsConstructor
public class MyWebMvcConfigurer implements WebMvcConfigurer {

  private final IntegerCodeToEnumConverterFactory integerCodeToEnumConverterFactory;
  private final StringCodeToEnumConverterFactory stringCodeToEnumConverterFactory;
  private final SameUrlDataInterceptor sameUrlDataInterceptor;

  @Value("${file.image.avatar}")
  private String avatarPath;

  /**
   * 跨域支持
   * @param registry
   */
  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**")
        .allowedOriginPatterns("*")
        .allowedMethods("*")
        .allowCredentials(true)
        .maxAge(1800)
        .allowedHeaders("*");
  }

  /**
   * 添加拦截器
   * @param registry
   */
  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(this.sameUrlDataInterceptor).addPathPatterns("/**");
  }

  /**
   * 添加资源访问路径
   * @param registry
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    String avatarUrl = "file:" + this.avatarPath.replace(StringPool.BACK_SLASH, StringPool.SLASH);
    registry.addResourceHandler(top.fanguoye.core.constant.Constants.AVATAR_PATH + StringPool.DOASTERISK)
        .addResourceLocations(avatarUrl.endsWith(StringPool.SLASH) ? avatarUrl : avatarUrl + StringPool.SLASH);

    /** swagger配置 */
    registry.addResourceHandler("/swagger-ui/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/");
  }

  /**
   * 添加转换器工厂
   * @param registry
   */
  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverterFactory(this.integerCodeToEnumConverterFactory);
    registry.addConverterFactory(this.stringCodeToEnumConverterFactory);
  }
}
