package top.fanguoye.config;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import top.fanguoye.core.config.ApplicationConfig;
import top.fanguoye.core.interfaces.BaseEnum;
import top.fanguoye.core.utils.ClassUtils;

import java.time.LocalTime;
import java.time.MonthDay;
import java.time.Year;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：swagger相关配置
 * @Date：2021/12/13 21:18
 */
@Slf4j
@Configuration
@RequiredArgsConstructor
public class SwaggerConfiguration {

  private final ApplicationConfig applicationConfig;

  /** 令牌请求头 */
  @Value("${token.header}")
  private String tokenHeader;
  /** 是否开启swagger */
  @Value("${springfox.documentation.enabled:false}")
  private boolean enabled;
  /** 枚举包 */
  @Value("${mybatis-plus.type-enums-package:}")
  private String typeEnumsPackage;

  @Bean
  public Docket createRestApi() {
    Docket docket = new Docket(DocumentationType.OAS_30)
        .enable(enabled)
        .apiInfo(apiInfo());
    // 配置文档识别数据类型
    directModelSubstitute(docket);
    return docket.select()
        // 扫描所有有注解的api，用这种方式更灵活
        .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
        .paths(PathSelectors.any())
        .build()
        /* 设置安全模式，swagger可以设置访问token */
        .securitySchemes(securitySchemes())
        .securityContexts(securityContexts());
  }

  /**
   * 安全模式，这里指定token通过Authorization请求头传递
   */
  private List<SecurityScheme> securitySchemes() {
    // 设置请求头信息
    List<SecurityScheme> apiKeyList = new ArrayList<>();
    apiKeyList.add(new ApiKey(this.tokenHeader, this.tokenHeader, In.HEADER.toValue()));
    return apiKeyList;
  }

  /**
   * 安全上下文
   */
  private List<SecurityContext> securityContexts() {
    // 设置需要登录认证的路径
    List<SecurityContext> securityContexts = new ArrayList<>();
    securityContexts.add(
        SecurityContext.builder()
            .securityReferences(defaultAuth())
            // ^标识开始  ()里是一子表达式  ?!/auth表示匹配不是/auth的位置，匹配上则添加请求头，注意路径以/开头  .表示任意字符  *表示前面的字符匹配多次 $标识结束
            .operationSelector(o -> o.requestMappingPattern().matches("^(?!/auth).*$")).build());
    return securityContexts;
  }

  /**
   * 默认的安全上引用
   */
  private List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    List<SecurityReference> securityReferences = new ArrayList<>();
    securityReferences.add(new SecurityReference(this.tokenHeader, authorizationScopes));
    return securityReferences;
  }

  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title(this.applicationConfig.getName() + "_接口文档")
        .description(this.applicationConfig.getDescription())
        .contact(new Contact(
            this.applicationConfig.getContact().getName(),
            this.applicationConfig.getContact().getUrl(),
            this.applicationConfig.getContact().getEmail()
        )).version(this.applicationConfig.getVersion()).build();
  }

  /**
   * 配置文档识别数据类型
   */
  private void directModelSubstitute(Docket docket) {
    // 配置文档识别LocalTime为字符串（默认是HH:mm:ss）
    docket.directModelSubstitute(LocalTime.class, String.class)
        // 配置文档识别MonthDay为字符串（默认是MM-dd）
        .directModelSubstitute(MonthDay.class, String.class)
        // 配置文档识别YearMonth为字符串（默认是yyyy-MM）
        .directModelSubstitute(YearMonth.class, String.class)
        // 配置文档识别Year为字符串
        .directModelSubstitute(Year.class, String.class)
        // 配置文档识别Object为String
        .directModelSubstitute(Object.class, String.class);

    try {
      Set<Class<BaseEnum>> classes = ClassUtils.getExtendsBaseEnumClass(this.typeEnumsPackage);
      if (CollectionUtil.isEmpty(classes)) {
        return;
      }
      for (Class<BaseEnum> aClass : classes) {
        docket.directModelSubstitute(aClass, String.class);
      }
    } catch (Exception e) {
      log.warn("swagger配置文档识别数据类型出现异常:", e);
    }
  }
}
