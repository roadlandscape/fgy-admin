package top.fanguoye.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import top.fanguoye.filter.RepeatableFilter;
import top.fanguoye.filter.WebSocketFilter;

import java.util.Collections;

/**
 * @Author：Vance
 * @Description：WebConfig
 * @Date：2022/2/20 22:20
 */
@Configuration
@RequiredArgsConstructor
public class WebConfig {

  /**
   * 注册 WebSocket过滤器
   */
  @Bean
  public FilterRegistrationBean<WebSocketFilter> webSocketFilter() {
    FilterRegistrationBean<WebSocketFilter> filterRegistrationBean = new FilterRegistrationBean<>();
    filterRegistrationBean.setFilter(new WebSocketFilter());
    // urlPatterns不能为/**，只能为/*，否则过滤器无法生效
    filterRegistrationBean.setUrlPatterns(Collections.singletonList("/websocket/*"));
    filterRegistrationBean.setName("webSocketFilter");
    filterRegistrationBean.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
    return filterRegistrationBean;
  }

  /**
   * 注册 可重复读取inputStream的request 过滤器
   * @return
   */
  @Bean
  public FilterRegistrationBean<RepeatableFilter> repeatableFilter() {
    FilterRegistrationBean<RepeatableFilter> filterRegistrationBean = new FilterRegistrationBean<>();
    filterRegistrationBean.setFilter(new RepeatableFilter());
    // urlPatterns不能为/**，只能为/*，否则过滤器无法生效
    filterRegistrationBean.setUrlPatterns(Collections.singletonList("/*"));
    filterRegistrationBean.setName("repeatableFilter");
    filterRegistrationBean.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
    return filterRegistrationBean;
  }
}
