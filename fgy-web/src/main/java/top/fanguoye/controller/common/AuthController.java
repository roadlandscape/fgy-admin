package top.fanguoye.controller.common;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.fanguoye.authority.constant.Constants;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.authority.security.mapstruct.LoginUserCopy;
import top.fanguoye.authority.security.service.TokenService;
import top.fanguoye.authority.security.utils.SecurityUtils;
import top.fanguoye.authority.shiro.utils.UserUtils;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.system.entity.po.SysUser;
import top.fanguoye.system.service.ISysUserService;

import javax.validation.constraints.NotBlank;
import java.util.Map;
import java.util.Optional;

/**
 * @Author：Vance
 * @Description：登录、退出、获取登录信息
 * @Date：2021/12/17 15:24
 */
@Api(tags = "1.登录、退出、获取登录信息")
@Slf4j
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@Validated
public class AuthController {

  private final ISysUserService sysUserService;
  private final AuthenticationManager authenticationManager;
  private final TokenService tokenService;
  private final LoginUserCopy loginUserCopy;

  /** 令牌请求头 */
  @Value("${token.header}")
  private String tokenHeader;

  @ApiOperation("登录-shiro")
  // @PostMapping("/login")
  public R loginShiro(@RequestParam @NotBlank String userAccount, @RequestParam(required = false) String password) {
    Optional<SysUser> sysUserOptional = this.sysUserService.selectByUserAccount(userAccount);
    sysUserOptional.orElseThrow(() -> new ServiceException("用户不存在!"));
    SysUser sysUser = sysUserOptional.get();

    if (StrUtil.isNotBlank(password) || StrUtil.isNotEmpty(sysUser.getPassword())) {
      if (StrUtil.isBlank(password)) {
        return R.error("用户密码为空！");
      }

      Sha256Hash encryptPassword = new Sha256Hash(password, password, Constants.PASSWORD_HASH_ITERATIONS);
      if (!encryptPassword.toString().equals(sysUser.getPassword())) {
        return R.error("密码错误!");
      }
    }

    if (Status.DISABLE.equals(sysUser.getStatus())) {
      return R.error("账号已被禁用,请联系管理员!");
    }

    LoginUser loginUser = loginUserCopy.toT(sysUser);
    loginUser.setLoginUniqueKey(UUID.fastUUID().toString());
    String token = tokenService.createToken(loginUser);
    sysUserService.recordLoginInfo(loginUser.getId());
    Map<String, Object> map = MapUtil.newHashMap(2);
    map.put(this.tokenHeader, token);
    map.put("userInfo", sysUser);
    return R.ok(map);
  }

  @ApiOperation("登录")
  @PostMapping("/login")
  public R login(@RequestParam @NotBlank String userAccount,
                 @RequestParam @NotBlank String password) {
    Authentication authentication;
    try {
      // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
      authentication = authenticationManager
          .authenticate(new UsernamePasswordAuthenticationToken(userAccount, password));
    } catch (Exception e) {
      log.warn("用户尝试登录失败:", e);
      if (e instanceof BadCredentialsException) {
        return R.resp(HttpStatus.AUTH_USER_PASSWORD);
      } else if (e instanceof DisabledException) {
        return R.resp(HttpStatus.AUTH_ACCOUNT_DISABLED);
      } else if (e instanceof LockedException) {
        return R.resp(HttpStatus.AUTH_ACCOUNT_LOCKED);
      } else if (e instanceof AccountExpiredException) {
        return R.resp(HttpStatus.AUTH_ACCOUNT_EXPIRED);
      }
      return R.resp(HttpStatus.AUTH_ERROR);
    }
    LoginUser loginUser = (LoginUser) authentication.getPrincipal();
    loginUser.setLoginUniqueKey(UUID.fastUUID().toString());
    String token = tokenService.createToken(loginUser);
    sysUserService.recordLoginInfo(loginUser.getId());
    Map<String, Object> map = MapUtil.newHashMap(2);
    map.put(this.tokenHeader, token);
    map.put("userInfo", loginUser);
    return R.ok(map);
  }

  @ApiOperation("注册")
  @PostMapping("/register")
  public R register(@RequestParam @NotBlank String userAccount,
                    @RequestParam @NotBlank String password) {
    Boolean unique = sysUserService.checkUserAccountUnique(new SysUser().setUserAccount(userAccount));
    if (!unique) {
      return R.resp(HttpStatus.AUTH_ACCOUNT_EXIST);
    }

    SysUser sysUser = new SysUser();
    sysUser.setUserAccount(userAccount)
        .setNickName(userAccount)
        .setPassword(SecurityUtils.encryptPassword(password));
        // .setPassword(new Sha256Hash(password, password, Constants.PASSWORD_HASH_ITERATIONS).toString());
    boolean save = sysUserService.save(sysUser);
    return save ? R.ok() : R.error("注册失败!");
  }

  @ApiOperation("获取当前登录的用户信息")
  @GetMapping("/info")
  public R<LoginUser> info() {
    // LoginUser loginUser = UserUtils.getLoginUser();
    LoginUser loginUser = SecurityUtils.getLoginUser();
    if (loginUser == null) {
      return R.error("用户未登录!");
    }
    return R.ok(loginUser);
  }

  @ApiOperation("退出登录")
  @PostMapping("/logout")
  public R logout() {
    return R.ok();
  }
}
