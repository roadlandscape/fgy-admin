package top.fanguoye.controller.common;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.core.utils.FileUtils;

/**
 * @Author：Vance
 * @Description：文件相关controller
 * @Date：2021/12/22 23:22
 */
@Api(tags = "2.文件上传")
@RestController
@RequestMapping("/file-upload")
public class FileController {

  @Value("${file.image.avatar}")
  private String avatarPath;

  @ApiOperation("上传图片，type可取值：avatar(头像)、")
  @PostMapping("/image/{type}")
  public R imageUpload(@PathVariable String type, @RequestPart("file") MultipartFile file) {
    String imagePath;
    String pathPre;
    switch (type) {
      case "avatar":
        imagePath = this.avatarPath;
        pathPre = Constants.AVATAR_PATH;
        break;
      default:
        throw new ServiceException("路径参数有误！");
    }
    if (!FileUtils.isImage(file)) {
      return R.error("文件格式有误！");
    }
    String path = FileUtils.upload(file, imagePath);
    return StrUtil.isNotEmpty(path) ? R.ok().setData(pathPre + path) : R.error("上传文件失败!");
  }
}
