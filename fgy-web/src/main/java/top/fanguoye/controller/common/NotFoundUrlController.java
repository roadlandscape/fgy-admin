package top.fanguoye.controller.common;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;

/**
 * @Author：Vance
 * @Description：处理404
 * @Date：2021/12/20 15:51
 */
@RestController
public class NotFoundUrlController {

  @RequestMapping
  public R error() {
    return R.resp(HttpStatus.ABSENT);
  }
}
