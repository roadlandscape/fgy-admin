package top.fanguoye.controller.im;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.authority.security.utils.SecurityUtils;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.interfaces.Insert;
import top.fanguoye.core.interfaces.Update;
import top.fanguoye.im.entity.po.ImGroupUser;
import top.fanguoye.im.service.IImGroupService;
import top.fanguoye.im.entity.po.ImGroup;
import top.fanguoye.im.service.IImGroupUserService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * <p>
 * im聊天群 前端控制器
 * </p>
 * @author Vance
 * @since 2022/03/20 23:11
 */
@Api(tags = "im聊天群模块")
@RestController
@RequestMapping("/im/group")
@RequiredArgsConstructor
@Validated
public class ImGroupController {

  private final IImGroupService imGroupService;
  private final IImGroupUserService imGroupUserService;

  @ApiOperation("新增")
  @PostMapping("/insert")
  @Transactional(rollbackFor = Exception.class)
  public R insert(@Validated(Insert.class) ImGroup entity) {
    Long loginUserId = SecurityUtils.getLoginUserId();
    entity.setMasterId(loginUserId)
        .setDeleted(null)
        .setId(null)
        .clearForbidUpdateField();
    boolean save = imGroupService.save(entity);
    if (!save) {
      return R.resp(HttpStatus.DATA_INSERT_FAILED);
    }

    ImGroupUser imGroupUser = new ImGroupUser();
    imGroupUser.setGroupId(entity.getId())
        .setUserId(loginUserId)
        .setCreateTime(LocalDateTime.now());
    imGroupUserService.save(imGroupUser);
    return R.ok();
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(@Validated(Update.class) ImGroup entity) {
    entity.setDeleted(null).clearForbidUpdateField();
    boolean update = imGroupService.updateById(entity);
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    boolean delete = imGroupService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<ImGroup> detail(@PathVariable Long id) {
    ImGroup detail = imGroupService.getById(id);
    return detail == null ? R.resp(HttpStatus.DATA_NOT_FOUND) : R.ok(detail);
  }

  @ApiOperation("添加群成员")
  @PostMapping("/add-users")
  public R addUsers(@NotNull Long groupId, @NotEmpty Set<@NotNull Long> userIds) {
    imGroupService.addUsers(groupId, userIds);
    return R.ok();
  }

  @ApiOperation("移除群成员")
  @PostMapping("/remove-users")
  public R removeUsers(@NotNull Long groupId, @NotEmpty Set<@NotNull Long> userIds) {
    imGroupService.removeUsers(groupId, userIds);
    return R.ok();
  }
}
