package top.fanguoye.controller.im;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.authority.security.utils.SecurityUtils;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.ChatType;
import top.fanguoye.im.entity.dto.ImMessagePageDto;
import top.fanguoye.im.entity.po.ImGroupUser;
import top.fanguoye.im.entity.po.ImMessage;
import top.fanguoye.im.entity.po.ImMsgRead;
import top.fanguoye.im.entity.vo.ImMessageVo;
import top.fanguoye.im.service.IImGroupUserService;
import top.fanguoye.im.service.IImMessageService;
import top.fanguoye.im.service.IImMsgReadService;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * im聊天消息 前端控制器
 * </p>
 * @author Vance
 * @since 2022/03/20 17:17
 */
@Api(tags = "im聊天消息模块")
@RestController
@RequestMapping("im/message")
@RequiredArgsConstructor
@Validated
public class ImMessageController {

  private final IImMessageService imMessageService;
  private final IImMsgReadService imMsgReadService;
  private final IImGroupUserService imGroupUserService;

  @ApiOperation("分页查询聊天记录")
  @PostMapping("/page")
  public R<PageData<ImMessageVo>> page(@Validated ImMessagePageDto pageDto) {
    PageData<ImMessageVo> data = imMessageService.page(pageDto, SecurityUtils.getLoginUserId());
    return R.ok(data);
  }

  @ApiOperation("发送信息")
  @ApiImplicitParams({
      @ApiImplicitParam(name = "toId", value = "接收者id，如果是群聊则是群id", dataTypeClass = Long.class, required = true),
      @ApiImplicitParam(name = "msg", value = "发送内容", dataTypeClass = String.class, required = true),
      @ApiImplicitParam(name = "type", value = "聊天类型", dataTypeClass = String.class, allowableValues = "1,2", required = false)
  })
  @PostMapping("/send")
  @Transactional(rollbackFor = Exception.class)
  public R send(
      @RequestParam Long toId,
      @RequestParam @NotBlank String msg,
      @RequestParam(required = false) ChatType type
  ) {
    if (type == null) {
      type = ChatType.SINGLE;
    }
    Long fromId = SecurityUtils.getLoginUserId();
    ImMessage imMessage = new ImMessage();
    imMessage.setChatId(toId)
        .setFromId(fromId)
        .setContent(msg)
        .setType(type);
    imMessageService.save(imMessage);

    if (type.equals(ChatType.SINGLE)) {
      ImMsgRead imMsgRead = new ImMsgRead();
      imMsgRead.setImId(imMessage.getId())
          .setChatId(toId)
          .setFromId(fromId)
          .setReadId(toId)
          .setRead(false);
      imMsgReadService.save(imMsgRead);
    } else {
      List<ImMsgRead> list = new ArrayList<>();
      List<ImGroupUser> groupUsers = imGroupUserService.selectByGroupId(toId);
      for (ImGroupUser groupUser : groupUsers) {
        ImMsgRead imMsgRead = new ImMsgRead();
        imMsgRead.setImId(imMessage.getId())
            .setChatId(toId)
            .setFromId(fromId)
            .setReadId(groupUser.getUserId())
            .setRead(groupUser.getUserId().equals(fromId));
        list.add(imMsgRead);
      }
      imMsgReadService.saveBatch(list);
    }

    // 通知收信息的用户 todo
    return R.ok();
  }
}
