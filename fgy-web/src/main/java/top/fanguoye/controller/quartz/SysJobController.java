package top.fanguoye.controller.quartz;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.enums.Status;
import top.fanguoye.core.interfaces.Insert;
import top.fanguoye.core.interfaces.Update;
import top.fanguoye.quartz.service.ISysJobService;
import top.fanguoye.quartz.entity.po.SysJob;
import top.fanguoye.quartz.utils.ScheduleUtils;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 定时任务调度 前端控制器
 * </p>
 * @author Vance
 * @since 2022/01/14 15:24
 */
@Api(tags = "定时任务调度模块")
@RestController
@RequestMapping("/quartz/job")
@RequiredArgsConstructor
@Validated
public class SysJobController {

  private final ISysJobService sysJobService;
  private final Scheduler scheduler;

  @ApiOperation("新增")
  @PostMapping("/insert")
  @Transactional(rollbackFor = Exception.class)
  public R insert(@Validated(Insert.class) SysJob entity) throws SchedulerException {
    String msg = checkInvokeMethod(entity.getInvokeMethod());
    if (msg != null) {
      return R.error(msg);
    }
    entity.setId(null).clearForbidUpdateField();
    boolean save = sysJobService.save(entity);
    if (save) {
      ScheduleUtils.createScheduleJob(entity);
    }
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  @Transactional(rollbackFor = Exception.class)
  public R update(@Validated(Update.class) SysJob entity) throws SchedulerException {
    String msg = checkInvokeMethod(entity.getInvokeMethod());
    if (msg != null) {
      return R.error(msg);
    }
    entity.clearForbidUpdateField();
    SysJob dbSysJob = sysJobService.getById(entity.getId());
    boolean update = sysJobService.updateById(entity);
    if (update) {
      ScheduleUtils.updateScheduleJob(dbSysJob, sysJobService.getById(entity.getId()));
    }
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("修改状态")
  @PutMapping("/update-status")
  @Transactional(rollbackFor = Exception.class)
  public R updateStatus(@RequestParam Long id, @RequestParam Status status) throws SchedulerException {
    SysJob sysJob = sysJobService.getById(id);
    Status dbStatus = sysJob.getStatus();
    if (status.equals(dbStatus)) {
      return R.ok();
    }
    sysJob.setStatus(status);
    boolean update = sysJobService.updateById(sysJob);
    if (update) {
      if (Status.NORMAL.equals(status)) {
        scheduler.resumeJob(ScheduleUtils.getJobKey(sysJob));
      } else {
        scheduler.pauseJob(ScheduleUtils.getJobKey(sysJob));
      }
    }
    return update ? R.ok() : R.error("修改状态失败！");
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  @Transactional(rollbackFor = Exception.class)
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) throws SchedulerException {
    List<SysJob> sysJobs = sysJobService.listByIds(ids);
    boolean delete = sysJobService.removeByIds(ids);
    for (SysJob sysJob : sysJobs) {
      scheduler.deleteJob(ScheduleUtils.getJobKey(sysJob));
    }
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<SysJob> detail(@PathVariable Long id) {
    SysJob detail = sysJobService.getById(id);
    return detail == null ? R.resp(HttpStatus.DATA_NOT_FOUND) : R.ok(detail);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysJob>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysJob> data = sysJobService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysJob>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysJob> list = sysJobService.list(condition);
    return R.ok(list);
  }

  @ApiOperation("执行一次定时任务")
  @PutMapping("/run/{id}")
  public R run(@PathVariable Long id) throws SchedulerException {
    sysJobService.run(id);
    return R.ok();
  }

  private String checkInvokeMethod(String invokeMethod) {
    if (StrUtil.isBlank(invokeMethod)) {
      return null;
    }
    invokeMethod = StrUtil.subBefore(invokeMethod, StringPool.LEFT_BRACKET, false);
    if (!invokeMethod.contains(StringPool.DOT)) {
      return "调用方法格式有误！";
    }
    invokeMethod = StrUtil.subBefore(invokeMethod, StringPool.DOT, true);
    if (StrUtil.contains(invokeMethod, StringPool.DOT)) {
      return StrUtil.startWithAny(invokeMethod, Constants.JOB_WHITELIST_STR) ? null : "Class类不在白名单之内。";
    }
    try {
      SpringUtil.getBean(invokeMethod);
      return null;
    } catch (NoSuchBeanDefinitionException e) {
      return "Bean不存在容器中。";
    }
  }
}
