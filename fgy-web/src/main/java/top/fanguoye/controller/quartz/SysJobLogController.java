package top.fanguoye.controller.quartz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.quartz.service.ISysJobLogService;
import top.fanguoye.quartz.entity.po.SysJobLog;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 定时任务调度日志 前端控制器
 * </p>
 * @author Vance
 * @since 2022/01/17 16:59
 */
@Api(tags = "定时任务调度日志模块")
@RestController
@RequestMapping("/quartz/job-log")
@RequiredArgsConstructor
@Validated
public class SysJobLogController {

  private final ISysJobLogService sysJobLogService;

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    boolean delete = sysJobLogService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<SysJobLog> detail(@PathVariable Long id) {
    SysJobLog detail = sysJobLogService.getById(id);
    return detail == null ? R.resp(HttpStatus.DATA_NOT_FOUND) : R.ok(detail);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysJobLog>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysJobLog> data = sysJobLogService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysJobLog>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysJobLog> list = sysJobLogService.list(condition);
    return R.ok(list);
  }

  @ApiOperation("清空定时任务调度日志")
  @DeleteMapping("/clean")
  public R clean() {
    sysJobLogService.cleanJobLog();
    return R.ok();
  }
}
