package top.fanguoye.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.interfaces.Insert;
import top.fanguoye.core.interfaces.Update;
import top.fanguoye.system.service.ISysDeptService;
import top.fanguoye.system.entity.po.SysDept;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 部门信息 前端控制器
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Api(tags = "SysDept模块")
@RestController
@RequestMapping("/system/dept")
@RequiredArgsConstructor
@Validated
public class SysDeptController {

  private final ISysDeptService sysDeptService;

  @ApiOperation("新增")
  @PostMapping("/insert")
  public R insert(@Validated(Insert.class) SysDept entity) {
    Boolean isExist = sysDeptService.checkDeptNameUnique(entity);
    if (!isExist) {
      return R.error("同级别下存在相同名称的部门！");
    }

    sysDeptService.setAncestors(entity);
    entity.setId(null).clearForbidUpdateField();
    boolean save = sysDeptService.save(entity);
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(@Validated(Update.class) SysDept entity) {
    if (entity.getId().equals(entity.getParentId())) {
      return R.error("上级部门不能是自己！");
    }

    Boolean isExist = sysDeptService.checkDeptNameUnique(entity);
    if (!isExist) {
      return R.error("同级别下存在相同名称的部门！");
    }

    sysDeptService.setAncestors(entity);
    entity.clearForbidUpdateField();
    boolean update = sysDeptService.updateById(entity);
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    boolean delete = sysDeptService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<SysDept> detail(@PathVariable Long id) {
    SysDept detail = sysDeptService.getById(id);
    return detail == null ? R.resp(HttpStatus.DATA_NOT_FOUND) : R.ok(detail);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysDept>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysDept> data = sysDeptService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysDept>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysDept> list = sysDeptService.list(condition);
    return R.ok(list);
  }

}
