package top.fanguoye.controller.system;

import cn.hutool.core.collection.CollectionUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.interfaces.Insert;
import top.fanguoye.core.interfaces.Update;
import top.fanguoye.system.entity.po.SysDictDetail;
import top.fanguoye.system.service.ISysDictDetailService;
import top.fanguoye.system.service.ISysDictService;
import top.fanguoye.system.entity.po.SysDict;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 字典 前端控制器
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
@Api(tags = "字典模块")
@RestController
@RequestMapping("/system/dict")
@RequiredArgsConstructor
@Validated
public class SysDictController {

  private final ISysDictService sysDictService;
  private final ISysDictDetailService sysDictDetailService;

  @ApiOperation("新增")
  @PostMapping("/insert")
  public R insert(@Validated(Insert.class) SysDict entity) {
    Boolean unique = sysDictService.checkDictNameUnique(entity);
    if (!unique) {
      return R.error("字典名称重复！");
    }
    entity.setId(null).clearForbidUpdateField();
    boolean save = sysDictService.save(entity);
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(@Validated(Update.class) SysDict entity) {
    Boolean unique = sysDictService.checkDictNameUnique(entity);
    if (!unique) {
      return R.error("字典名称重复！");
    }
    entity.clearForbidUpdateField();
    boolean update = sysDictService.updateById(entity);
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    List<SysDictDetail> list = sysDictDetailService.selectByDictIds(ids);
    if (CollectionUtil.isNotEmpty(list)) {
      return R.error("所要删除的字典存在字典详情，请先删除字典详情。");
    }
    boolean delete = sysDictService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<SysDict> detail(@PathVariable Long id) {
    SysDict detail = sysDictService.getById(id);
    return detail == null ? R.resp(HttpStatus.DATA_NOT_FOUND) : R.ok(detail);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysDict>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysDict> data = sysDictService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysDict>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysDict> list = sysDictService.list(condition);
    return R.ok(list);
  }

}
