package top.fanguoye.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.interfaces.Insert;
import top.fanguoye.core.interfaces.Update;
import top.fanguoye.system.entity.vo.DictDetailVo;
import top.fanguoye.system.service.ISysDictDetailService;
import top.fanguoye.system.entity.po.SysDictDetail;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 字典详情 前端控制器
 * </p>
 * @author Vance
 * @since 2022/01/12 13:37
 */
@Api(tags = "字典详情模块")
@RestController
@RequestMapping("/system/dict-detail")
@RequiredArgsConstructor
@Validated
public class SysDictDetailController {

  private final ISysDictDetailService sysDictDetailService;

  @ApiOperation("新增")
  @PostMapping("/insert")
  public R insert(@Validated(Insert.class) SysDictDetail entity) {
    entity.setId(null).clearForbidUpdateField();
    boolean save = sysDictDetailService.save(entity);
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(@Validated(Update.class) SysDictDetail entity) {
    entity.clearForbidUpdateField();
    boolean update = sysDictDetailService.updateById(entity);
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    boolean delete = sysDictDetailService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<SysDictDetail> detail(@PathVariable Long id) {
    SysDictDetail detail = sysDictDetailService.getById(id);
    return detail == null ? R.resp(HttpStatus.DATA_NOT_FOUND) : R.ok(detail);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysDictDetail>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysDictDetail> data = sysDictDetailService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysDictDetail>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysDictDetail> list = sysDictDetailService.list(condition);
    return R.ok(list);
  }

  @ApiOperation("根据字典名称查询字典所有详情")
  @GetMapping("/dict-detail/{dictName}")
  public R<List<DictDetailVo>> dictDetail(@PathVariable String dictName) {
    List<DictDetailVo> list = sysDictDetailService.selectByDictName(dictName);
    return R.ok(list);
  }
}
