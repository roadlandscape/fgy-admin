package top.fanguoye.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.authority.security.utils.SecurityUtils;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.enums.UserType;
import top.fanguoye.system.service.ISysMenuService;
import top.fanguoye.system.entity.po.SysMenu;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单权限表 前端控制器
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Api(tags = "SysMenu模块")
@RestController
@RequestMapping("/system/menu")
@RequiredArgsConstructor
@Validated
public class SysMenuController {

  private final ISysMenuService sysMenuService;

  @ApiOperation("新增")
  @PostMapping("/insert")
  public R insert(SysMenu entity) {
    entity.setId(null).clearForbidUpdateField();
    boolean save = sysMenuService.save(entity);
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(SysMenu entity) {
    entity.clearForbidUpdateField();
    boolean update = sysMenuService.updateById(entity);
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    boolean delete = sysMenuService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysMenu>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysMenu> data = sysMenuService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysMenu>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysMenu> list = sysMenuService.list(condition);
    return R.ok(list);
  }

  @ApiOperation("获取登录用户的路由信息")
  @GetMapping("/user-routers")
  public R<List<SysMenu>> userRouters() {
    LoginUser loginUser = SecurityUtils.getLoginUser();
    List<SysMenu> menus;
    if (loginUser == null || loginUser.getUserType().equals(UserType.ADMIN)) {
      menus = sysMenuService.selectMenuTreeAll();
    } else {
      menus = sysMenuService.selectMenuTreeByUserId(SecurityUtils.getLoginUserId());
    }

    return R.ok(buildHierarchyMenuTree(menus));
  }

  /**
   * 构建层级菜单
   *
   * @param menus
   * @return
   */
  private List<SysMenu> buildHierarchyMenuTree(List<SysMenu> menus) {
    List<SysMenu> returnList = new ArrayList<>();
    Iterator<SysMenu> menuIterator = menus.iterator();
    while (menuIterator.hasNext()) {
      SysMenu next = menuIterator.next();
      if (next.getParentId() == Constants.DEFAULT_PARENT_NODE) {
        recursionChildMenu(menus, next);
        returnList.add(next);
        menuIterator.remove();
      }
    }
    return returnList;
  }

  /**
   * 递归子菜单
   *
   * @param menus
   * @param menu
   */
  private void recursionChildMenu(List<SysMenu> menus, SysMenu menu) {
    List<SysMenu> childList = menus.stream()
        .filter(next -> next.getParentId().equals(menu.getId())).collect(Collectors.toList());
    if (childList.size() > 0) {
      menu.setChildrenMenus(childList);
      for (SysMenu m : childList) {
        recursionChildMenu(menus, m);
      }
    }
  }
}
