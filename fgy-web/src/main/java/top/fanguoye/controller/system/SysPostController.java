package top.fanguoye.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import top.fanguoye.core.entity.PageData;
import top.fanguoye.core.entity.PageDomain;
import top.fanguoye.core.entity.QueryCondition;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.system.service.ISysPostService;
import top.fanguoye.system.entity.po.SysPost;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * <p>
 * 岗位信息 前端控制器
 * </p>
 * @author Vance
 * @since 2021/12/19 14:57
 */
@Api(tags = "SysPost模块")
@RestController
@RequestMapping("/system/post")
@RequiredArgsConstructor
@Validated
public class SysPostController {

  private final ISysPostService sysPostService;

  @ApiOperation("新增")
  @PostMapping("/insert")
  public R insert(SysPost entity) {
    entity.setId(null).clearForbidUpdateField();
    boolean save = sysPostService.save(entity);
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(SysPost entity) {
    entity.clearForbidUpdateField();
    boolean update = sysPostService.updateById(entity);
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    boolean delete = sysPostService.removeByIds(ids);
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysPost>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysPost> data = sysPostService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysPost>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysPost> list = sysPostService.list(condition);
    return R.ok(list);
  }

}
