package top.fanguoye.controller.system;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.fanguoye.authority.security.utils.SecurityUtils;
import top.fanguoye.core.constant.RedisConstants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.interfaces.Insert;
import top.fanguoye.core.interfaces.Update;
import top.fanguoye.core.entity.*;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.redis.RedisCache;
import top.fanguoye.system.entity.po.SysDept;
import top.fanguoye.system.entity.po.SysUser;
import top.fanguoye.system.service.ISysDeptService;
import top.fanguoye.system.service.ISysUserService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：用户模块
 * @Date：2021/12/14 09:33
 */
@Api(tags = "3.用户模块")
@RestController
@RequestMapping("/system/user")
@RequiredArgsConstructor
@Validated
public class SysUserController {

  private final ISysUserService sysUserService;
  private final ISysDeptService sysDeptService;
  private final RedisCache redisCache;

  @ApiOperation("新增")
  @PostMapping("/insert")
  public R insert(@Validated(Insert.class) SysUser entity) {
    Boolean unique = sysUserService.checkUserAccountUnique(entity);
    if (!unique) {
      return R.resp(HttpStatus.AUTH_ACCOUNT_EXIST);
    }

    entity.setDeleted(null).setLoginIp(null).setLoginDate(null).setId(null).clearForbidUpdateField();
    String password = entity.getPassword();
    if (StrUtil.isNotBlank(password)) {
      /*Sha256Hash encryptPassword = new Sha256Hash(password, password, Constants.PASSWORD_HASH_ITERATIONS);
      entity.setPassword(encryptPassword.toString());*/
      entity.setPassword(SecurityUtils.encryptPassword(password));
    }
    boolean save = sysUserService.save(entity);
    return save ? R.ok() : R.resp(HttpStatus.DATA_INSERT_FAILED);
  }

  @ApiOperation("修改")
  @PutMapping("/update")
  public R update(@Validated(Update.class) SysUser entity) {
    Boolean unique = sysUserService.checkUserAccountUnique(entity);
    if (!unique) {
      return R.resp(HttpStatus.AUTH_ACCOUNT_EXIST);
    }

    entity.setDeleted(null).setLoginIp(null).setLoginDate(null).clearForbidUpdateField();
    SysUser sysUser = sysUserService.getById(entity.getId());
    String password = entity.getPassword();
    if (StrUtil.isNotBlank(password)) {
      if (sysUser != null && !password.equals(sysUser.getPassword())) {
        /*Sha256Hash encryptPassword = new Sha256Hash(password, password, Constants.PASSWORD_HASH_ITERATIONS);
        entity.setPassword(encryptPassword.toString());*/
        entity.setPassword(SecurityUtils.encryptPassword(password));
      }
    }
    boolean update = sysUserService.updateById(entity);
    if (update) {
      Set<String> keys = redisCache.keys(RedisConstants.LOGIN_INFO_KEY + sysUser.getUserAccount() + StringPool.ASTERISK);
      redisCache.delObject(keys);
    }
    return update ? R.ok() : R.resp(HttpStatus.DATA_UPDATE_FAILED);
  }

  @ApiOperation("删除、批量删除")
  @DeleteMapping("/delete")
  public R delete(@RequestBody @NotEmpty Set<@NotNull Long> ids) {
    List<SysUser> sysUsers = sysUserService.listByIds(ids);
    boolean delete = sysUserService.removeByIds(ids);
    if (delete) {
      Set<String> allKeys = new HashSet<>();
      for (SysUser sysUser : sysUsers) {
        Set<String> keys = redisCache.keys(RedisConstants.LOGIN_INFO_KEY + sysUser.getUserAccount() + StringPool.ASTERISK);
        allKeys.addAll(keys);
      }
      redisCache.delObject(allKeys);
    }
    return delete ? R.ok() : R.resp(HttpStatus.DATA_DELETE_FAILED);
  }

  @ApiOperation("详情")
  @GetMapping("/detail/{id}")
  public R<SysUser> detail(@PathVariable Long id) {
    SysUser user = sysUserService.getById(id);
    if (user == null) {
      return R.resp(HttpStatus.DATA_NOT_FOUND);
    }
    SysDept dept = sysDeptService.getById(user.getDeptId());
    if (dept != null) {
      user.setDeptName(dept.getDeptName());
    }
    return R.ok(user);
  }

  @ApiOperation("分页查询列表")
  @PostMapping("/page")
  public R<PageData<SysUser>> page(@RequestBody(required = false) PageDomain pageDomain) {
    PageData<SysUser> data = sysUserService.page(pageDomain == null ? new PageDomain() : pageDomain);
    return R.ok(data);
  }

  @ApiOperation("查询列表")
  @PostMapping("/list")
  public R<List<SysUser>> list(@RequestBody(required = false) QueryCondition condition) {
    List<SysUser> list = sysUserService.list(condition);
    return R.ok(list);
  }
}
