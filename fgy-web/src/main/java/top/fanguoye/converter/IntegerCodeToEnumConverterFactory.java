package top.fanguoye.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.stereotype.Component;
import top.fanguoye.core.interfaces.BaseEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：编码 -> 枚举 转化器工厂类
 * @Date：2021/12/27 10:18
 */
@Component
public class IntegerCodeToEnumConverterFactory implements ConverterFactory<Integer, BaseEnum> {

  private static final Map<Class, Converter> CONVERTERS = new HashMap<>();

  @Override
  public <T extends BaseEnum> Converter<Integer, T> getConverter(Class<T> targetType) {
    Converter<Integer, T> converter = CONVERTERS.get(targetType);
    if (converter == null) {
      converter = new IntegerToEnumConverter<>(targetType);
      CONVERTERS.put(targetType, converter);
    }
    return converter;
  }

  private static class IntegerToEnumConverter<T extends BaseEnum> implements Converter<Integer, T> {

    private final Map<Integer, T> enumMap = new HashMap<>();

    public IntegerToEnumConverter(Class<T> enumType) {
      T[] enums = enumType.getEnumConstants();
      for (T e : enums) {
        enumMap.put(e.getCode(), e);
      }
    }

    @Override
    public T convert(Integer source) {
      if (source == null) {
        return null;
      }
      T t = enumMap.get(source);
      if (t == null) {
        throw new IllegalArgumentException("参数有误！");
      }
      return t;
    }
  }
}
