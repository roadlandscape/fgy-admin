package top.fanguoye.converter;

import cn.hutool.core.util.StrUtil;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.stereotype.Component;
import top.fanguoye.core.interfaces.BaseEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：编码 -> 枚举 转化器工厂类
 * @Date：2021/12/27 09:27
 */
@Component
public class StringCodeToEnumConverterFactory implements ConverterFactory<String, BaseEnum> {

  private static final Map<Class, Converter> CONVERTERS = new HashMap<>();

  @Override
  public <T extends BaseEnum> Converter<String, T> getConverter(Class<T> targetType) {
    Converter<String, T> converter = CONVERTERS.get(targetType);
    if (converter == null) {
      converter = new StringToEnumConverter<>(targetType);
      CONVERTERS.put(targetType, converter);
    }
    return converter;
  }

  private static class StringToEnumConverter<T extends BaseEnum> implements Converter<String, T> {

    private final Map<String, T> enumMap = new HashMap<>();

    public StringToEnumConverter(Class<T> enumType) {
      T[] enums = enumType.getEnumConstants();
      for (T e : enums) {
        enumMap.put(String.valueOf(e.getCode()), e);
      }
    }

    @Override
    public T convert(String source) {
      if (StrUtil.isBlank(source)) {
        return null;
      }
      T t = enumMap.get(source);
      if (t == null) {
        throw new IllegalArgumentException("参数有误！");
      }
      return t;
    }
  }
}
