package top.fanguoye.filter;

import top.fanguoye.core.utils.ServletUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * @Author：Vance
 * @Description：构建可重复读取inputStream的request
 * @Date：2022/3/7 23:21
 */
public class RepeatedlyRequestWrapper extends HttpServletRequestWrapper {

  private final byte[] body;

  public RepeatedlyRequestWrapper(HttpServletRequest request) {
    super(request);
    body = ServletUtils.getBodyString(request).getBytes(StandardCharsets.UTF_8);
  }

  @Override
  public BufferedReader getReader() throws IOException {
    return new BufferedReader(new InputStreamReader(getInputStream()));
  }

  @Override
  public ServletInputStream getInputStream() throws IOException {
    final ByteArrayInputStream inputStream = new ByteArrayInputStream(body);
    return new ServletInputStream() {
      @Override
      public int read() throws IOException {
        return inputStream.read();
      }

      @Override
      public int available() throws IOException {
        return body.length;
      }

      @Override
      public boolean isFinished() {
        return false;
      }

      @Override
      public boolean isReady() {
        return false;
      }

      @Override
      public void setReadListener(ReadListener readListener) {
      }
    };
  }
}
