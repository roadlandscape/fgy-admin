package top.fanguoye.filter;

import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import top.fanguoye.authority.security.entity.LoginUser;
import top.fanguoye.authority.security.service.TokenService;
import top.fanguoye.core.constant.StringPool;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author：Vance
 * @Description：WebSocket过滤器
 * @Date：2022/2/20 22:30
 */
public class WebSocketFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String requestURI = request.getRequestURI();
        // Sec-WebSocket-Protocol请求头参数值不能有空格，传token不需要带前缀 "Bearer "
        String token = request.getHeader("Sec-WebSocket-Protocol");
        LoginUser loginUser = SpringUtil.getBean(TokenService.class).getLoginUser(token);
        if (loginUser == null || !String.valueOf(loginUser.getLoginUniqueKey())
            .equals(StrUtil.subAfter(requestURI, StringPool.SLASH, true))) {
            return;
        }

        HttpServletResponse response = (HttpServletResponse) servletResponse;
        // 如果传递了Sec-WebSocket-Protocol请求头，后端响应的时候，也必须带上这个响应头！否则前端接收不到数据！
        response.setHeader("Sec-WebSocket-Protocol", token);
        filterChain.doFilter(request, response);
    }
 
    @Override
    public void destroy() {
    }
}