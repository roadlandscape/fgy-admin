package top.fanguoye.handler;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.UnauthorizedException;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskRejectedException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import top.fanguoye.core.constant.Constants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.enums.HttpStatus;
import top.fanguoye.core.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;

/**
 * @Author：Vance
 * @Description：全局异常处理器
 * @Date：2021/12/18 17:14
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

  @Value("${spring.profiles.active:pro}")
  private String environment;

  /**
   * 系统异常
   */
  @ExceptionHandler(Exception.class)
  public R handleException(Exception e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',发生系统异常.", requestURI, e);
    return Constants.PRO_ENVI_VAR.equals(this.environment) ?  R.error() : R.error(e.getMessage());
  }

  /**
   * 未知运行时异常
   */
  @ExceptionHandler(RuntimeException.class)
  public R handleRuntimeException(RuntimeException e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',发生未知异常.", requestURI, e);
    return Constants.PRO_ENVI_VAR.equals(this.environment) ? R.error() : R.error(e.getMessage());
  }

  /**
   * 线程池已满，丢掉任务并抛出异常
   */
  @ExceptionHandler(TaskRejectedException.class)
  public R handleTaskRejectedException(TaskRejectedException e) {
    log.error("线程池已满，丢掉任务抛出异常.", e);
    return R.resp(HttpStatus.UNAVAILABLE);
  }

  /**
   * 权限认证未通过异常
   */
  @ExceptionHandler(UnauthorizedException.class)
  public R handleUnauthorizedException(UnauthorizedException e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',权限认证未通过.", requestURI, e);
    return R.resp(HttpStatus.FORBIDDEN);
  }

  /**
   * 权限认证未通过异常
   */
  @ExceptionHandler(AccessDeniedException.class)
  public R handleAccessDeniedException(AccessDeniedException e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',权限认证未通过.", requestURI, e);
    return R.resp(HttpStatus.FORBIDDEN);
  }

  /**
   * 业务异常
   */
  @ExceptionHandler(ServiceException.class)
  public R handleServiceException(ServiceException e) {
    log.error("业务异常{}：{}", e.getCode(), e.getMsg());
    return R.resp(e.getCode(), e.getMsg());
  }

  /**
   * 请求方式不支持
   */
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public R handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',不支持'{}'请求", requestURI, e.getMethod());
    return R.error(StrUtil.format("不支持{}请求方式", e.getMethod()));
  }

  /**
   * 处理方法参数校验异常
   */
  @ExceptionHandler(value = ConstraintViolationException.class)
  public R handleConstraintViolationException(ConstraintViolationException e) {
    StringBuilder msg = new StringBuilder();
    Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
    for (ConstraintViolation<?> constraintViolation : constraintViolations) {
      PathImpl pathImpl = (PathImpl) constraintViolation.getPropertyPath();
      String paramName = pathImpl.getLeafNode().getName();
      if ("<iterable element>".equals(paramName)) {
        paramName = "数组中的元素";
      } else if ("<list element>".equals(paramName)) {
        paramName = "数组中的元素";
      }
      String message = constraintViolation.getMessage();
      msg.append(paramName).append(message).append(StringPool.CHINESE_DOT);
    }
    if (StringPool.CHINESE_DOT.equals(msg.substring(msg.length() - 1))) {
      return R.error(msg.substring(0, msg.length() - 1));
    }
    return R.error(msg.toString());
  }

  /**
   * 处理实体类参数校验异常
   */
  @ExceptionHandler(BindException.class)
  public R handlerBindException(BindException e) {
    StringBuilder msg = new StringBuilder();
    List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
    int size = allErrors.size();
    for (int i = 0; i < size; i++) {
      ObjectError error = allErrors.get(i);
      String field = StringPool.EMPTY;
      if (error instanceof FieldError) {
        field = ((FieldError) error).getField();
      }
      String defaultMessage = error.getDefaultMessage();
      msg.append(field).append(defaultMessage);
      if (i != size - 1) {
        msg.append(StringPool.CHINESE_DOT);
      }
    }
    return R.error(msg.toString());
  }

  /**
   * 前端参数转换失败异常
   */
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public R handlerHttpMessageNotReadableException(HttpMessageNotReadableException e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',参数转换失败.", requestURI, e);
    return R.error(e.getCause().getCause().getMessage());
  }

  /**
   * 前端参数转换失败异常（处理自定义参数转换器抛出的异常）
   */
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public R handlerMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e, HttpServletRequest request) {
    String requestURI = request.getRequestURI();
    log.error("请求地址'{}',参数转换失败.", requestURI, e);
    return R.error(e.getName() + "参数有误");
  }

  /**
   * 处理文件上传大小超出限制异常
   */
  @ExceptionHandler(MaxUploadSizeExceededException.class)
  public R handlerMaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
    log.error("上传文件大小超出最大限制 - ", e);
    return R.error("上传文件大小超出最大限制！");
  }
}
