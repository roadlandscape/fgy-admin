package top.fanguoye.interceptor;

import cn.hutool.json.JSONUtil;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import top.fanguoye.core.annotation.RepeatSubmit;
import top.fanguoye.core.entity.R;
import top.fanguoye.core.utils.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @Author：Vance
 * @Description：防止重复提交拦截器
 * @Date：2022/3/7 15:01
 */
public abstract class RepeatSubmitInterceptor implements HandlerInterceptor {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    if (handler instanceof HandlerMethod) {
      HandlerMethod handlerMethod = (HandlerMethod) handler;
      Method method = handlerMethod.getMethod();
      RepeatSubmit annotation = method.getAnnotation(RepeatSubmit.class);
      if (annotation != null) {
        if (this.isRepeatSubmit(request, annotation)) {
          String result = JSONUtil.toJsonStr(R.error(annotation.message()));
          ServletUtils.renderString(response, result);
          return false;
        }
      }
    }
    return true;
  }

  /**
   * 由子类实现具体的防重复提交的规则
   * @param request
   * @param annotation
   * @return
   */
  public abstract boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation);
}
