package top.fanguoye.interceptor;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import top.fanguoye.core.annotation.RepeatSubmit;
import top.fanguoye.core.constant.RedisConstants;
import top.fanguoye.core.constant.StringPool;
import top.fanguoye.core.redis.RedisCache;
import top.fanguoye.core.utils.ServletUtils;
import top.fanguoye.filter.RepeatedlyRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author：Vance
 * @Description：判断请求url和数据是否和上一次相同，如果和上次相同，则是重复提交
 * @Date：2022/3/7 15:14
 */
@Component
@RequiredArgsConstructor
public class SameUrlDataInterceptor extends RepeatSubmitInterceptor {

  private final RedisCache redisCache;

  /** 令牌请求头 */
  @Value("${token.header}")
  private String tokenHeader;

  @Override
  public boolean isRepeatSubmit(HttpServletRequest request, RepeatSubmit annotation) {
    String nowParams = null;
    if (request instanceof RepeatedlyRequestWrapper) {
      RepeatedlyRequestWrapper repeatedlyRequest = (RepeatedlyRequestWrapper) request;
      nowParams = ServletUtils.getBodyString(repeatedlyRequest);
    }

    // body参数为空，则获取Parameter的数据
    if (StrUtil.isEmpty(nowParams)) {
      Map<String, String[]> parameterMap = request.getParameterMap();
      if (CollectionUtil.isEmpty(parameterMap)) {
        return false;
      }
      nowParams = JSONUtil.toJsonStr(parameterMap);
    }

    // 请求地址（作为存放cache的key值）
    String url = request.getRequestURI();
    // 唯一值（没有消息头则使用请求地址）
    String token = request.getHeader(this.tokenHeader);
    // 唯一标识（指定key + url + 消息头）
    String cacheRepeatKey = RedisConstants.REPEAT_SUBMIT_KEY + url + StringPool.COLON + token;

    String preParams = redisCache.getCacheObject(cacheRepeatKey);
    if (StrUtil.isNotEmpty(preParams) && nowParams.equals(preParams)) {
      return true;
    }

    redisCache.setCacheObject(cacheRepeatKey, nowParams, annotation.interval(), TimeUnit.MILLISECONDS);
    return false;
  }
}
