package top.fanguoye.server;

import cn.hutool.core.collection.CollectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import top.fanguoye.core.interfaces.IWebSocketServer;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @Author：Vance
 * @Description：WebSocket支撑类
 * @Date：2022/2/19 17:39
 */
@Slf4j
@Component
@ServerEndpoint("/websocket/{loginUniqueKey}")
public class WebSocketServer implements IWebSocketServer {

  /** 存放每个客户端连接的 session 对象 */
  private static final ConcurrentMap<String, Session> SESSION_MAP = new ConcurrentHashMap<>();

  private String loginUniqueKey;

  @OnOpen
  @Override
  public void onOpen(Session session, @PathParam("loginUniqueKey") String loginUniqueKey) {
    log.info("{}用户连接", loginUniqueKey);
    this.loginUniqueKey = loginUniqueKey;
    WebSocketServer.SESSION_MAP.put(loginUniqueKey, session);
  }

  @OnMessage
  @Override
  public void onMessage(String message, Session session) {
    log.info("{}用户消息:{}", this.loginUniqueKey, message);
  }

  @OnClose
  @Override
  public void onClose() {
    log.info("{}用户退出", this.loginUniqueKey);
    WebSocketServer.SESSION_MAP.remove(this.loginUniqueKey);
  }

  @OnError
  @Override
  public void onError(Session session, Throwable error) {
    log.error("{}用户错误 - ", this.loginUniqueKey, error);
  }

  /**
   * 服务器主动给在线用户推送
   * @param message
   */
  public void sendMessage(String message) throws IOException {
    if (CollectionUtil.isEmpty(WebSocketServer.SESSION_MAP)) {
      log.warn("pc会议没有用户在线！");
      return;
    }
    log.info("在线用户：{}", WebSocketServer.SESSION_MAP.keySet());
    Collection<Session> values = WebSocketServer.SESSION_MAP.values();
    for (Session value : values) {
      if (!value.isOpen()) {
        continue;
      }
      value.getBasicRemote().sendText(message);
    }
  }
}
