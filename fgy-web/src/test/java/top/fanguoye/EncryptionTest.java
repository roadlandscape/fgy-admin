package top.fanguoye;

import com.baomidou.mybatisplus.core.toolkit.AES;
import org.junit.jupiter.api.Test;

/**
 * @Author：Vance
 * @Description：
 * @Date：2021/12/16 13:58
 */
public class EncryptionTest {

  @Test
  public void test() {
    // 生成 16 位随机 AES 密钥
    String randomKey = AES.generateRandomKey();
    System.out.println(randomKey);
    // 随机密钥加密
    String result = AES.encrypt("root", randomKey);
    System.out.println(result); // 使用：mpw:result

    String decrypt = AES.decrypt(result, randomKey);
    System.out.println(decrypt);

    // 使用
    // Jar 启动参数（ idea 设置 Program arguments , 服务器可以设置为启动环境变量 ）
    // --mpw.key=root
  }
}
