package top.fanguoye.wechat.condition;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.dtflys.forest.callback.SuccessWhen;
import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.http.ForestResponse;

/**
 * @Author：Vance
 * @Description：获取access_token成功的条件
 * @Date：2022/2/8 22:28
 */
public class AccessTokenSuccessCondition implements SuccessWhen {

  @Override
  public boolean successWhen(ForestRequest req, ForestResponse res) {
    boolean isSuccess = res.noException() && res.statusOk();
    if (!isSuccess) {
      return false;
    }

    JSONObject obj = JSONUtil.parseObj(res.getContent());
    return obj.get("access_token") != null;
  }
}
