package top.fanguoye.wechat.condition;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.dtflys.forest.callback.SuccessWhen;
import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.http.ForestResponse;

/**
 * @Author：Vance
 * @Description：code2Session执行成功的条件
 * @Date：2022/6/28 18:08
 */
public class Code2SessionSuccessCondition implements SuccessWhen {

  @Override
  public boolean successWhen(ForestRequest req, ForestResponse res) {
    boolean isSuccess = res.noException() && res.statusOk();
    if (!isSuccess) {
      return false;
    }

    JSONObject obj = JSONUtil.parseObj(res.getContent());
    return (int) obj.get("errcode") == 0;
  }
}
