package top.fanguoye.wechat.condition;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.dtflys.forest.callback.SuccessWhen;
import com.dtflys.forest.http.ForestRequest;
import com.dtflys.forest.http.ForestResponse;

/**
 * @Author：Vance
 * @Description：模板消息相关接口请求成功的条件
 * @Date：2022/2/8 23:21
 */
public class TemplateInfoSuccessCondition implements SuccessWhen {

  @Override
  public boolean successWhen(ForestRequest req, ForestResponse res) {
    boolean isSuccess = res.noException() && res.statusOk();
    if (!isSuccess) {
      return false;
    }

    JSONObject obj = JSONUtil.parseObj(res.getContent());
    return (int) obj.get("errcode") == 0;
  }
}
