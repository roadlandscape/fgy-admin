package top.fanguoye.wechat.entity.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：微信公众号消息模板DTO
 * @Date：2022/1/29 17:51
 */
@Data
@Accessors(chain = true)
public class TemplateDto implements Serializable {

  private static final long serialVersionUID = 1L;

  /** 接收者openid */
  private String touser;
  /** 模板ID */
  private String template_id;
  /** 详情跳转url */
  private String url;
  /** 跳小程序所需数据，不需跳小程序可不用传该数据 */
  private MiniProgram miniprogram;
  /** 模板数据 */
  private Map<String, TemplateData> data;

  @Data
  public static class MiniProgram {

    /** 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏） */
    private String appid;
    /** 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），要求该小程序已发布，暂不支持小游戏 */
    private String pagepath;
  }

  @Data
  public static class TemplateData {

    private String value;
    /** 模板内容字体颜色，不填默认为黑色（十六进制） */
    private String color;
  }
}
