package top.fanguoye.wechat.request;

import com.dtflys.forest.annotation.BaseRequest;
import com.dtflys.forest.annotation.GetRequest;
import com.dtflys.forest.annotation.Success;
import com.dtflys.forest.annotation.Var;
import com.dtflys.forest.callback.OnError;
import top.fanguoye.wechat.condition.Code2SessionSuccessCondition;

import java.util.Map;

/**
 * @Author：Vance
 * @Description：微信小程序相关http请求
 * @Date：2022/6/28 17:19
 */
@BaseRequest(baseURL = "#{wechat.base-url}")
public interface WechatAppletRequest {

  /**
   * 获取微信用户 openid、session_key、unionid
   *
   * @param code
   * @param onError
   * @return
   */
  @GetRequest(url = "#{wechat.code2Session-url}?appid=#{wechat.appid}&secret=#{wechat.secret}&js_code={code}&grant_type=authorization_code")
  @Success(condition = Code2SessionSuccessCondition.class)
  Map<String, Object> code2Session(@Var("code") String code, OnError onError);
}
