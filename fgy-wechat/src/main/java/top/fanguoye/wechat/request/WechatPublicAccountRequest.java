package top.fanguoye.wechat.request;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.callback.OnError;
import top.fanguoye.wechat.condition.AccessTokenSuccessCondition;
import top.fanguoye.wechat.condition.TemplateInfoSuccessCondition;
import top.fanguoye.wechat.entity.dto.TemplateDto;

import java.util.Map;

/**
 * @Author：Vance
 * @Description：微信公众号相关http请求
 * @Date：2022/6/28 17:13
 */
@BaseRequest(baseURL = "#{wechat.base-url}")
public interface WechatPublicAccountRequest {

  /**
   * 获取基础支持的access_token
   *
   * @param onError
   * @return
   */
  @GetRequest(url = "#{wechat.base-access_token-url}?grant_type=client_credential&appid=#{wechat.appid}&secret=#{wechat.secret}")
  @Success(condition = AccessTokenSuccessCondition.class)
  Map<String, Object> getBaseAccessToken(OnError onError);

  /**
   * 推送模板信息
   *
   * @param accessToken
   * @param templateDto
   * @param onError
   * @return
   */
  @PostRequest(url = "#{wechat.template-url}")
  @Success(condition = TemplateInfoSuccessCondition.class)
  Map<String, Object> pushInfo(@Query("access_token") String accessToken, @JSONBody TemplateDto templateDto, OnError onError);
}
