package top.fanguoye.wechat.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.fanguoye.core.exception.ServiceException;
import top.fanguoye.wechat.entity.dto.TemplateDto;
import top.fanguoye.wechat.request.WechatPublicAccountRequest;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author：Vance
 * @Description：模板消息service
 * @Date：2022/2/8 09:32
 */
@Slf4j
@Service("templateInfoService")
@RequiredArgsConstructor
public class TemplateInfoService {

  private final WechatTokenService wechatTokenService;
  private final WechatPublicAccountRequest wechatPublicAccountRequest;

  /**
   * 推送模板消息
   * @param templateDto
   * @return
   */
  public boolean pushInfo(TemplateDto templateDto) {
    String accessToken = wechatTokenService.getBaseAccessToken();
    if (accessToken == null) {
      throw new ServiceException("accessToken获取失败！");
    }

    Map<String, Object> resultMap = wechatPublicAccountRequest.pushInfo(accessToken, templateDto, (e, req, resp) -> {
      log.error("推送模板信息失败 - ", e);
    });

    return resultMap != null;
  }

  // @PostConstruct
  public void test() {
    TemplateDto.TemplateData templateData = new TemplateDto.TemplateData();
    templateData.setColor("#f0000").setValue("123");
    Map<String, TemplateDto.TemplateData> map = new HashMap<>();
    map.put("first", templateData);

    TemplateDto templateDto = new TemplateDto();
    templateDto.setTemplate_id("kjaf4fyBnu38v66wL_jtxSlTcVKHFvLGnwMOLaY5gZU")
        .setTouser("oQ0TV5sqhu5wmV6gvMvtcUPO5wHk")
        .setUrl("www.baidu.com")
        .setData(map);
    pushInfo(templateDto);
  }
}
