package top.fanguoye.wechat.service;

import cn.hutool.core.util.StrUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.fanguoye.core.constant.RedisConstants;
import top.fanguoye.core.redis.RedisCache;
import top.fanguoye.wechat.request.WechatPublicAccountRequest;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author：Vance
 * @Description：微信token相关service
 * @Date：2022/2/8 10:19
 */
@Slf4j
@Service("wechatTokenService")
@RequiredArgsConstructor
public class WechatTokenService {

  private final RedisCache redisCache;
  private final WechatPublicAccountRequest wechatPublicAccountRequest;

  /**
   * 获取基础支持的access_token
   * @return
   */
  public String getBaseAccessToken() {
    String accessToken = redisCache.getCacheObject(RedisConstants.WECHAT_BASE_ACCESS_TOKEN);
    if (StrUtil.isNotEmpty(accessToken)) {
      return accessToken;
    }

    Map<String, Object> resultMap = wechatPublicAccountRequest.getBaseAccessToken((e, req, resp) -> {
      log.error("获取accessToken失败 - ", e);
    });

    if (resultMap == null) {
      return null;
    }

    accessToken = (String) resultMap.get("access_token");
    redisCache.setCacheObject(RedisConstants.WECHAT_BASE_ACCESS_TOKEN,
        accessToken, (Integer) resultMap.get("expires_in"), TimeUnit.SECONDS);

    return accessToken;
  }

  /**
   * 获取网页授权access_token
   * @return
   */
  public String getAuthorizeAccessToken() {
    // todo
    return null;
  }
}
